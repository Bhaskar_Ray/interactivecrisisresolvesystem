package com.iiucbrs.emergencyservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity implements
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener, OnClickListener, LocationListener {

	ImageView actionImageView, actionImageView_manu;
	Button manuButton;
	TextView actionTextview;
	ActionBar actionBar;
	Typeface tf;
	String type;
	ImageView snapshot;

	private static final int GPS_ERRORDIALOG_REQUEST = 9001;
	private static final float DEFAULTZOOM = 12.0f;
	private static final float CHITTAGONG_LAT = 22.371088f;
	private static final float CHITTAGONG_LNG = 91.791694f;

	private int PROXIMITY_RADIUS = 6000;
	private static final String GOOGLE_API_KEY = "AIzaSyAkun-3v9OWpI2amecQp67HznXwwcbZ5m8";
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	private com.google.android.gms.maps.GoogleMap mMap;
	private Marker mMarker_Map_Activity;

	private GoogleApiClient apiClient_map_activity;

	private Button nearestHospital_button;
	private Button myLocationButton;
	private Button refreshButton;
	Boolean connected;
	private double latitude = 22.371088f;
	private double longitude = 91.791694f;
	
    double currentLatitude;
    double currentLongitude;
	Location currentLocation;
	private LocationRequest mLocationRequest;
	//private double my_loc_lat,my_loc_lng;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//actionTextview = (TextView) findViewById(R.id.actionTextView);

		actionBar = getActionBar();
		actionBar.hide();

		// tf = Typeface.createFromAsset(this.getAssets(),
		// "Agatha-Regular.ttf");
		// Log.i("test", tf.toString());
		// actionTextview.setTypeface(tf, Typeface.BOLD);
		type = getIntent().getExtras().getString("types");
		//gotoLocation_Mapactivity(CHITTAGONG_LAT, CHITTAGONG_LNG, 12.0f);


		if (servicesOK()) {
			setContentView(R.layout.map_activity);
			//gotoLocation_Mapactivity(CHITTAGONG_LAT, CHITTAGONG_LNG, 12.0f);
			
//			//Akhan a extra add korlam
//			
//			if (initMap_Activity()) {
//				//Toast.makeText(MapActivity.this, "MapACtivity", Toast.LENGTH_LONG).show();
//				gotoLocation(latitude, longitude, 12.0f);
//				apiClient_map_activity = new GoogleApiClient.Builder(MapActivity.this)
//						.addApi(LocationServices.API)
//						.addConnectionCallbacks(MapActivity.this)
//						.addOnConnectionFailedListener(MapActivity.this).build();
//				apiClient_map_activity.connect();
//				setMarkers("Chittagong", "Bangladesh", CHITTAGONG_LAT,
//						CHITTAGONG_LNG, R.drawable.find_marker);
//				nearestPlaces(CHITTAGONG_LAT, CHITTAGONG_LNG);
//			} else {
//				Toast.makeText(this, "Map not available!", Toast.LENGTH_SHORT)
//						.show();
//			}
//			
//			//Akhan a extra add korlam
			
			// Create the LocationRequest object
			mLocationRequest = LocationRequest.create()
			        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
			        .setInterval(10 * 1000)        // 10 seconds, in milliseconds
			        .setFastestInterval(1 * 1000); // 1 second, in milliseconds

			nearestHospital_button = (Button) findViewById(R.id.nearestHospital_Button);
			myLocationButton = (Button) findViewById(R.id.myLocation_Button);
			refreshButton = (Button) findViewById(R.id.refreshButton);
			manuButton = (Button) findViewById(R.id.menu_Button);
			actionImageView = (ImageView) findViewById(R.id.snapshotImage);

			actionImageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						CaptureMapScreen();
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}
			});
			nearestHospital_button.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						LayoutInflater inflater = LayoutInflater
								.from(MapActivity.this);
						View view = inflater.inflate(R.layout.search_layout,
								null);

						final EditText searchET = (EditText) view
								.findViewById(R.id.etSearch);

						AlertDialog.Builder builder = new AlertDialog.Builder(
								MapActivity.this);
						builder.setTitle("Find Location");
						builder.setView(view);

						builder.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										mMap.clear();
										checkIConnection();
										if (connected) {

											String location = searchET
													.getText().toString();

											Geocoder gc = new Geocoder(
													MapActivity.this);
											List<Address> addresses = null;
											try {
												addresses = gc
														.getFromLocationName(
																location, 1);
											} catch (IOException e) {
												e.printStackTrace();
												return;
											}
											Address address;
											try {
												address = addresses.get(0);
											} catch (Exception e) {
												e.printStackTrace();
												Log.e("Other_LocationFind",
														e.getMessage());
												Toast.makeText(
														MapActivity.this,
														"Location not found",
														Toast.LENGTH_SHORT)
														.show();
												return;
											}
											latitude = address.getLatitude();
											longitude = address.getLongitude();
											gotoLocation_Mapactivity(address.getLatitude(),
													address.getLongitude(),
													11.0f);
											setMarkers(address.getLocality(),
													address.getCountryName(),
													address.getLatitude(),
													address.getLongitude(),
													R.drawable.find_marker);
											nearestPlaces(
													address.getLatitude(),
													address.getLongitude());

											return;
										} else {
											Toast.makeText(
													MapActivity.this,
													"Internet connection not available",
													Toast.LENGTH_SHORT).show();
										}

									}
								});
						builder.setNeutralButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
									}
								});
						AlertDialog simpleDialog = builder.create();
						simpleDialog.show();

					} catch (Exception e) {
						// TODO: handle exception
						Log.e("other_findplace", e.getMessage());
					}

				}
			});

			myLocationButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						currentLocation = LocationServices.FusedLocationApi
								.getLastLocation(apiClient_map_activity);
						if (currentLocation == null) {
							Toast.makeText(getApplicationContext(),
									"Current location isn't available",
									Toast.LENGTH_SHORT).show();
						} else {
							mMap.clear();
							latitude=currentLocation.getLatitude();
							longitude=currentLocation.getLongitude();
							handleNewLocation(currentLocation);
							
							nearestPlaces(currentLocation.getLatitude(),
									currentLocation.getLongitude());
						}

					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(
								getApplicationContext(),
								"Location not found" + "\n"
										+ "Check Your Location Settings",
								Toast.LENGTH_LONG).show();
					}

				}
			});

			refreshButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// finish();
					// Intent in =new
					// Intent(MapActivity.this,MapActivity.class);
					// in.putExtra("types", type);
					// startActivity(in);

					gotoLocation_Mapactivity(latitude, longitude, 11.0f);
					setMarkers("", "", latitude, longitude,
							R.drawable.find_marker);
					nearestPlaces(latitude, longitude);
				}
			});

			manuButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					final String mapTypes[] = { "None", "Normal", "Satellite",
							"Terrain", "Hybrid" };
					AlertDialog.Builder builder = new AlertDialog.Builder(
							MapActivity.this);
					builder.setTitle("Set Map Type");
					builder.setCancelable(false);
					builder.setItems(mapTypes,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int possition) {
									switch (possition) {
									case 0:
										mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
										break;

									case 1:
										mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
										break;

									case 2:
										mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
										break;

									case 3:
										mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
										break;

									case 4:
										mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
										break;

									default:
										break;
									}
								}
							});
					builder.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int possition) {
									dialog.dismiss();
								}
							});
					AlertDialog mapTypeDialog = builder.create();
					mapTypeDialog.show();

				}
			});

			if (initMap_Activity()) {
				mMap.clear();
				
				apiClient_map_activity = new GoogleApiClient.Builder(this)
						.addApi(LocationServices.API)
						.addConnectionCallbacks(this)
						.addOnConnectionFailedListener(this).build();
				apiClient_map_activity.connect();
				setMarkers("Chittagong", "Bangladesh", CHITTAGONG_LAT,
						CHITTAGONG_LNG, R.drawable.find_marker);
				gotoLocation_Mapactivity(CHITTAGONG_LAT, CHITTAGONG_LNG, DEFAULTZOOM);
				nearestPlaces(CHITTAGONG_LAT, CHITTAGONG_LNG);
			} else {
				Toast.makeText(this, "Map not available!", Toast.LENGTH_SHORT)
						.show();
			}
		} else {
			Toast.makeText(this, "Google Play Services is not available",
					Toast.LENGTH_LONG).show();
			finish();
		}
		// datasource = new CrimeDatasource(this);
		// datasource.open();
	}

	private boolean initMap_Activity() {
		if (mMap == null) {
			SupportMapFragment mapFrag_map_activity = (SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map);
			mMap = mapFrag_map_activity.getMap();

			if (mMap != null) {

				/*
				 * mMap.setInfoWindowAdapter(new InfoWindowAdapter() {
				 * 
				 * @Override public View getInfoWindow(Marker marker) { View
				 * view = getLayoutInflater().inflate( R.layout.info_windows,
				 * null);
				 * 
				 * ImageView crimeIcon = (ImageView) view
				 * .findViewById(R.id.crime_ic); TextView crimeType = (TextView)
				 * view .findViewById(R.id.crime_type); TextView crimeDesc =
				 * (TextView) view .findViewById(R.id.crime_desc); TextView
				 * victimName = (TextView) view .findViewById(R.id.victim_name);
				 * 
				 * return view; }
				 * 
				 * @Override public View getInfoContents(Marker marker) { return
				 * null; } });
				 */

				mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

					@Override
					public void onInfoWindowClick(Marker marker) {

					}
				});

				//
				// mMap.setOnMapLongClickListener(new OnMapLongClickListener() {
				//
				// @Override
				// public void onMapLongClick(LatLng ll) {
				// Intent intent = new Intent(GoogleMap.this,
				// AddNewSpotActivity.class);
				// startActivity(intent);
				// }
				// });

				mMap.setOnMarkerClickListener(new OnMarkerClickListener() {

					@Override
					public boolean onMarkerClick(Marker marker) {
						try {
							if (mMarker_Map_Activity != null) {
								String str_origin = "origin="
										+ mMarker_Map_Activity.getPosition().latitude + ","
										+ mMarker_Map_Activity.getPosition().longitude;
								// Destination of route
								String str_dest = "destination="
										+ marker.getPosition().latitude + ","
										+ marker.getPosition().longitude;
								// Sensor enabled
								String sensor = "sensor=false";
								// Building the parameters to the web service
								String parameters = str_origin + "&" + str_dest
										+ "&" + sensor;
								// Output format
								String output = "json";
								// Building the url to the web service
								String url = "https://maps.googleapis.com/maps/api/directions/"
										+ output + "?" + parameters;
								DownloadTask downloadTask = new DownloadTask();
								// Start downloading json data from Google
								// Directions
								// API
								Object[] toPass = new Object[2];
								toPass[0] = mMap;
								toPass[1] = url;
								downloadTask.execute(toPass);
							}

						} catch (Exception e) {
							// TODO: handle exception

							Toast.makeText(getApplicationContext(),
									"Check your Location Access",
									Toast.LENGTH_LONG).show();

						}

						return false;
					}
				});
			}
		}
		return (mMap != null);
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}

	public boolean servicesOK() {
		int isAvailable = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		if (isAvailable == ConnectionResult.SUCCESS) {
			return true;
		} else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable)) {
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isAvailable,
					this, GPS_ERRORDIALOG_REQUEST);
			dialog.show();
		} else {
			Toast.makeText(this, "Can't connect to Google Play services",
					Toast.LENGTH_SHORT).show();
		}
		return false;
	}

	private void gotoLocation_Mapactivity(double lat_new, double lng_new, float zoom_new) {
		try {
//			Toast.makeText(this, "zomming",
//					Toast.LENGTH_SHORT).show();
			
			LatLng lat_lng = new LatLng(lat_new, lng_new);
			CameraUpdate update_new = CameraUpdateFactory.newLatLngZoom(lat_lng, zoom_new);
			mMap.animateCamera(update_new);
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("MPAC_GOTO", e.getMessage());
		}
		
	}

	public void geoLocate(View v) throws IOException {

	}

	private void setMarkers(String locality, String countryName, double lat,
			double lng, int markerIconRes) {

		try {

			if (mMarker_Map_Activity != null) {
				mMarker_Map_Activity.remove();
			}
			MarkerOptions options = new MarkerOptions().title(locality)
					.position(new LatLng(lat, lng))
					.icon(BitmapDescriptorFactory.fromResource(markerIconRes));

			if (markerIconRes == 0) {
				options.icon(BitmapDescriptorFactory.defaultMarker());
			}

			if (countryName.length() > 0) {
				options.snippet(countryName);
			}

			mMarker_Map_Activity = mMap.addMarker(options);
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("MapActi_SetMarker", e.getMessage());
		}

	}

	private void hideSoftKeyboard(View v) {
		InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	@Override
	protected void onResume() {

		super.onResume();
		// datasource.open();
		// MapStateManager mgr = new MapStateManager(this);
		// CameraPosition position = mgr.getSavedCameraPosition();
		// if (position != null) {
		// CameraUpdate update = CameraUpdateFactory
		// .newCameraPosition(position);
		// mMap.moveCamera(update);
		// mMap.setMapType(mgr.getSavedMapType());
		// }
		try {
			apiClient_map_activity.connect();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("onResume", e.getMessage());
		}
		
	
	}

	@Override
	protected void onPause() {
		super.onPause();
		// datasource.close();
		try {
			if (apiClient_map_activity.isConnected()) {
				LocationServices.FusedLocationApi.removeLocationUpdates(apiClient_map_activity, this);
		        apiClient_map_activity.disconnect();
		    }
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("onPause", e.getMessage());
		}
		
		
	}

	@Override
	protected void onStop() {
		super.onStop();
//		MapStateManager mgr = new MapStateManager(MapActivity.this);
//		mgr.saveMapState(mMap);
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		
		 if (connectionResult.hasResolution()) {
		        try {
		            // Start an Activity that tries to resolve the error
		            connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
		        } catch (IntentSender.SendIntentException e) {
		            e.printStackTrace();
		        }
		    } else {
		        Log.i("onConnectionFailed", "Location services connection failed with code " + connectionResult.getErrorCode());
		    }
		
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		 Log.i("onConnected_mapacti", "Location services connected.");
		 try {
			 
			 currentLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient_map_activity);
			 
			 if (currentLocation == null) {
			        LocationServices.FusedLocationApi.requestLocationUpdates(apiClient_map_activity, mLocationRequest, this);
			    }
			    else {
			        //handleNewLocation(location);
		    	 LocationServices.FusedLocationApi.requestLocationUpdates(apiClient_map_activity, mLocationRequest, this);
//			    	 handleNewLocation(currentLocation);
			    }
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("onConnected", e.getMessage());
		}
		 
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		  Log.i("onConnectionSuspended", "Location services suspended. Please reconnect.");
	}

//	private LatLng getCurrentLocation() {
//		Location currentLocation = LocationServices.FusedLocationApi
//				.getLastLocation(apiClient_map_activity);
//		return new LatLng(currentLocation.getLatitude(),
//				currentLocation.getLongitude());
//	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	public void CaptureMapScreen() {
		SnapshotReadyCallback callback = new SnapshotReadyCallback() {
			Bitmap bitmap;

			@Override
			public void onSnapshotReady(Bitmap snapshot) {
				// TODO Auto-generated method stub
				bitmap = snapshot;

				String filePath = Environment.getExternalStorageDirectory()
						+ File.separator + "Pictures/MapScreen.png";
				File imagePath = new File(filePath);
				FileOutputStream fos;
				try {
					fos = new FileOutputStream(imagePath);
					bitmap.compress(CompressFormat.PNG, 100, fos);
					fos.flush();
					fos.close();

				} catch (FileNotFoundException e) {
					Log.e("GREC", e.getMessage(), e);
				} catch (IOException e) {
					Log.e("GREC", e.getMessage(), e);
				}

				// try {
				// FileOutputStream out = new FileOutputStream("/mnt/sdcard/"
				// + "MyMapScreen" + System.currentTimeMillis()
				// + ".png");
				//
				// // above "/mnt ..... png" => is a storage path (where image
				// will be stored) + name of image you can customize as per your
				// Requirement
				//
				// bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
				// } catch (Exception e) {
				// e.printStackTrace();
				// }
				openShareImageDialog(filePath);
			}
		};

		mMap.snapshot(callback);

		// myMap is object of GoogleMap +> GoogleMap myMap;
		// which is initialized in onCreate() =>
		// myMap = ((SupportMapFragment)
		// getSupportFragmentManager().findFragmentById(R.id.map_pass_home_call)).getMap();
	}

	public void openShareImageDialog(String filePath) {
		// File file = this.getFileStreamPath(filePath);

		if (!filePath.equals("")) {
			// final ContentValues values = new ContentValues(2);
			// values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
			// values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
			// final Uri contentUriFile =
			// getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
			// values);

			Intent intent = new Intent(android.content.Intent.ACTION_SEND);
			intent.setType("image/png");
			Uri myUri = Uri.parse("file://" + filePath);
			intent.putExtra(android.content.Intent.EXTRA_STREAM, myUri);
			startActivity(Intent.createChooser(intent, "Share Image"));
		} else {
			// This is a custom class I use to show dialogs...simply replace
			// this with whatever you want to show an error message, Toast, etc.
			Toast.makeText(getApplicationContext(), "Share Image Failed",
					Toast.LENGTH_LONG).show();
		}
	}

	private void nearestPlaces(double lat, double lng) {

		StringBuilder googlePlacesUrl = new StringBuilder(
				"https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
		googlePlacesUrl.append("location=" + lat + "," + lng);
		googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
		googlePlacesUrl.append("&types=" + type);
		googlePlacesUrl.append("&sensor=true");
		googlePlacesUrl.append("&key=" + GOOGLE_API_KEY);

		if (type.equals("hospital")) {
			GooglePlacesReadTask googlePlacesReadTask = new GooglePlacesReadTask();
			Object[] toPass = new Object[3];
			toPass[0] = mMap;
			toPass[1] = googlePlacesUrl.toString();
			toPass[2] = new LatLng(lat, lng);
			googlePlacesReadTask.execute(toPass);

		} else if (type.equals("police")) {
			PolicePlaceReadTask policePlaceReadTask = new PolicePlaceReadTask();
			Object[] toPass = new Object[3];
			toPass[0] = mMap;
			toPass[1] = googlePlacesUrl.toString();
			toPass[2] = new LatLng(lat, lng);
			policePlaceReadTask.execute(toPass);
		} else if (type.equals("fire_station")) {
			FirePlaceReadTask firePlaceReadTask = new FirePlaceReadTask();
			Object[] toPass = new Object[3];
			toPass[0] = mMap;
			toPass[1] = googlePlacesUrl.toString();
			toPass[2] = new LatLng(lat, lng);
			firePlaceReadTask.execute(toPass);
		}

	}

	public void checkIConnection() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState() == NetworkInfo.State.CONNECTED
				|| connectivityManager.getNetworkInfo(
						ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
			connected = true;
			// Toast.makeText(getApplicationContext(), "Connected",
			// Toast.LENGTH_LONG).show();
		} else {
			connected = false;
			Toast.makeText(getApplicationContext(),
					"No internet connection available", Toast.LENGTH_LONG)
					.show();
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		mMap.clear();
		finish();
		
	}
	
	private void handleNewLocation(Location location) {
	    Log.d("handleNewLocation", location.toString());

	   currentLatitude = location.getLatitude();
	   currentLongitude = location.getLongitude();
	    
	    gotoLocation_Mapactivity(currentLatitude,
	    		currentLongitude , 11.0f);
		setMarkers("I am here", "",
				currentLatitude,
				currentLongitude,
				R.drawable.me);
	    
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
//		try {
//			
//			Toast.makeText(MapActivity.this, "Location Changed", Toast.LENGTH_LONG).show();
//			handleNewLocation(currentLocation);
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//			Log.e("onLocationChanged", e.getMessage());
//		}
		
		
	}

}
