package com.iiucbrs.emergencyservice;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

public class ReportActivity extends Activity  implements OnClickListener {
	ActionBar actionBar;
	private Spinner CategorySpinner;
	private Spinner SourceSpinner;
	private Spinner SeverityLevelSpinner;
	private EditText titleEditText;
	private EditText descriptionEditText;
	private EditText impactEditText;
	private EditText dateEditText;
	private EditText timeEditText;
	ImageView photo;
	String image;
	//String formatted;
	String url;
	boolean connected;
	boolean available = false;
	int success;
	String message;
	String imgPath;
	String locality="";
	double latitude,longitude;
	int reportCategoryId;
	List<String> severityList;
	List<String> severityIc;
	List<String> souceList;
	List<String> souceIcon;
	public static final String U_ID = "user_id";
	SharedPreferences pref_user_id;
	private JSONParser jsonParser = new JSONParser();

	private AsyncTask<String, String, String> asyncTask;

	private ProgressDialog pDialog;

	JSONObject jsonObj;

	private Button submit;
	private String reportSource;
	private String severityLevel;
	String value;

	private String serverResponse = null;

	private DatePickerDialog datePickerDialog;
	private TimePickerDialog timePickerDialog;
	private SimpleDateFormat dateFormatter;
	ArrayList<String> categoryTitle = new ArrayList<String>();
	ArrayList<Integer> category_id = new ArrayList<Integer>();
	ArrayList<String> category_image = new ArrayList<String>();
	public static final String POST_URL = "http://codencrayon.com/crisis_resolve/testAdd.php";
	//public static final String POST_URL = "http://192.168.43.191/myserver/testAdd.php";
	private Uri outputFileUri;
	private static final int CAPTURE_PHOTO = 1;
	private static final int RESULT_LOAD_IMG = 2;
	
	
	Bitmap bitmap;
	String encodedString;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_report);
		actionBar = getActionBar();
		actionBar.hide();
		pref_user_id= getSharedPreferences("USER_ID",
				Context.MODE_PRIVATE);
		titleEditText = (EditText) findViewById(R.id.edtTitle);
		submit = (Button) findViewById(R.id.btnSubmit);
		descriptionEditText = (EditText) findViewById(R.id.edtDescription);
		impactEditText = (EditText) findViewById(R.id.edtImpact);
		timeEditText = (EditText) findViewById(R.id.edtTime);
		timeEditText.setInputType(InputType.TYPE_NULL);
		dateEditText = (EditText) findViewById(R.id.edtDate);
		dateEditText.setInputType(InputType.TYPE_NULL);
		CategorySpinner = (Spinner) findViewById(R.id.spCategory);
		SourceSpinner = (Spinner) findViewById(R.id.spSource);
		SeverityLevelSpinner = (Spinner) findViewById(R.id.spSeverity);
		dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		photo=(ImageView) findViewById(R.id.reportPhoto);
		
		latitude = getIntent().getDoubleExtra("lat", 0);
		longitude = getIntent().getDoubleExtra("lng", 0);
		Toast.makeText(getApplicationContext(), "Lat: "+latitude+"\nLon"+longitude, Toast.LENGTH_LONG).show();
		Geocoder gc = new Geocoder(ReportActivity.this);
		try {
			List<Address> addresses = gc.getFromLocation(latitude,
					longitude, 1);
			Address address = addresses.get(0);
			locality = address.getLocality();
			Toast.makeText(getApplicationContext(), "Locality: "+locality, Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			Log.e("Locality_ERROR", ""+e);
		}
		

		timeEditText.setOnClickListener(this);
		dateEditText.setOnClickListener(this);
		photo.setOnClickListener(this);
		submit.setOnClickListener(this);
		titleEditText.setOnClickListener(this);
		descriptionEditText.setOnClickListener(this);
		impactEditText.setOnClickListener(this);
		try {
			checkIConnection();
			if(connected)
			new Category_AsyncTaskRunner().execute();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Category_AsyncTaskRunner", ""+e);
		}
		
		String[] sev = getResources().getStringArray(R.array.severity_type);
		severityList = Arrays.asList(sev);
		String[] sevIc = getResources().getStringArray(R.array.severity_ic);
		severityIc = Arrays.asList(sevIc);
		SourceAdapter severityAdapter = new SourceAdapter(
				getApplicationContext(), severityList, severityIc);
		SeverityLevelSpinner.setAdapter(severityAdapter);
		SeverityLevelSpinner
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub

						severityLevel = severityList.get(position);
//						Toast.makeText(getApplicationContext(), severityLevel,
//								Toast.LENGTH_LONG).show();
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub

					}
				});
		String[] source = getResources().getStringArray(R.array.source_type);
		souceList = Arrays.asList(source);
		String[] sourceIc = getResources().getStringArray(R.array.source_ic);
		souceIcon = Arrays.asList(sourceIc);
		SourceAdapter sourceAd = new SourceAdapter(getApplicationContext(),
				souceList, souceIcon);
		SourceSpinner.setAdapter(sourceAd);
		SourceSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				reportSource = souceList.get(position);
//				Toast.makeText(getApplicationContext(), reportSource,
//						Toast.LENGTH_LONG).show();

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});
		
//		double latitude = getIntent().getDoubleExtra("lat", 0);
//		double longitude = getIntent().getDoubleExtra("lng", 0);
		//Toast.makeText(getApplicationContext(), "Latitude = "+latitude+"Longitiude ="+longitude, Toast.LENGTH_LONG).show();

	//LocationManager locMan = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
       //long networkTS = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getTime();
      //long networkTS = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getTime();
		//long networkTS = System.currentTimeMillis();
        //Toast.makeText(getApplicationContext(),""+networkTS,Toast.LENGTH_LONG ).show();
       // Date date = new Date(networkTS);
        //SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss aa");
       // format.setTimeZone(TimeZone.getTimeZone("GMT+6:00"));
      // formatted = format.format(date);
        //Toast.makeText(getApplicationContext(),""+formatted,Toast.LENGTH_LONG ).show();
       


	}
	

 	
	public void checkIConnection() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState() == NetworkInfo.State.CONNECTED
				|| connectivityManager.getNetworkInfo(
						ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
			connected = true;
			Toast.makeText(getApplicationContext(), "Connected",
					Toast.LENGTH_LONG).show();
		} else {
			connected = false;
			Toast.makeText(getApplicationContext(),
					"No internet connection available", Toast.LENGTH_LONG)
					.show();
		}
	}
	private static String paddingString(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	class Category_AsyncTaskRunner extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(ReportActivity.this);

		protected void onPreExecute() {
			progressDialog.setMessage("Please wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				url = "http://codencrayon.com/crisis_resolve/getcategory.php";
				jsonObj = JSONsimple.getJSONfromURL(url);

				categoryTitle.clear();
				category_id.clear();
				category_image.clear();
				success = jsonObj.getInt("success");
				if (success == 1) {
					JSONArray spins = jsonObj.getJSONArray("category");
					for (int i = 0; i < spins.length(); i++) {
						JSONObject spin = spins.getJSONObject(i);

						categoryTitle.add(spin.optString("type_name"));
						category_image.add(spin.optString("type_icon"));
						category_id.add(spin.optInt("type_id"));
					}
					available = true;
				}

			} catch (Exception e) {
				Log.d("Error:", e + "");
			}
			return null;
		}

		protected void onPostExecute(String string) {
			ReportAdapter rpAdapter = new ReportAdapter(
					getApplicationContext(), categoryTitle, category_image);
			CategorySpinner.setAdapter(rpAdapter);
			CategorySpinner
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							reportCategoryId = category_id.get(position);
//							Toast.makeText(getApplicationContext(),
//									"CategoryID=" + reportCategoryId,
//									Toast.LENGTH_SHORT).show();

						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
							// TODO Auto-generated method stub

						}
					});

			progressDialog.dismiss();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == dateEditText) {
			//dateEditText.requestFocus();
			Calendar newCalendar = Calendar.getInstance();
			datePickerDialog = new DatePickerDialog(this,
					new OnDateSetListener() {

						public void onDateSet(DatePicker view, int year,
								int monthOfYear, int dayOfMonth) {
							Calendar newDate = Calendar.getInstance();
							newDate.set(year, monthOfYear, dayOfMonth);
							dateEditText.setText(dateFormatter.format(newDate
									.getTime()));
						}

					}, newCalendar.get(Calendar.YEAR),
					newCalendar.get(Calendar.MONTH),
					newCalendar.get(Calendar.DAY_OF_MONTH));
			datePickerDialog.show();
		} else if (v == timeEditText) {
			//timeEditText.requestFocus();
			Calendar newCalendar = Calendar.getInstance();
			timePickerDialog = new TimePickerDialog(this,
					new OnTimeSetListener() {

						@Override
						public void onTimeSet(TimePicker view, int hourOfDay,
								int minute) {
							timeEditText.setText(paddingString(hourOfDay) + ":"
									+ paddingString(minute));
						}
					}, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);
			timePickerDialog.show();
		} 
		else if (v == photo) {
			

			// TODO Auto-generated method stub
			LayoutInflater inflater=LayoutInflater.from(ReportActivity.this);
			View view=inflater.inflate(R.layout.custom_layout, null);
			Button btnDialogCam=(Button)view.findViewById(R.id.btnDialogCam);
			Button btnDialogLibrary=(Button)view.findViewById(R.id.btnDialogLibrary);
			Button btnCancel=(Button)view.findViewById(R.id.btnCancel);
			
			AlertDialog.Builder builder=new AlertDialog.Builder(ReportActivity.this);
			builder.setView(view);
			builder.setCancelable(false);
			final AlertDialog dialog=builder.create();
			
			btnDialogCam.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					String imageTitle = titleEditText.getText().toString().replaceAll("\\s+",""); //white space remove kortesi
		            imageTitle=imageTitle.toLowerCase();
					image = imageTitle+"_"+System.currentTimeMillis() + ".jpg";
					
					File file = new File(Environment.getExternalStorageDirectory(),
							image);
					outputFileUri = Uri.fromFile(file);
					Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
					startActivityForResult(captureIntent, CAPTURE_PHOTO);
					dialog.cancel();
				}
			});
			btnDialogLibrary.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent galleryIntent = new Intent(Intent.ACTION_PICK,
			                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			        // Start the Intent
			        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
			        dialog.cancel();
					
				}
			});
			btnCancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 dialog.cancel();
				}
			});
			
			dialog.show();
			
		
			
			
			
			
			
		} 
		else if (v == submit) {
			try {
				
				if (imgPath != null && !imgPath.isEmpty()) {
		           
					new encodeImagetoString().execute();
		        } else {
		            Toast.makeText(
		                    getApplicationContext(),
		                    "No photo Added",
		                    Toast.LENGTH_LONG).show();
		            runAddNewIncidentSpot();
		        }
				
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("SUBMIT_ERROR", ""+e);
			}
		}
		else if(v==titleEditText)
		{
			//To-Do
			//titleEditText.requestFocus();
			String preValue = titleEditText.getText()
					.toString();
			
			inputDialog(preValue,"Title: ", "Report Title",1);
			
		}
		else if(v==descriptionEditText)
		{
			//To-Do
			//descriptionEditText.requestFocus();
			String preValue = descriptionEditText.getText()
					.toString();
			inputDialog(preValue,"Description: ", "Description of the Incident",2);
			
		}
		else if(v==impactEditText)
		{
			//To-Do
			//impactEditText.requestFocus();
			String preValue = impactEditText.getText()
					.toString();
			inputDialog(preValue,"Event Impact: ", "Casulaties or Missing,Evacuated and Injured People",3);
			
		}
		
	}
	/*public void upload()
	{
		if (outputFileUri != null) {
			
			try {
				// image file->bitmap
				BitmapFactory.Options op = new BitmapFactory.Options();
				op.inPreferredConfig = Bitmap.Config.ARGB_8888;
				op.inSampleSize = 2;

				Bitmap bmp = BitmapFactory.decodeFile(outputFileUri.getPath(),
						op);

				// bitmap-> bytearrayoutputstream
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.PNG, 50, baos);

				// bytearrayoutputstream->byte[]
				byte[] ba = baos.toByteArray();

				// byte[]-> base 64 encoded string
				ba1 = Base64.encodeBytes(ba);
				//Toast.makeText(getApplicationContext(), "base 64"+ba1, Toast.LENGTH_LONG).show();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			
		}
		else
		{
			Toast.makeText(getApplicationContext(), "upload image not possible", Toast.LENGTH_LONG).show();
		}
		

	}*/
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAPTURE_PHOTO) {
			if (resultCode == RESULT_OK) {
				if (outputFileUri != null) {
					imgPath=outputFileUri.getPath();
					BitmapFactory.Options ops = new BitmapFactory.Options();
					ops.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(imgPath, ops);
					int imgW = ops.outWidth;
					int imgH = ops.outHeight;
					int scaleFactor = Math.min(imgW / 320, imgH / 240);
					ops.inJustDecodeBounds = false;
					ops.inSampleSize = scaleFactor;
					ops.inPurgeable = true;

					

					Bitmap bmp = BitmapFactory.decodeFile(
							imgPath, ops);
					photo.setImageBitmap(bmp);
					Toast.makeText(getApplicationContext(), image, Toast.LENGTH_LONG).show();
				}
			} else {
				
				//ToDo................

			}
		}else if(requestCode==RESULT_LOAD_IMG)
		{
			if (resultCode == RESULT_OK) {
			//TO DO----FOR LOADING
			Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            // Get the cursor
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgPath = cursor.getString(columnIndex);
            cursor.close();
            
            // Set the Image in ImageView
            photo.setImageBitmap(BitmapFactory
                    .decodeFile(imgPath));
            // Get the Image's file name
            String fileNameSegments[] = imgPath.split("/");
            image = fileNameSegments[fileNameSegments.length - 1];
            String imageTitle = titleEditText.getText().toString().replaceAll("\\s+",""); //white space remove kortesi
            imageTitle=imageTitle.toLowerCase();
            image=imageTitle+"_"+System.currentTimeMillis()+image;
            Toast.makeText(getApplicationContext(), image, Toast.LENGTH_LONG).show();
			}
			else
			{
				//To-DO.............
			}
		}
	}
	private void runAddNewIncidentSpot() {
//		if (connected) {
			if (asyncTask == null) {
				// --- create a new task --
				asyncTask = new AddNewReportSpot();
				asyncTask.execute();
			} else if (asyncTask.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTask = new AddNewReportSpot();
				asyncTask.execute();
			} else if (asyncTask.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTask.cancel(false);
				asyncTask = new AddNewReportSpot();
				asyncTask.execute();
			}
		//} 
//	else {
//			Toast.makeText(this, "Internet connection not available",
//					Toast.LENGTH_SHORT).show();
//		}
	}
	
	
	public class encodeImagetoString extends AsyncTask<String, String, String>
	{
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pDialog = new ProgressDialog(ReportActivity.this);
			pDialog.setMessage("Converting Image to Binary Data..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			// image file->bitmap
			BitmapFactory.Options op = new BitmapFactory.Options();
			op.inPreferredConfig = Bitmap.Config.ARGB_8888;
			op.inSampleSize = 2;

			Bitmap bmp = BitmapFactory.decodeFile(imgPath,
					op);

			// bitmap-> bytearrayoutputstream
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);

			// bytearrayoutputstream->byte[]
			byte[] ba = baos.toByteArray();

			// byte[]-> base 64 encoded string
			encodedString = Base64.encodeBytes(ba);
			return null;
			
			/*BitmapFactory.Options options = null;
            options = new BitmapFactory.Options();
            options.inSampleSize = 3;
            bitmap = BitmapFactory.decodeFile(imgPath,
                    options);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            // Must compress the Image to reduce image size to make upload easy
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream); 
            byte[] byte_arr = stream.toByteArray();
            // Encode Image to String
            encodedString = Base64.encodeBytes(byte_arr);
            
			return null;*/
		}
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pDialog.dismiss();
			try {
				checkIConnection();
				if(connected)
				{
					
					runAddNewIncidentSpot();
				}
				else
				{
					//To-Do------------
				}
				
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("runAddNewIncidentSpot", ""+e);
			}
			
		}
		
	}

	public class AddNewReportSpot extends AsyncTask<String, String, String> {

		boolean flag = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ReportActivity.this);
			pDialog.setMessage("Adding Information..");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		protected String doInBackground(String... args) {
			String title="",description="",time="",date="",impact="";
			
				try {
				title = titleEditText.getText().toString();
				description = descriptionEditText.getText().toString();
				time = timeEditText.getText().toString();
				date = dateEditText.getText().toString();
				impact = impactEditText.getText().toString();
				
				} catch (Exception e) {
					// TODO: handle exception
					Log.e("GetTextBack", ""+e);
				}		
				

				
				
		    try {
		    	if (title.length() == 0 || description.length() == 0
						|| time.length() == 0 || date.length() == 0
						|| impact.length() == 0 || locality.length() == 0
						|| String.valueOf(latitude).length() == 0
						|| String.valueOf(longitude).length() == 0) {
					publishProgress();
					return null;
				}
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("publishProgress_call", ""+e);
			}

			

			
			  
				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				
				params.add(new BasicNameValuePair("image_n", image));
				params.add(new BasicNameValuePair("image", encodedString));
				params.add(new BasicNameValuePair("latitude", String
						.valueOf(latitude)));
				params.add(new BasicNameValuePair("longitude", String
						.valueOf(longitude)));
				params.add(new BasicNameValuePair("title", title));
				params.add(new BasicNameValuePair("reportCategoryId", String.valueOf(reportCategoryId)));
				params.add(new BasicNameValuePair("severity", severityLevel));
				params.add(new BasicNameValuePair("reportSource", reportSource));
				params.add(new BasicNameValuePair("description", description));
				params.add(new BasicNameValuePair("impact", impact));
				params.add(new BasicNameValuePair("time", time));
				params.add(new BasicNameValuePair("date", date));
				int userId=getUserIdFromSharedPref();
				params.add(new BasicNameValuePair("user_id", ""+userId));
				//params.add(new BasicNameValuePair("submission_date", formatted));
				params.add(new BasicNameValuePair("locality", locality));

				// getting JSON Object
				// Note that create product url accepts POST method
				
				// check for success tag
				try {
					JSONObject json = jsonParser.makeHttpRequest(
							POST_URL, "POST", params);
					int success = json.getInt("success");
					serverResponse = json.getString("message");
					if (success == 1) {
						flag = true;
					} else {
						
						
					}
				} catch (JSONException e) {
					Log.e("JS_OB_SUCC", ""+e);
				}

			
			
			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
			Toast.makeText(ReportActivity.this,
					"Required field(s) is missing From Client End", Toast.LENGTH_SHORT)
					.show();

		}
		
		protected void onPostExecute(String string) {
			pDialog.dismiss();
			if (flag) {
				Toast.makeText(ReportActivity.this, serverResponse,
						Toast.LENGTH_SHORT).show();
			}
		
			
		}

	}
	
	public Void inputDialog(String preValue,String titleDialog,String Hint,final int swtichValue) {
		
		
		
		
		
		
		LayoutInflater inflater = LayoutInflater
				.from(ReportActivity.this);
		View view = inflater.inflate(R.layout.search_layout,
				null);

		final EditText searchET = (EditText) view
				.findViewById(R.id.etSearch);
		searchET.setHint(Hint);
		searchET.setText(preValue);
		searchET.setSelection(searchET.getText().length());
		
		AlertDialog.Builder builder=new AlertDialog.Builder(ReportActivity.this);
		builder.setTitle(titleDialog);
		builder.setView(view);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				value = searchET.getText()
						.toString();
				switch (swtichValue) {
				case 1:
					titleEditText.setText(value);
					break;
					
				case 2:
					descriptionEditText.setText(value);
					break;

				case 3:
					impactEditText.setText(value);
					break;
				}
				
				
				
				
				
				
				
				
				
			}
		});
		
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		AlertDialog simpleDialog = builder.create();
		simpleDialog.show();
		return null;
		
		
		
		
		
	
		
	}
	
	 private Integer getUserIdFromSharedPref() {
			
			int userID=pref_user_id.getInt(U_ID, -1);
			
			return userID;
			
		
		}
	

}
