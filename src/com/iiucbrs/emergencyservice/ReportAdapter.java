package com.iiucbrs.emergencyservice;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ReportAdapter extends BaseAdapter {
	LayoutInflater inflater;
	Context context;

	ArrayList<String> categoryTitle;
	ArrayList<String> category_image;

	public ReportAdapter(Context context, ArrayList<String> categoryTitle,
			ArrayList<String> category_image) {
		this.context = context;
		this.categoryTitle = categoryTitle;
		this.category_image = category_image;
		inflater = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return categoryTitle.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return categoryTitle.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.reportcustom, null);
			holder.img = (ImageView) convertView.findViewById(R.id.imgCustom);
			holder.txt = (TextView) convertView.findViewById(R.id.txtText);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.txt.setText(categoryTitle.get(position));
		int resourceID = context.getResources().getIdentifier(
				category_image.get(position), "drawable",
				context.getPackageName());
		holder.img.setImageResource(resourceID);
		return convertView;
	}

	public static class ViewHolder {
		ImageView img;
		TextView txt;
	}

}
