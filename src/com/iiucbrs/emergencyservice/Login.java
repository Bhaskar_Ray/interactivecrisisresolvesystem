package com.iiucbrs.emergencyservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.iiucbrs.emergencyservice.Registration.GcmReg;
import com.iiucbrs.emergencyservice.Registration.RegUserAsync;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {
	ActionBar actionBar;
	LinearLayout regLayout;
	ProgressDialog pDialog;
	GoogleCloudMessaging gcmObj;
	EditText logEmail,logPassword;
	Button login;
	SharedPreferences pref_gcm;    
	SharedPreferences.Editor editor_gcm;
	SharedPreferences pref_user_id;
	SharedPreferences.Editor editor_user_id;
	public static final String GCM_ID = "gcmId";
	public static final String U_ID = "user_id";
	public static final String U_NAME = "Login_user";
	Boolean connected;
	Context applicationContext;
	String strEmail,strPassword;
	String checkGCMinShared;
	String gcmId = "";
	private AsyncTask<String, String, String> createLoginTask;
	private AsyncTask<String, String, String> createGcmIdTask;
	private JSONParser jsonParser = new JSONParser();
	private JSONArray json_login;
	ArrayList<Integer> arrayList_user_id = new ArrayList<Integer>();
	ArrayList<String> arrayList_user_name = new ArrayList<String>();
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	static final String GOOGLE_PROJ_ID = "994721534063";
	public static final String LOGIN_URL = "http://codencrayon.com/crisis_resolve/logUser.php";
	//public static final String LOGIN_URL = "http://192.168.43.191/myserver/logUser.php";
	private String serverResponse = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		actionBar = getActionBar();
		actionBar.hide();
		logEmail=(EditText) findViewById(R.id.edtLogEmail);
		logPassword=(EditText) findViewById(R.id.edtLogPassword);
		login=(Button) findViewById(R.id.btnLog);
		pref_gcm = getSharedPreferences("GCM_REG",
				Context.MODE_PRIVATE);
		pref_user_id= getSharedPreferences("USER_ID",
				Context.MODE_PRIVATE);
		applicationContext=getApplicationContext();
		login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int id=getUserIdFromSharedPref();
				String checkUser=getUserNameFromSharedPref();
				if(id==-1&&checkUser.equals("no_login"))
				{
					
					
					
					strEmail=logEmail.getText().toString();
					strPassword=logPassword.getText().toString();
					checkGCMinShared=getgcmIdFromSharedPref();
					if(checkGCMinShared.equals("No"))
					{
						if(!strEmail.isEmpty()&&!strPassword.isEmpty())
						{
							
							try {
								if(checkPlayServices())
								{
									runGcmReg();
								}
								
							} catch (Exception e) {
								// TODO: handle exception
								Log.e("runGcmReg", e.getMessage());
							}
						
						}
						else
						{
							Toast.makeText(applicationContext, "Information missing!", Toast.LENGTH_SHORT).show();
						}
						
					}
					else
					{
						if(!checkGCMinShared.equals("No")&&!checkGCMinShared.isEmpty())
						{
							if(!strEmail.isEmpty()&&!strPassword.isEmpty())
							{
								
								try {
									runLogUserAsync();
								} catch (Exception e) {
									// TODO: handle exception
									Log.e("runLogUserAsync", e.getMessage());
								}
							
							}
							else
							{
								Toast.makeText(applicationContext, "Information missing!", Toast.LENGTH_SHORT).show();
							}
							
						}
						else
						{
							Toast.makeText(applicationContext, "Invalid Gcm id!!", Toast.LENGTH_SHORT).show();
						}
						
					
						
						
					}
			
					
					
				}
				else
				{
					

					
					
					Toast.makeText(applicationContext, "Already Logged in!", Toast.LENGTH_SHORT).show();
					
				
					
				}
				
				
				
			}
		});
		
		regLayout=(LinearLayout) findViewById(R.id.layoutReg);
		regLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in=new Intent(Login.this,Registration.class);
				startActivity(in);
				
				
			}
		});
	
	}
	
private void storeUserIDinSharedPref(Context context, int uId,String login_user) {
		
		
		try {
			
			
			if(uId!=0&&!login_user.isEmpty())
			{
				editor_user_id = pref_user_id.edit();
				editor_user_id.putInt(U_ID, uId);
				editor_user_id.putString(U_NAME, login_user);
				editor_user_id.commit();
				Toast.makeText(applicationContext, "user_id strored", Toast.LENGTH_SHORT).show();
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("storeUserIDinSharedPref", e.getMessage());
		}
		
		
}


private void runGcmReg() {
	checkIConnection();
	if (connected) {
		if (createGcmIdTask == null) {
			// --- create a new task --
			createGcmIdTask = new GcmReg();
			createGcmIdTask.execute();
		} else if (createGcmIdTask.getStatus() == AsyncTask.Status.FINISHED) {
			createGcmIdTask = new GcmReg();
			createGcmIdTask.execute();
		} else if (createGcmIdTask.getStatus() == AsyncTask.Status.RUNNING) {
			createGcmIdTask.cancel(false);
			createGcmIdTask = new GcmReg();
			createGcmIdTask.execute();
		}
	} else {
		Toast.makeText(this, "Internet connection not available",
				Toast.LENGTH_SHORT).show();
	}
}

class GcmReg extends AsyncTask<String, String, String> {
	
	Boolean flag=false;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(Login.this);
		pDialog.setMessage("GCM registration on going please wait...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);

		pDialog.show();
	}

	protected String doInBackground(String... args) {

		
		
		
		try {
			if (gcmObj == null) {
				gcmObj = GoogleCloudMessaging
						.getInstance(Login.this);
			}
			gcmId = gcmObj
					.register(GOOGLE_PROJ_ID);
			

		} catch (IOException ex) {
			Log.e("GCM_REG", ex.getMessage());
		}
		return gcmId;

		
	}

	protected void onPostExecute(String id_gcm) {
		pDialog.dismiss();
		
		Toast.makeText(
				Login.this,
				"Message: "
						+ id_gcm, Toast.LENGTH_SHORT).show();
		if(!id_gcm.isEmpty())
		{
			storegcmIdinSharedPref(applicationContext, id_gcm);
		}
		else
		{
			Toast.makeText(
					Login.this,
					"GCM id is null"+"\n"+"Can't store"+"\n"+"Try again!", Toast.LENGTH_SHORT).show();
		}
		
		checkGCMinShared=getgcmIdFromSharedPref();
		runLogUserAsync();
		
		

	}

}



private void runLogUserAsync() {
	checkIConnection();
	if (connected) {
		if (createLoginTask == null) {
			// --- create a new task --
			createLoginTask = new LogUserAsync();
			createLoginTask.execute();
		} else if (createLoginTask.getStatus() == AsyncTask.Status.FINISHED) {
			createLoginTask = new LogUserAsync();
			createLoginTask.execute();
		} else if (createLoginTask.getStatus() == AsyncTask.Status.RUNNING) {
			createLoginTask.cancel(false);
			createLoginTask = new LogUserAsync();
			createLoginTask.execute();
		}
	} else {
		Toast.makeText(this, "Internet connection not available",
				Toast.LENGTH_SHORT).show();
	}
}



class LogUserAsync extends AsyncTask<String, String, String> {
	
	Boolean flag=false;
	Boolean gcmUpadteFlag=false;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(Login.this);
		pDialog.setMessage("Login process ongoing please wait...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);

		pDialog.show();
	}

	protected String doInBackground(String... args) {

		try {
			
	    arrayList_user_id.clear();
	    arrayList_user_name.clear();
			
		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("email",strEmail));
		params.add(new BasicNameValuePair("password", strPassword));
		params.add(new BasicNameValuePair("gcm_id",checkGCMinShared));
		// getting JSON string from URL
		jsonParser = new JSONParser();
		JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST",
				params);
		
			// Checking for SUCCESS TAG

			int success = json.getInt("success");
			serverResponse=json.getString("message");
			int gcmUpdate=json.getInt("gcm_update");
			if(gcmUpdate == 1)
			{
				gcmUpadteFlag=true;
				
			}
			if (success == 1) {
				
				flag=true;
				//To-Do------------------
				

				json_login = json.getJSONArray("user_login");

				for (int i = 0; i < json_login.length(); i++) {
					JSONObject c = json_login.getJSONObject(i);

					
					
					arrayList_user_id.add(c.getInt("user_id"));
					arrayList_user_name.add(c.getString("user_name"));
					
					

				}
				

			
				
				
				
			} else {
				
				
				

			}
		} catch (JSONException e) {
			Log.e("Login_Success", ""+e);
		}

		return null;
	}

	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		
		if(flag)
		{
			
			if(gcmUpadteFlag)
			{
				Toast.makeText(applicationContext,"Gcm id updated!" , Toast.LENGTH_LONG).show();
			}
			Toast.makeText(applicationContext,serverResponse , Toast.LENGTH_LONG).show();
			Toast.makeText(
					applicationContext,
					"UserID_From_Server: " + arrayList_user_id.get(0)
							+ "\n" + "UserName_From_Server: "
							+ arrayList_user_name.get(0), Toast.LENGTH_LONG)
					.show();
			
			try {
				
				storeUserIDinSharedPref(applicationContext, arrayList_user_id.get(0),arrayList_user_name.get(0));
				int id=getUserIdFromSharedPref();
				String login_u=getUserNameFromSharedPref();
				Toast.makeText(applicationContext,"UserID_From_Shared: "+id+"\n"+"UserName_From_Shared: "+login_u , Toast.LENGTH_LONG).show();
				if(id!=-1&&!login_u.isEmpty())
				{
					Intent in=new Intent(Login.this,WeatherActivity.class);
					startActivity(in);
					finish();
					
				}
				

			

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("storeUserIDinSharedPref", "" + e);
			}
		}
		else
		{
			Toast.makeText(getApplicationContext(),serverResponse , Toast.LENGTH_LONG).show();
		}
		
		
		
		
		

	}

}

private void storegcmIdinSharedPref(Context context, String gcmId) {
	
	
	try {
		
		
		editor_gcm = pref_gcm.edit();
		editor_gcm.putString(GCM_ID, gcmId);
		editor_gcm.commit();
		Toast.makeText(applicationContext, "GCM_ID strored", Toast.LENGTH_SHORT).show();
		
	} catch (Exception e) {
		// TODO: handle exception
		Log.e("storegcmIdinSharedPref", e.getMessage());
	}
	
	

}

	
    private String getgcmIdFromSharedPref() {
		
		String gcmFromShared=pref_gcm.getString(GCM_ID, "No");
		
		return gcmFromShared;
		

	}
    
   private Integer getUserIdFromSharedPref() {
		
		int userID=pref_user_id.getInt(U_ID, -1);
		
		return userID;
		
	
	}
   
   private String getUserNameFromSharedPref() {
		
		String userName=pref_user_id.getString(U_NAME, "no_login");
		
		return userName;
		
	
	}
   
	public void checkIConnection() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState() == NetworkInfo.State.CONNECTED
				|| connectivityManager.getNetworkInfo(
						ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
			connected = true;
			
		} else {
			connected = false;
			Toast.makeText(getApplicationContext(),
					"No internet connection available", Toast.LENGTH_LONG)
					.show();
		}
		
		
		
	}
	
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		// When Play services not found in device
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				// Show Error dialog to install Play services
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Toast.makeText(
						Login.this,
						"This device doesn't support Play services, App will not work normally",
						Toast.LENGTH_LONG).show();
				finish();
			}
			return false;
		} else {

			
//			Toast.makeText(
//					Registration.this,
//					"This device supports Play services, App will work normally",
//					Toast.LENGTH_LONG).show();
		
		}
		return true;
	}
   

}
