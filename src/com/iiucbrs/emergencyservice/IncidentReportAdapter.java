package com.iiucbrs.emergencyservice;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class IncidentReportAdapter extends BaseAdapter {
	LayoutInflater inflater;
	Context context;

	ArrayList<String> IncidentTitle;
	ArrayList<String> IncidentLocality;
	ArrayList<String> Incident_image;

	public IncidentReportAdapter(Context context, ArrayList<String> IncidentTitle,ArrayList<String> IncidentLocality,
			ArrayList<String> Incident_image) {
		this.context = context;
		this.IncidentTitle = IncidentTitle;
		this.IncidentLocality = IncidentLocality;
		this.Incident_image = Incident_image;
		inflater = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return IncidentTitle.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return IncidentTitle.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.customsos, null);
			holder.img = (ImageView) convertView.findViewById(R.id.imgCustom);
			holder.txt = (TextView) convertView.findViewById(R.id.txtText);
			holder.txt2 = (TextView) convertView.findViewById(R.id.txtText2);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.txt.setText(IncidentTitle.get(position));
		holder.txt2.setText(IncidentLocality.get(position));
		int resourceID = context.getResources().getIdentifier(
				Incident_image.get(position), "drawable",
				context.getPackageName());
		holder.img.setImageResource(resourceID);
		return convertView;
	}

	public static class ViewHolder {
		ImageView img;
		TextView txt;
		TextView txt2;
	}

}
