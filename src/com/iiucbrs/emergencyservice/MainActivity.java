package com.iiucbrs.emergencyservice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	List<String>divisonList;
	String[]zillaString;
	ListView list;

	
//	Spinner zilla;
//	Button submit;
	String selectedZilla;
	ArrayList<String> Shelter_Name = new ArrayList<String>();
	ArrayList<String> Shelter_Lat = new ArrayList<String>();
	ArrayList<String> Shelter_Lon = new ArrayList<String>();
	JSONParser jParser = new JSONParser();
	boolean availableProduct=false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		list = (ListView) findViewById(R.id.listCyclone);
//		submit=(Button)findViewById(R.id.btnSubmit);
//		zilla = (Spinner) findViewById(R.id.zilla);
		final String[] zillaString = getResources().getStringArray(
				R.array.zilla);
		divisonList=Arrays.asList(zillaString);
		CustomCycloneListView adapter = new CustomCycloneListView(getApplicationContext(),
				divisonList);
			list.setAdapter(adapter);
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view, int pos,
						long id) {
					// TODO Auto-generated method stub
					selectedZilla = zillaString[pos];
       			    selectedZilla=selectedZilla.toLowerCase();
       			 new AsyncTaskRunnerCurrent().execute();
					
					

				}
			});
//		zilla.setAdapter(new ArrayAdapter<String>(this,
//				android.R.layout.simple_list_item_1, zillaString));

//		zilla.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view,
//					int position, long id) {
//				// TODO Auto-generated method stub
//				selectedZilla = zillaString[position];
//				selectedZilla=selectedZilla.toLowerCase();
////				Toast.makeText(getApplicationContext(),
////						"you Selected " + selectedZilla, Toast.LENGTH_LONG)
////						.show();
//
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) {
//				// TODO Auto-generated method stub
//
//			}
//		});
		
		
//		submit.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				new AsyncTaskRunnerCurrent().execute();
//			}
//		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	

	//----------- Fetching Current Project
	class AsyncTaskRunnerCurrent extends AsyncTask<String, String, String>
	{
		 ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
		
		String error="";
		
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			try
			{
				
			

				List<NameValuePair> pair= new ArrayList<NameValuePair>();
				pair.add(new BasicNameValuePair("tableName", selectedZilla));
				
				
			//JSONObject json=jParser.makeHttpRequest("http://everestdiners.com/market/get_product_name.php", "GET", pair)	;
				JSONObject json=jParser.makeHttpRequest("http://emergencyservice.codencrayon.com/cyclone/getdata.php", "GET", pair)	;
	
				Shelter_Name.clear();
				Shelter_Lat.clear();
				Shelter_Lon.clear();
				
				int success=json.getInt("success");
				
				if(success==1)
				{
					
					
				JSONArray	 shelter= json.getJSONArray("shelter");
					
					
					for(int i=0;i<shelter.length();i++)
					{
						JSONObject item=shelter.getJSONObject(i);
						
						
						Shelter_Name.add(item.getString("shelter_name"));
						Shelter_Lat.add(item.getString("shelter_lat"));
						Shelter_Lon.add(item.getString("shelter_lon"));
					}
					
					
			         availableProduct=true;		
					
					
				}
				else
				{
					//Toast.makeText(getApplicationContext(), "Sorry a problem occured", Toast.LENGTH_LONG).show();
				Log.d("NOOOOOOOOOOOOOOOOOOOOOOOOOOO", "NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
				 availableProduct=false;
				}
				
				
				
				
			}
			catch(Exception e)
			{
				//Toast.makeText(getApplicationContext(),"A problem occured. Please restart the application once again", Toast.LENGTH_LONG).show();
			
			  Log.d("Problemasdddddddddddddd", e+"");
			  error =e+"";
			}
			return null;
		}
		
		
		
		protected void onPostExecute(String string)
		{
			
			progressDialog.dismiss();
			
			if(availableProduct==true)
			{
				 Toast.makeText(getApplicationContext(), Shelter_Name.get(0),Toast.LENGTH_LONG).show();
				 HashMap<String,ArrayList> shelter_hash=new HashMap<String,ArrayList>();
				 shelter_hash.put("Shelter_Name", Shelter_Name);
				 shelter_hash.put("Shelter_Lat", Shelter_Lat);
				 shelter_hash.put("Shelter_Lon", Shelter_Lon);
				 Intent in=new Intent(MainActivity.this,MapClass.class);
				 in.putExtra("key", shelter_hash);
				 in.putExtra("shelter_hash", shelter_hash);
				 startActivity(in);
			}
			
			else
			{
				Toast.makeText(getApplicationContext(), "There is no available data"+"\n"+"Check Your Internet Connection", Toast.LENGTH_LONG).show();
			}
		
		}
		
		protected void onPreExecute()
		{
			
			progressDialog.setMessage("Please wait. Loading..");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}
		
		
		
		
			}
	
	
	
	//------------------------------------------------------------------


	
	
	
	

}
