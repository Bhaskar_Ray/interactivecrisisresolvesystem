package com.iiucbrs.emergencyservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapReport extends FragmentActivity implements
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener, OnClickListener, LocationListener {

	ImageView actionImageView, actionImageView_manu;
	Button manuButton;
	TextView actionTextview;
	ActionBar actionBar;
	Typeface tf;
	String type;
	ImageView snapshot;
	public static final String INCIDENT_URL = "http://codencrayon.com/crisis_resolve/get_all_incidents.php";
	//public static final String INCIDENT_URL = "http://192.168.43.191/myserver/get_all_incidents.php";
	Boolean connected;
	private AsyncTask<String, String, String> asyncTask;

	private static final int GPS_ERRORDIALOG_REQUEST = 9001;
	private static final float DEFAULTZOOM_REPORT = 6.5f;
	private static final float Dhaka_LAT = 23.777176f;
	private static final float Dhaka_LNG = 90.399452f;
	private ProgressDialog pDialog;
	private JSONParser jsonParser = new JSONParser();
	private JSONArray incidents;
	//double currentLatitude_mapreport;
	//double currentLongitude_mapreport;
    Location currentLocation_mapreport;
	private LocationRequest mLocationRequest_mapreport;
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	ArrayList<Integer> incident_id = new ArrayList<Integer>();
	ArrayList<String> incident_title = new ArrayList<String>();
	ArrayList<String> incident_lat = new ArrayList<String>();
	ArrayList<String> incident_lon = new ArrayList<String>();
	ArrayList<String> incident_icon = new ArrayList<String>();
	ArrayList<String> incident_locality = new ArrayList<String>();

	private com.google.android.gms.maps.GoogleMap rMap;
	private Marker mMarker;

	private GoogleApiClient apiClient_map_report;

	private Button find;
	private Button myLocationButton;
	private Button refreshButton;
	private double currentMarkerLat;
	private double currentMarkerLng;
	private String locality = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//actionTextview = (TextView) findViewById(R.id.actionTextView);

		actionBar = getActionBar();
		actionBar.hide();

		// tf = Typeface.createFromAsset(this.getAssets(),
		// "Agatha-Regular.ttf");
		// Log.i("test", tf.toString());
		// actionTextview.setTypeface(tf, Typeface.BOLD);
		// type=getIntent().getExtras().getString("types");

		if (servicesOK()) {
			setContentView(R.layout.activity_report_map);

			find = (Button) findViewById(R.id.find_Button);
			myLocationButton = (Button) findViewById(R.id.myLocation_Button);
			refreshButton = (Button) findViewById(R.id.refreshButton);
			manuButton = (Button) findViewById(R.id.menu_Button);
			actionImageView = (ImageView) findViewById(R.id.snapshotImage);
			
			mLocationRequest_mapreport = LocationRequest.create()
			        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
			        .setInterval(10 * 1000)        // 10 seconds, in milliseconds
			        .setFastestInterval(1 * 1000); // 1 second, in milliseconds

			actionImageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						CaptureMapScreen();
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}

				}
			});
			find.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						LayoutInflater inflater = LayoutInflater
								.from(MapReport.this);
						View view = inflater.inflate(R.layout.search_layout,
								null);

						final EditText searchET = (EditText) view
								.findViewById(R.id.etSearch);

						AlertDialog.Builder builder = new AlertDialog.Builder(
								MapReport.this);

						builder.setTitle("Find Location");
						builder.setView(view);

						builder.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {

										String location = searchET.getText()
												.toString();
										

										Geocoder gc = new Geocoder(
												MapReport.this);
										List<Address> addresses = null;
										try {
											addresses = gc.getFromLocationName(
													location, 1);
										} catch (IOException e) {
											e.printStackTrace();
											Log.e("Find_Address", ""+e);
											return;
										}
										try {
											Address address = addresses.get(0);
											locality = address.getLocality();
											Toast.makeText(
													getApplicationContext(),
													locality,
													Toast.LENGTH_SHORT).show();
											gotoLocation(address.getLatitude(),
													address.getLongitude(),
													12.0f);
											// setMarkers(address.getLatitude(),
											// address.getLongitude(),
											// R.drawable.find_marker);
											// currentMarkerLat =
											// address.getLatitude();
											// currentMarkerLng =
											// address.getLongitude();
										} catch (Exception e) {
											e.printStackTrace();
											Log.e("Find_Locality", ""+e);
											Toast.makeText(MapReport.this,
													"Location not found",
													Toast.LENGTH_SHORT).show();
											return;
										}
										try {
											if (rMap != null) {
												rMap.clear();
											}
										} catch (Exception e) {
											// TODO: handle exception
											Log.e("Map_Clear_find", ""+e);
										}
										try {
											if (!locality.equals("")) {
												checkIConnection();
												if (connected)
													runLoadAllIncident();
											}
										} catch (Exception e) {
											// TODO: handle exception
											Log.e("runLoadAllIncident", ""+e);
										}
										

									}
								});
						builder.setNeutralButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
									}
								});
						AlertDialog simpleDialog = builder.create();
						simpleDialog.show();

					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(getApplicationContext(),
								"Check Your Internet Connection",
								Toast.LENGTH_LONG).show();
					}

				}
			});

			myLocationButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
						currentLocation_mapreport = LocationServices.FusedLocationApi
								.getLastLocation(apiClient_map_report);
						if (currentLocation_mapreport == null) {
							Toast.makeText(getApplicationContext(),
									"Current location isn't available",
									Toast.LENGTH_SHORT).show();
						} else {

							locality = getLocality(
									currentLocation_mapreport.getLatitude(),
									currentLocation_mapreport.getLongitude());
							Toast.makeText(getApplicationContext(), locality,
									Toast.LENGTH_LONG).show();
							currentMarkerLat = currentLocation_mapreport.getLatitude();
							currentMarkerLng = currentLocation_mapreport.getLongitude();
							gotoLocation(currentLocation_mapreport.getLatitude(),
									currentLocation_mapreport.getLongitude(), 12.0f);

							if (rMap != null) {
								rMap.clear();

							}
							if (!locality.equals("")) {
								checkIConnection();
								if (connected)
									runLoadAllIncident();

							}

							setMarkers("You are here", "",
									currentLocation_mapreport.getLatitude(),
									currentLocation_mapreport.getLongitude(),
									R.drawable.me);
						}

					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(
								getApplicationContext(),
								"Location not found" + "\n"
										+ "Check Your Location Settings",
								Toast.LENGTH_LONG).show();
					}

				}
			});

			refreshButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// runLoadAllCrimeSpots();
					// locality="";
					// checkIConnection();
					// if(connected)
					// runLoadAllIncident();

					AlertDialog.Builder builder = new AlertDialog.Builder(
							MapReport.this);
					LayoutInflater inflater = LayoutInflater
							.from(MapReport.this);
					View view = inflater.inflate(R.layout.incidentlist, null);
					builder.setCancelable(true);
					builder.setView(view);
					ListView list = (ListView) view
							.findViewById(R.id.listIncident);
					Button ok=(Button) view.findViewById(R.id.btnIncidentList);
					IncidentReportAdapter adapter = new IncidentReportAdapter(
							MapReport.this, incident_title, incident_locality,
							incident_icon);
					list.setAdapter(adapter);
					final AlertDialog dg = builder.create();
					list.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							Intent in = new Intent(MapReport.this,
									ReportDetails.class);
							in.putExtra("incident_id",
									incident_id.get(position));
							startActivity(in);
							dg.dismiss();

						}
					});
					
					//final AlertDialog dg = builder.create();
					dg.show();
                    ok.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dg.cancel();
						}
					});
					

				}

			});

			manuButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					final String mapTypes[] = { "None", "Normal", "Satellite",
							"Terrain", "Hybrid" };
					AlertDialog.Builder builder = new AlertDialog.Builder(
							MapReport.this);
					builder.setTitle("Set Map Type");
					builder.setCancelable(false);
					builder.setItems(mapTypes,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int possition) {
									switch (possition) {
									case 0:
										rMap.setMapType(GoogleMap.MAP_TYPE_NONE);
										break;

									case 1:
										rMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
										break;

									case 2:
										rMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
										break;

									case 3:
										rMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
										break;

									case 4:
										rMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
										break;

									default:
										break;
									}
								}
							});
					builder.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int possition) {
									dialog.dismiss();
								}
							});
					AlertDialog mapTypeDialog = builder.create();
					mapTypeDialog.show();

				}
			});

			if (initMap()) {
				gotoLocation(Dhaka_LAT, Dhaka_LNG, DEFAULTZOOM_REPORT);
				apiClient_map_report = new GoogleApiClient.Builder(this)
						.addApi(LocationServices.API)
						.addConnectionCallbacks(this)
						.addOnConnectionFailedListener(this).build();
				apiClient_map_report.connect();
				checkIConnection();
				// if(connected)
				// new LoadAllIncident().execute();
				runLoadAllIncident();
			} else {
				Toast.makeText(this, "Map not available!", Toast.LENGTH_SHORT)
						.show();
			}
		} else {
			Toast.makeText(this, "Google Play Services is not available",
					Toast.LENGTH_LONG).show();
			finish();
		}
		// datasource = new CrimeDatasource(this);
		// datasource.open();
	}

	private boolean initMap() {
		if (rMap == null) {
			SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.mapReport);
			rMap = mapFrag.getMap();

			if (rMap != null) {

				rMap.setInfoWindowAdapter(new InfoWindowAdapter() {

					@Override
					public View getInfoWindow(Marker marker) {

						if (marker.getPosition().latitude == currentMarkerLat
								&& marker.getPosition().longitude == currentMarkerLng) {
							return null;
						}
						int i = Integer.parseInt(marker.getSnippet());
						// int latIndex = incident_lat.indexOf(marker
						// .getPosition());
						// Toast.makeText(getApplicationContext(), "Index: "+i,
						// Toast.LENGTH_LONG).show();

						View view = getLayoutInflater().inflate(
								R.layout.infowindow, null);

						ImageView crimeIcon = (ImageView) view
								.findViewById(R.id.crime_ic);
						TextView incidentType = (TextView) view
								.findViewById(R.id.incident_type);
						TextView incidentLocality = (TextView) view
								.findViewById(R.id.incident_locality);

						incidentType.setText(incident_title.get(i));
						incidentLocality.setText(incident_locality.get(i));
						// Toast.makeText(getApplicationContext(),
						// "Locality: "+incident_locality.get(i),
						// Toast.LENGTH_LONG).show();

						int resourceID = getResources().getIdentifier(
								incident_icon.get(i), "drawable",
								getPackageName());
						crimeIcon.setImageResource(resourceID);

						return view;
					}

					@Override
					public View getInfoContents(Marker marker) {
						return null;
					}
				});

				rMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

					@Override
					public void onInfoWindowClick(Marker marker) {
						
						//To-Do
						Intent in = new Intent(MapReport.this,
								ReportDetails.class);
						int i = Integer.parseInt(marker.getSnippet());
						in.putExtra("incident_id",
								incident_id.get(i));
						startActivity(in);

					}
				});

				//
				// mMap.setOnMapLongClickListener(new OnMapLongClickListener() {
				//
				// @Override
				// public void onMapLongClick(LatLng ll) {
				// Intent intent = new Intent(GoogleMap.this,
				// AddNewSpotActivity.class);
				// startActivity(intent);
				// }
				// });

				rMap.setOnMapLongClickListener(new OnMapLongClickListener() {

					@Override
					public void onMapLongClick(LatLng lng) {
						Intent intent = new Intent(MapReport.this,
								ReportActivity.class);
						intent.putExtra("lat", lng.latitude);
						intent.putExtra("lng", lng.longitude);
						startActivity(intent);
					}
				});

				rMap.setOnMarkerClickListener(new OnMarkerClickListener() {

					@Override
					public boolean onMarkerClick(Marker marker) {
						try {

						} catch (Exception e) {
							// TODO: handle exception

							Toast.makeText(getApplicationContext(),
									"Check your Location Access",
									Toast.LENGTH_LONG).show();

						}

						return false;
					}
				});
			}
		}
		return (rMap != null);
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}

	public boolean servicesOK() {
		int isAvailable = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		if (isAvailable == ConnectionResult.SUCCESS) {
			return true;
		} else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable)) {
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isAvailable,
					this, GPS_ERRORDIALOG_REQUEST);
			dialog.show();
		} else {
			Toast.makeText(this, "Can't connect to Google Play services",
					Toast.LENGTH_SHORT).show();
		}
		return false;
	}

	private void gotoLocation(double lat, double lng, float zoom) {
		try {
			LatLng ll = new LatLng(lat, lng);
			CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
			rMap.animateCamera(update);
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("gotoLocation", ""+e);
		}
		
	}

	public void geoLocate(View v) throws IOException {

	}

	private void setMarkers(String locality, String countryName, double lat,
			double lng, int markerIconRes) {

		if (mMarker != null) {
			mMarker.remove();
		}
		MarkerOptions options = new MarkerOptions().title(locality)
				.position(new LatLng(lat, lng))
				.icon(BitmapDescriptorFactory.fromResource(markerIconRes));

		if (markerIconRes == 0) {
			options.icon(BitmapDescriptorFactory.defaultMarker());
		}

		if (countryName.length() > 0) {
			options.snippet(countryName);
		}

		mMarker = rMap.addMarker(options);
	}

	// private void hideSoftKeyboard(View v) {
	// InputMethodManager imm = (InputMethodManager)
	// getSystemService(INPUT_METHOD_SERVICE);
	// imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
	// }

	@Override
	protected void onResume() {
		super.onResume();
		// datasource.open();
		// MapStateManager mgr = new MapStateManager(this);
		// CameraPosition position = mgr.getSavedCameraPosition();
		// if (position != null) {
		// CameraUpdate update = CameraUpdateFactory
		// .newCameraPosition(position);
		// mMap.moveCamera(update);
		// mMap.setMapType(mgr.getSavedMapType());
		// }
		
		try {
			apiClient_map_report.connect();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("onResume", e.getMessage());
		}
		
	}

	@Override
	protected void onPause() {
		super.onPause();
		// datasource.close();
		try {
			if (apiClient_map_report.isConnected()) {
				LocationServices.FusedLocationApi.removeLocationUpdates(apiClient_map_report, this);
		        apiClient_map_report.disconnect();
		    }
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("onPause", e.getMessage());
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
//		MapStateManager mgr = new MapStateManager(MapReport.this);
//		mgr.saveMapState(rMap);
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		
		 if (connectionResult.hasResolution()) {
		        try {
		            // Start an Activity that tries to resolve the error
		            connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
		        } catch (IntentSender.SendIntentException e) {
		            e.printStackTrace();
		        }
		    } else {
		        Log.i("MPR_onConnectionFailed", "Location services connection failed with code " + connectionResult.getErrorCode());
		    }
	}

	@Override
	public void onConnected(Bundle arg0) {

		 Log.i("onConnected_mapacti", "Location services connected.");
		 try {
			 
			 currentLocation_mapreport = LocationServices.FusedLocationApi.getLastLocation(apiClient_map_report);
			 
			 if (currentLocation_mapreport == null) {
			        LocationServices.FusedLocationApi.requestLocationUpdates(apiClient_map_report, mLocationRequest_mapreport, this);
			    }
			    else {
			        //handleNewLocation(location);
		    	 LocationServices.FusedLocationApi.requestLocationUpdates(apiClient_map_report, mLocationRequest_mapreport, this);
//			    	 handleNewLocation(currentLocation);
			    }
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("onConnected", e.getMessage());
		}
		 
	
		
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		
		  Log.i("onConnectionSuspended", "Location services suspended. Please reconnect.");
		
	}
//
//	private LatLng getCurrentLocation() {
//		Location currentLocation = LocationServices.FusedLocationApi
//				.getLastLocation(apiClient_map_report);
//		return new LatLng(currentLocation.getLatitude(),
//				currentLocation.getLongitude());
//	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

	public void CaptureMapScreen() {
		SnapshotReadyCallback callback = new SnapshotReadyCallback() {
			Bitmap bitmap;

			@Override
			public void onSnapshotReady(Bitmap snapshot) {
				// TODO Auto-generated method stub
				bitmap = snapshot;

				String filePath = Environment.getExternalStorageDirectory()
						+ File.separator + "Pictures/MapScreen.png";
				File imagePath = new File(filePath);
				FileOutputStream fos;
				try {
					fos = new FileOutputStream(imagePath);
					bitmap.compress(CompressFormat.PNG, 100, fos);
					fos.flush();
					fos.close();

				} catch (FileNotFoundException e) {
					Log.e("GREC", e.getMessage(), e);
				} catch (IOException e) {
					Log.e("GREC", e.getMessage(), e);
				}

				// try {
				// FileOutputStream out = new FileOutputStream("/mnt/sdcard/"
				// + "MyMapScreen" + System.currentTimeMillis()
				// + ".png");
				//
				// // above "/mnt ..... png" => is a storage path (where image
				// will be stored) + name of image you can customize as per your
				// Requirement
				//
				// bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
				// } catch (Exception e) {
				// e.printStackTrace();
				// }
				openShareImageDialog(filePath);
			}
		};

		rMap.snapshot(callback);

		// myMap is object of GoogleMap +> GoogleMap myMap;
		// which is initialized in onCreate() =>
		// myMap = ((SupportMapFragment)
		// getSupportFragmentManager().findFragmentById(R.id.map_pass_home_call)).getMap();
	}

	public void openShareImageDialog(String filePath) {
		// File file = this.getFileStreamPath(filePath);

		if (!filePath.equals("")) {
			// final ContentValues values = new ContentValues(2);
			// values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
			// values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
			// final Uri contentUriFile =
			// getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
			// values);

			Intent intent = new Intent(android.content.Intent.ACTION_SEND);
			intent.setType("image/png");
			Uri myUri = Uri.parse("file://" + filePath);
			intent.putExtra(android.content.Intent.EXTRA_STREAM, myUri);
			startActivity(Intent.createChooser(intent, "Share Image"));
		} else {
			// This is a custom class I use to show dialogs...simply replace
			// this with whatever you want to show an error message, Toast, etc.
			Toast.makeText(getApplicationContext(), "Share Image Failed",
					Toast.LENGTH_LONG).show();
		}
	}

	public void checkIConnection() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState() == NetworkInfo.State.CONNECTED
				|| connectivityManager.getNetworkInfo(
						ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
			connected = true;
			// Toast.makeText(getApplicationContext(), "Connected",
			// Toast.LENGTH_LONG).show();
		} else {
			connected = false;
			Toast.makeText(getApplicationContext(),
					"No internet connection available", Toast.LENGTH_LONG)
					.show();
		}
	}

	private String getLocality(double latitude, double longitude) {

		Geocoder gc = new Geocoder(this);
		List<Address> addresses = null;
		try {
			addresses = gc.getFromLocation(latitude, longitude, 1);
			locality = addresses.get(0).getLocality();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return locality;
	}

	private void setIncidentMarkers(double lat, double lng, int markerIconRes,
			int pos) {
		MarkerOptions options = new MarkerOptions().position(
				new LatLng(lat, lng)).icon(
				BitmapDescriptorFactory.fromResource(markerIconRes));

		if (markerIconRes == 0) {
			options.icon(BitmapDescriptorFactory.defaultMarker());
		}
		options.snippet("" + pos);
		rMap.addMarker(options);

	}

	class LoadAllIncident extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(MapReport.this);
			pDialog.setMessage("Loading Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);

			pDialog.show();
		}

		protected String doInBackground(String... args) {

			incident_id.clear();
			incident_title.clear();
			incident_lat.clear();
			incident_lon.clear();
			incident_icon.clear();
			incident_locality.clear();
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("locality", locality));
			// getting JSON string from URL
			jsonParser = new JSONParser();
			JSONObject json = jsonParser.makeHttpRequest(INCIDENT_URL, "GET",
					params);
			try {
				// Checking for SUCCESS TAG

				int success = json.getInt("success");
				if (success == 1) {
					incidents = json.getJSONArray("incident");

					for (int i = 0; i < incidents.length(); i++) {
						JSONObject c = incidents.getJSONObject(i);

						incident_id.add(c.getInt("incident_id"));
						incident_title.add(c.getString("incident_title"));
						incident_lat.add(c.getString("incident_lat"));
						incident_lon.add(c.getString("incident_lon"));
						incident_icon.add(c.getString("icon"));
						incident_locality.add(c.getString("locality"));

					}
					Collections.reverse(incident_id);
					Collections.reverse(incident_title);
					Collections.reverse(incident_lat);
					Collections.reverse(incident_lon);
					Collections.reverse(incident_icon);
					Collections.reverse(incident_locality);

				} else {

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			Log.i("test", "AsynchTask Stopped");
			// updating UI from Background Thread
			// runOnUiThread(new Runnable() {
			// public void run() {
			// Toast.makeText(getApplicationContext(),
			// "Id"+incident_id.get(0)+"\n"+"TITLE"+incident_title.get(0)+"\n"+"LAT"+incident_lat.get(0)+"\n"+"LON"+incident_lon.get(0)+"\n"+"ICON"+incident_icon.get(0)+"\n",
			// Toast.LENGTH_LONG).show();
			if (incident_lat.size() > 0) {
				Toast.makeText(
						MapReport.this,
						incident_lat.size() + " incident report found in "
								+ locality + " Locality", Toast.LENGTH_SHORT)
						.show();
			} else {
				Toast.makeText(MapReport.this,
						"No incident spots found in " + locality + " Locality",
						Toast.LENGTH_SHORT).show();
			}
			for (int i = 0; i < incident_lat.size(); i++) {
				int iconId = getResources().getIdentifier(incident_icon.get(i),
						"drawable", getPackageName());
				// Toast.makeText(MapReport.this,
				// "IconId: "+iconId,
				// Toast.LENGTH_SHORT).show();
				try {

					MapReport.this.setIncidentMarkers(
							Double.parseDouble(incident_lat.get(i)),
							Double.parseDouble(incident_lon.get(i)), iconId, i);
				} catch (Exception e) {
					// TODO: handle exception
					Log.e("MAPRE_MARKER", e.getMessage());
					Toast.makeText(MapReport.this, "Marker not set",
							Toast.LENGTH_SHORT).show();
				}

			}
			// }
			// });

		}

	}

	private void runLoadAllIncident() {
		checkIConnection();
		if (connected) {
			if (asyncTask == null) {
				// --- create a new task --
				asyncTask = new LoadAllIncident();
				asyncTask.execute();
			} else if (asyncTask.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTask = new LoadAllIncident();
				asyncTask.execute();
			} else if (asyncTask.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTask.cancel(false);
				asyncTask = new LoadAllIncident();
				asyncTask.execute();
			}
		} else {
			Toast.makeText(this, "Internet connection not available",
					Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		rMap.clear();
		finish();
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		
	}

}
