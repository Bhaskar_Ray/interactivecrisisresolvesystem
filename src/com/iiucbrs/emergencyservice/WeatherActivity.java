package com.iiucbrs.emergencyservice;



import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class WeatherActivity extends Activity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, LocationListener{
	ActionBar actionBar;
	ImageView img,imgLog;
	Button shelter,sos,emergency,report;
	private GoogleApiClient apiClient_helpsms;
	//double myLat,myLon;
	Button send;
	private ProgressDialog pDialog;
	private AsyncTask<String, String, String> asyncTasksendMsg;
	Boolean connected;
	ArrayList<String> msg = new ArrayList<String>();
	ArrayList<String> msg_link = new ArrayList<String>();
	ArrayList<String> msg_to = new ArrayList<String>();
	String SMS_SENT = "SMS_SENT";
	String SMS_DELIVERED = "SMS_DELIVERED";
	PendingIntent sentPendingIntent,deliveredPendingIntent;
	SharedPreferences pref_sms;
	SharedPreferences pref_user_id;
	SharedPreferences.Editor editor_user_id;
	public static final String U_ID = "user_id";
	public static final String U_NAME = "Login_user";
	
    double currentLatitude_helpsms;
    double currentLongitude_helpsms;
	Location currentLocation_helpsms;
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	private LocationRequest mLocationRequest_helpsms;
	String phone1,phone2,phone3,msg_body;
	private int usr_id;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_weather);
	actionBar = getActionBar();
	actionBar.hide();
	shelter=(Button) findViewById(R.id.btnShelter);
	sos=(Button) findViewById(R.id.btnSOS);
	emergency=(Button) findViewById(R.id.btnEmergency);
	report=(Button) findViewById(R.id.btnReport);
	img=(ImageView) findViewById(R.id.actionImageView);
	imgLog=(ImageView) findViewById(R.id.imgLogout);
	sentPendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(SMS_SENT), 0);
	deliveredPendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(SMS_DELIVERED), 0);
	pref_user_id= getSharedPreferences("USER_ID",
			Context.MODE_PRIVATE);
	usr_id=getUserIdFromSharedPref();
	pref_sms= getSharedPreferences(""+usr_id,
			Context.MODE_PRIVATE);

	
	mLocationRequest_helpsms = LocationRequest.create()
	        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
	        .setInterval(10 * 1000)        // 10 seconds, in milliseconds
	        .setFastestInterval(1 * 1000); // 1 second, in milliseconds
	
	apiClient_helpsms = new GoogleApiClient.Builder(this)
	.addApi(LocationServices.API).addConnectionCallbacks(this)
	.addOnConnectionFailedListener(this).build();
apiClient_helpsms.connect();



imgLog.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		LayoutInflater inflater=LayoutInflater.from(WeatherActivity.this);
		View view=inflater.inflate(R.layout.custom_logout, null);
		TextView loginUserNw=(TextView)view.findViewById(R.id.txtLoginUser);
		Button btnLogOutNw=(Button)view.findViewById(R.id.btnLogOut);
		Button btnLogoutCancel=(Button)view.findViewById(R.id.btnLogoutCancel);
		

		AlertDialog.Builder builder=new AlertDialog.Builder(WeatherActivity.this);
		builder.setView(view);
		builder.setCancelable(false);
		final AlertDialog dialog1=builder.create();
		String userNw=getUserNameFromSharedPref();
		loginUserNw.setText(userNw);
		btnLogOutNw.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			storeUserIDinSharedPref(WeatherActivity.this, -1, "no_login");
			Intent in=new Intent(WeatherActivity.this,Login.class);
			startActivity(in);
			finish();
				
			dialog1.cancel();	
				
				
			}
		});
		
		btnLogoutCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
		        dialog1.cancel();
				
			}
		});
		
		dialog1.show();
		
	}
});

registerReceiver(new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (getResultCode()) {
            case Activity.RESULT_OK:
                Toast.makeText(context, "SMS sent successfully", Toast.LENGTH_SHORT).show();
                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                Toast.makeText(context, "Generic failure cause", Toast.LENGTH_SHORT).show();
                TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(context);
                boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
                boolean isSIM2Ready = telephonyInfo.isSIM2Ready();
                boolean isDualSIM = telephonyInfo.isDualSIM();

                Log.e("IS DUAL SIM", "" + isDualSIM);
                Log.e("isSIM1Ready", "" + isSIM1Ready);
                Log.e("isSIM2Ready", "" + isSIM2Ready);

                if(isDualSIM) {
                    if(isSIM1Ready || isSIM2Ready) {
                        Toast.makeText(context, "An Unexpected failure occured while sending SMS. Please check whether you have working SMS plan and try again later.",
                                Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(context, "Please activate atleast one SIM for sending SMS and retry.",
                                Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    if(!isSIM1Ready) {
                        Toast.makeText(context, "No SIM detected to perform this action.",
                                Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(context, "An Unexpected failure occured while sending SMS. Please check whether you have working SMS plan and try again later.",
                                Toast.LENGTH_LONG).show();
                    }
                }

                
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                Toast.makeText(context, "Service is currently unavailable", Toast.LENGTH_SHORT).show();
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
                Toast.makeText(context, "No pdu provided", Toast.LENGTH_SHORT).show();
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                Toast.makeText(context, "Radio was explicitly turned off", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}, new IntentFilter(SMS_SENT));


//For when the SMS has been delivered
		registerReceiver(new BroadcastReceiver() {
		    @Override
		    public void onReceive(Context context, Intent intent) {
		        switch (getResultCode()) {
		            case Activity.RESULT_OK:
		                Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
		                break;
		            case Activity.RESULT_CANCELED:
		                Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
		                break;
		        }
		    }
		}, new IntentFilter(SMS_DELIVERED));
		
		
	
	
	
	if (savedInstanceState == null) {
        getFragmentManager().beginTransaction()
                .add(R.id.container, new WeatherFragment())
                .commit();
    }
	img.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			showInputDialog();
			
		}
	});
	shelter.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			LayoutInflater inflater=LayoutInflater.from(WeatherActivity.this);
			View view=inflater.inflate(R.layout.online_offline_select, null);
			Button btnOnline=(Button)view.findViewById(R.id.btn_online_mode);
			Button btnOffline=(Button)view.findViewById(R.id.btn_offline_mode);
			Button btnCancel=(Button)view.findViewById(R.id.btn_mode_cancel);
	
			
			AlertDialog.Builder builder=new AlertDialog.Builder(WeatherActivity.this);
			builder.setView(view);
			builder.setCancelable(false);
			final AlertDialog dialog_on_off=builder.create();	
			btnOnline.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in=new Intent(WeatherActivity.this,CycloneShelter.class);
       			    startActivity(in);
					dialog_on_off.cancel();
				}
			});
			btnOffline.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in=new Intent(WeatherActivity.this,CycloneShelterOffline.class);
       			    startActivity(in);
					dialog_on_off.cancel();
				}
			});
			btnCancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog_on_off.cancel();
				}
			});
			
			
			dialog_on_off.show();
			
			
		}
	});
	sos.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
//			Intent in=new Intent(WeatherActivity.this,HelpSmsSender.class);
//			startActivity(in);
			

			

			// TODO Auto-generated method stub
			LayoutInflater inflater=LayoutInflater.from(WeatherActivity.this);
			View view=inflater.inflate(R.layout.custom_alert, null);
			Button btnSOS=(Button)view.findViewById(R.id.btnHelpSOS);
			Button btnSettings=(Button)view.findViewById(R.id.btnHelpSettings);
			Button btnCancel=(Button)view.findViewById(R.id.btnHelpCancel);
	
			
			AlertDialog.Builder builder=new AlertDialog.Builder(WeatherActivity.this);
			builder.setView(view);
			builder.setCancelable(false);
			final AlertDialog dialog=builder.create();
			
			btnSOS.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					
					try {
						
						phone1=getsms_content("contact1", usr_id);
						phone2=getsms_content("contact2", usr_id);
						phone3=getsms_content("contact3", usr_id);
						msg_body=getsms_content("sms", usr_id);
						
						Toast.makeText(WeatherActivity.this, "phone 1:"+phone1+"\n"+"MSG: "+msg_body, Toast.LENGTH_LONG).show();
						if(!msg_body.isEmpty())
						{
							msg.add(msg_body);
						}
						if(!phone1.isEmpty())
						{
							msg_to.add(phone1);
						}
						if(!phone2.isEmpty())
						{
							msg_to.add(phone3);
						}
						if(!phone3.isEmpty())
						{
							msg_to.add(phone3);
						}
						
						if(msg_to.size()>0)
						{
							runSendSmsHelp();
						}
						else
						{
							Toast.makeText(WeatherActivity.this, "You have to save atleast 1 contact number."+"\n"+"Go settings to save your contact number ", Toast.LENGTH_LONG).show();
						}
						
					} catch (Exception e) {
						// TODO: handle exception
						Log.e("runSendSmsHelp", e.getMessage());
					}
					
					dialog.cancel();
				}
			});
			btnSettings.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent in=new Intent(WeatherActivity.this,HelpSmsSender.class);
					startActivity(in);
					
					
			        dialog.cancel();
					
				}
			});
			btnCancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					 dialog.cancel();
				}
			});
			
			dialog.show();
			
		
			
			
			
			
			
		

			
		}
	});
	emergency.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent in=new Intent(WeatherActivity.this,OtherEmergency.class);
			startActivity(in);
			
			
		}
	});
	report.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent in =new Intent(WeatherActivity.this,MapReport.class);
			//in.putExtra("types", "police");
			startActivity(in);
			
			
		}
	});
	
}

//@Override
//public boolean onCreateOptionsMenu(Menu menu) {
//	// Inflate the menu; this adds items to the action bar if it is present.
//	getMenuInflater().inflate(R.menu.main, menu);
//	return true;
//}
//@Override
//public boolean onOptionsItemSelected(MenuItem item) {
//    if(item.getItemId() == R.id.change_city){
//        showInputDialog();
//    }
//    return false;
//}
 
private void showInputDialog(){
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("Find Weather Report");
    final EditText input = new EditText(this);
    input.setInputType(InputType.TYPE_CLASS_TEXT);
    input.setHint("Enter City Name");
    //input.setHintTextColor(3);
    
    builder.setView(input);
    
    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
        	String inputCity=input.getText().toString();
        	if(!inputCity.isEmpty())
        	{
        		 changeCity(inputCity);
        	}
        	else
        	{
        		Toast.makeText(WeatherActivity.this, "No city name entered!!", Toast.LENGTH_LONG).show();
        	}
           
        }
    });
    builder.show();
}
 
public void changeCity(String city){
    WeatherFragment wf = (WeatherFragment)getFragmentManager()
                            .findFragmentById(R.id.container);
    wf.changeCity(city);
    new CityPreference(this).setCity(city);
}



@Override
public void onConnectionFailed(ConnectionResult connectionResult) {
	// TODO Auto-generated method stub
	 if (connectionResult.hasResolution()) {
	        try {
	            // Start an Activity that tries to resolve the error
	            connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
	        } catch (IntentSender.SendIntentException e) {
	            e.printStackTrace();
	        }
	    } else {
	        Log.i("onConnectionFailed", "Location services connection failed with code " + connectionResult.getErrorCode());
	    }
	
}

@Override
public void onConnected(Bundle arg0) {
	// TODO Auto-generated method stub

	 Log.i("onConnected_mapacti", "Location services connected.");
	 try {
		 
		 currentLocation_helpsms = LocationServices.FusedLocationApi.getLastLocation(apiClient_helpsms);
		 
		 if (currentLocation_helpsms == null) {
		        LocationServices.FusedLocationApi.requestLocationUpdates(apiClient_helpsms, mLocationRequest_helpsms, this);
		    }
		    else {
		        //handleNewLocation(location);
	    	 LocationServices.FusedLocationApi.requestLocationUpdates(apiClient_helpsms, mLocationRequest_helpsms, this);
//		    	 handleNewLocation(currentLocation);
		    }
		
	} catch (Exception e) {
		// TODO: handle exception
		Log.e("onConnected", e.getMessage());
	}
	 

	
	
}

@Override
public void onConnectionSuspended(int arg0) {
	// TODO Auto-generated method stub
	 Log.i("onConnectionSuspended", "Location services suspended. Please reconnect.");
	
}
@Override
protected void onResume() {
	// TODO Auto-generated method stub
	super.onResume();
	
	try {
		apiClient_helpsms.connect();
	} catch (Exception e) {
		// TODO: handle exception
		Log.e("onResume", e.getMessage());
	}
	
}

@Override
protected void onPause() {
	// TODO Auto-generated method stub
	super.onPause();
	
	try {
		if (apiClient_helpsms.isConnected()) {
			LocationServices.FusedLocationApi.removeLocationUpdates(apiClient_helpsms, this);
	        apiClient_helpsms.disconnect();
	    }
	} catch (Exception e) {
		// TODO: handle exception
		Log.e("onPause", e.getMessage());
	}
}

@Override
protected void onStop() {
	// TODO Auto-generated method stub
	super.onStop();
}

private void runSendSmsHelp() {
	//checkIConnection();
	//if (connected) {
		if (asyncTasksendMsg == null) {
			// --- create a new task --
			asyncTasksendMsg = new SendSmsHelp();
			asyncTasksendMsg.execute();
		} else if (asyncTasksendMsg.getStatus() == AsyncTask.Status.FINISHED) {
			asyncTasksendMsg = new SendSmsHelp();
			asyncTasksendMsg.execute();
		} else if (asyncTasksendMsg.getStatus() == AsyncTask.Status.RUNNING) {
			asyncTasksendMsg.cancel(false);
			asyncTasksendMsg = new SendSmsHelp();
			asyncTasksendMsg.execute();
		}
//	} else {
//		Toast.makeText(
//				WeatherActivity.this,
//				"Location Can't be found." + "\n"
//						+ "Check Internet Connection",
//				Toast.LENGTH_LONG).show();
//	}
}



class SendSmsHelp extends AsyncTask<String, String, String> {
	
	Boolean flag=false;
	String sms_body="";

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		pDialog = new ProgressDialog(WeatherActivity.this);
		pDialog.setMessage("Sending sms please wait...");
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(true);

		pDialog.show();
		
	}

	protected String doInBackground(String... args) {
		
		currentLocation_helpsms = LocationServices.FusedLocationApi
				.getLastLocation(apiClient_helpsms);
		try {
			currentLatitude_helpsms=currentLocation_helpsms.getLatitude();
			currentLongitude_helpsms=currentLocation_helpsms.getLongitude();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("HelpSmsLocation", e.getMessage());
		}
		if(msg.size()>0)
		{
			sms_body=msg.get(0);
		}
		msg_link.add(sms_body+"\n"+"http://maps.google.de/maps?q=loc:"+currentLatitude_helpsms+","+currentLongitude_helpsms);
		

		try {
			SmsManager smsManager = SmsManager.getDefault();
			
			for (int i = 0; i< msg_to.size(); i++) {
				
				  smsManager.sendTextMessage(msg_to.get(i), null, msg_link.get(0), sentPendingIntent, deliveredPendingIntent);
			}
			
			
			
			
			
		} catch (Exception e) {
			Log.e("SMS_SEND_SUCCESS", ""+e);
		}

		return null;
	}

	protected void onPostExecute(String file_url) {
		pDialog.dismiss();
		msg.clear();
		msg_to.clear();
		msg_link.clear();
	
	}

}

public void checkIConnection() {
	ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
			.getState() == NetworkInfo.State.CONNECTED
			|| connectivityManager.getNetworkInfo(
					ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
		connected = true;
		// Toast.makeText(getApplicationContext(), "Connected",
		// Toast.LENGTH_LONG).show();
	} else {
		connected = false;
		Toast.makeText(getApplicationContext(),
				"No internet connection available", Toast.LENGTH_LONG)
				.show();
	}
	
	
	
}

private String getsms_content(String what,int uId) {
	
	String sms_content=pref_sms.getString(what+uId, "");
	
	return sms_content;
	

}

private Integer getUserIdFromSharedPref() {
	
	int userID=pref_user_id.getInt(U_ID, -1);
	
	return userID;
	

}



private String getUserNameFromSharedPref() {
	
	String userName=pref_user_id.getString(U_NAME, "no_login");
	
	return userName;
	

}
	


private void storeUserIDinSharedPref(Context context, int uId,String login_user) {
	
	
	try {
		
		
		if(uId!=0&&!login_user.isEmpty())
		{
			editor_user_id = pref_user_id.edit();
			editor_user_id.putInt(U_ID, uId);
			editor_user_id.putString(U_NAME, login_user);
			editor_user_id.commit();
			//Toast.makeText(context, "user_id strored", Toast.LENGTH_SHORT).show();
		}
		
		
	} catch (Exception e) {
		// TODO: handle exception
		Log.e("storeUserIDinSharedPref", e.getMessage());
	}
	
	

}



@Override
public void onLocationChanged(Location arg0) {
	// TODO Auto-generated method stub
	
}
@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	//super.onBackPressed();
	finish();
	
}



}
