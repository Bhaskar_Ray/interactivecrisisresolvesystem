package com.iiucbrs.emergencyservice;

import java.util.List;

import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MyLocationOverlay;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.OverlayItem;




import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

public class OfflineMap extends Activity {
	private String lat_offline,lon_offline,distance;
	private String districtName;
	private String upazilaName;
	private String unionName;
	private String shelterNm_offline;
	private double lat,lon;
	
	
	 MyLocationOverlay myLocationOverlay = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offline_map);
		shelterNm_offline=getIntent().getExtras().getString("shelter_name");
		districtName=getIntent().getExtras().getString("districtName");
		upazilaName=getIntent().getExtras().getString("upazilaName");
		unionName=getIntent().getExtras().getString("unionName");
		distance=getIntent().getExtras().getString("distance");
		lat_offline=getIntent().getExtras().getString("lat");
		lon_offline=getIntent().getExtras().getString("lon");
		MapView mapView =(MapView) findViewById(R.id.mapview1);
		   mapView.setClickable(true);
		   mapView.setBuiltInZoomControls(true);
		   mapView.setTileSource(TileSourceFactory.MAPQUESTOSM);
		   mapView.getController().setZoom(14); //set initial zoom-level, depends on your need
		   
		   
		   List<Overlay> mapOverlays = mapView.getOverlays();
		   Drawable drawable = this.getResources().getDrawable(R.drawable.shelter2);
		MyOwnItemizedOverlay itemizedoverlay = new MyOwnItemizedOverlay(drawable,this);
		lat = Double.parseDouble(lat_offline);
		lon =Double.parseDouble(lon_offline);
		
		GeoPoint shelter_point = new GeoPoint(lat,lon);
		OverlayItem overlayitem = new OverlayItem(shelterNm_offline+" cyclone shelter", districtName+" // "+upazilaName+" // "+unionName+" // "+distance+" Km", shelter_point);
		
		
		mapView.getController().setZoom(14);
		mapView.getController().setCenter(shelter_point);
		itemizedoverlay.addItem(overlayitem);
		mapView.getOverlays().add(itemizedoverlay.getOverlay());
		
		
	   
	   
		 myLocationOverlay = new MyLocationOverlay(this, mapView);
	        mapView.getOverlays().add(myLocationOverlay);
	        myLocationOverlay.enableMyLocation();
	}
	public void onBackPressed() {
		myLocationOverlay.disableMyLocation();
		finish();
		
	};

}
