package com.iiucbrs.emergencyservice;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class AboutDev extends Activity {
	ArrayList<String> name1 = new ArrayList<String>();
	ArrayList<String> ds1 = new ArrayList<String>();
	ArrayList<String> name2 = new ArrayList<String>();
	ArrayList<String> ds2 = new ArrayList<String>();
	TextView dev1, dev2, devdes1, devdes2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actiivity_aboutdev);
		dev1 = (TextView) findViewById(R.id.txtName1);
		dev2 = (TextView) findViewById(R.id.txtName2);
		devdes1 = (TextView) findViewById(R.id.txtDs1);
		devdes2 = (TextView) findViewById(R.id.txtDs2);
		name1.add("Bhaskar Ray");
		ds1.add("International Islamic University Chittagong" + "\n"+"Dept. of CSE"+"\n"+"\n"
				+ "Contact: " + "+8801673533450" +"\n"+"\n"+ "Email: "
				+ "raybhaskar.ray@gmail.com");
		name2.add("Muhammad Estiak Omar");
		ds2.add("International Islamic University Chittagong" + "\n"+"Dept. of CSE"+"\n"+"\n"
				+ "Contact: " + "+8801710200333" +"\n"+"\n"+ "Email: "
				+ "estiak.omar@gmail.com");
		dev1.setText(name1.get(0));
		dev2.setText(name2.get(0));
		devdes1.setText(ds1.get(0));
		devdes2.setText(ds2.get(0));

	}

}
