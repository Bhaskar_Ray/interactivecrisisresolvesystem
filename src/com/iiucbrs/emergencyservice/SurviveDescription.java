package com.iiucbrs.emergencyservice;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

public class SurviveDescription extends Activity {
	TextView head1, head2, head3, head4, des1, des2, des3, des4;
	int pos;
	ArrayList<String> head1Array = new ArrayList<String>();
	ArrayList<String> head2Array = new ArrayList<String>();
	ArrayList<String> head3Array = new ArrayList<String>();
	ArrayList<String> head4Array = new ArrayList<String>();
	ArrayList<String> des1Array = new ArrayList<String>();
	ArrayList<String> des2Array = new ArrayList<String>();
	ArrayList<String> des3Array = new ArrayList<String>();
	ArrayList<String> des4Array = new ArrayList<String>();
	Typeface tyf;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_survivedescription);
		head1 = (TextView) findViewById(R.id.textHead1);
		head2 = (TextView) findViewById(R.id.textHead2);
		head3 = (TextView) findViewById(R.id.textHead3);
		head4 = (TextView) findViewById(R.id.textHead4);
		des1 = (TextView) findViewById(R.id.textDes1);
		des2 = (TextView) findViewById(R.id.textDes2);
		des3 = (TextView) findViewById(R.id.textDes3);
		des4 = (TextView) findViewById(R.id.textDes4);
		tyf=Typeface.createFromAsset(this.getAssets(), "fonts/BenSenHandwriting.ttf");

		pos = getIntent().getExtras().getInt("key");
		setDataHandler();
		setContent(pos);
		head1.setTypeface(tyf,Typeface.NORMAL);
		head2.setTypeface(tyf,Typeface.NORMAL);
		head3.setTypeface(tyf,Typeface.NORMAL);
		head4.setTypeface(tyf,Typeface.NORMAL);
		des1.setTypeface(tyf,Typeface.NORMAL);
		des2.setTypeface(tyf,Typeface.NORMAL);
		des3.setTypeface(tyf,Typeface.NORMAL);
		des4.setTypeface(tyf,Typeface.NORMAL);
		head1.setText(head1Array.get(0));
		head2.setText(head2Array.get(0));
		head3.setText(head3Array.get(0));
		head4.setText(head4Array.get(0));
		des1.setText(des1Array.get(0));
		des2.setText(des2Array.get(0));
		des3.setText(des3Array.get(0));
		des4.setText(des4Array.get(0));

	}

	private void setContent(int x) {
		String dbPath = "/data/data/com.iiucbrs.emergencyservice/databases/survival.db";
		SQLiteDatabase myDatabase = null;

		try {
			myDatabase = SQLiteDatabase.openDatabase(dbPath, null,
					SQLiteDatabase.OPEN_READWRITE);
		} catch (Exception e) {
			// Log.d("myLog", "Database Open Problem menu");
		}
		// Log.d("myLog", "getCategory before query=" + getCategory);

		String sql = "SELECT * FROM disasters WHERE id=" + x;
		// Log.d("myLog", "SQL: " + sql);

		Cursor c = null;
		try {
			c = myDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			// Log.d("myLog", "raw query error");
			// e.printStackTrace();
		}

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			try {
				// rest_description.add(c.getString(c
				// .getColumnIndex("rest_description")));
				//
				// rest_id.add(c.getInt(c.getColumnIndex("rest_id")));
				// rest_contact.add(c.getString(c.getColumnIndex("rest_contact")));
				// rest_name.add(c.getString(c.getColumnIndex("rest_name")));
				// rest_lat.add(c.getDouble(c.getColumnIndex("rest_lat")));
				// rest_lon.add(c.getDouble(c.getColumnIndex("rest_lon")));
				head1Array.add(c.getString(c.getColumnIndex("head1")));
				head2Array.add(c.getString(c.getColumnIndex("head2")));
				head3Array.add(c.getString(c.getColumnIndex("head3")));
				head4Array.add(c.getString(c.getColumnIndex("head4")));
				des1Array.add(c.getString(c.getColumnIndex("des1")));
				des2Array.add(c.getString(c.getColumnIndex("des2")));
				des3Array.add(c.getString(c.getColumnIndex("des3")));
				des4Array.add(c.getString(c.getColumnIndex("des4")));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				// Log.d("myLog", "setting var error");
				// e.printStackTrace();
			}

			//
		}

		myDatabase.close();

	}

	private void setDataHandler() {
		DataHandler db = new DataHandler(getApplicationContext());
		try {
			db.createDataBase();
		} catch (IOException e) {
			// Log.d("myLog", "DataBase Exists");
			// e.printStackTrace();
		}

	}

}
