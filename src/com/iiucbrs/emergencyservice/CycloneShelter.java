package com.iiucbrs.emergencyservice;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.iiucbrs.emergencyservice.ReportDetails.AddComments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class CycloneShelter extends Activity {
	boolean connected;
	boolean available=false;
	String url;

	int success;
	String message;
	

	JSONObject jsonObj;
	private AsyncTask<String, String, String> asyncTaskShelter;
	private AsyncTask<String, String, String> asyncTask_Spinner1;
	private AsyncTask<String, String, String> asyncTask_Spinner2;
	private AsyncTask<String, String, String> asyncTask_Spinner3;

	Spinner spinner1, spinner2, spinner3;
	Button submit;
	int selectedspinner1, selectedspinner2, selectedspinner3;
	ArrayList<String> spinner1_list = new ArrayList<String>();
	ArrayList<Integer> spinner1_id = new ArrayList<Integer>();
	ArrayList<String> spinner2_list = new ArrayList<String>();
	ArrayList<Integer> spinner2_id = new ArrayList<Integer>();
	ArrayList<String> spinner3_list = new ArrayList<String>();
	ArrayList<Integer> spinner3_id = new ArrayList<Integer>();
	ArrayList<String> Shelter_Name = new ArrayList<String>();
	ArrayList<String> Shelter_Lat = new ArrayList<String>();
	ArrayList<String> Shleter_Lon = new ArrayList<String>();
	ArrayList<String> Shelter_Condition = new ArrayList<String>();
	ArrayList<String> Shelter_no_floor = new ArrayList<String>();
	ArrayList<String> Shleter_floor_space = new ArrayList<String>();
	ArrayList<String> Shelter_capacity = new ArrayList<String>();
	ArrayList<String> Shelter_no_toilet = new ArrayList<String>();
	ArrayList<String> Shleter_water = new ArrayList<String>();
	ArrayList<String> structure_type = new ArrayList<String>();
	ArrayList<String> type_use = new ArrayList<String>();
	
	private String districtName="";
	private String upazilaName="";
	private String unionName="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cyclone_shelter);

		spinner1 = (Spinner) findViewById(R.id.spDistrict);
		spinner2 = (Spinner) findViewById(R.id.spUpazila);
		spinner3 = (Spinner) findViewById(R.id.spUnion);
		submit=(Button) findViewById(R.id.cyclone_button);
		submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				checkIConnection();
				if(connected)
				{
					runshelter_AsyncTaskRunner();
				}
				
			}
		});
		

		checkIConnection();
		if(connected)
		{
			runspin1_AsyncTaskRunner();
			
		}
		
	}

	public void checkIConnection() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState() == NetworkInfo.State.CONNECTED
				|| connectivityManager.getNetworkInfo(
						ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
			connected = true;
//			Toast.makeText(getApplicationContext(), "Connected",
//					Toast.LENGTH_LONG).show();
		} else {
			connected = false;
			Toast.makeText(getApplicationContext(),
					"No internet connection available", Toast.LENGTH_LONG)
					.show();
		}
	}
	
	
	
	private void runspin1_AsyncTaskRunner() {
		checkIConnection();
		if (connected) {
			if (asyncTask_Spinner1 == null) {
				// --- create a new task --
				asyncTask_Spinner1 = new spin1_AsyncTaskRunner();
				asyncTask_Spinner1.execute();
			} else if (asyncTask_Spinner1.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTask_Spinner1 = new spin1_AsyncTaskRunner();
				asyncTask_Spinner1.execute();
			} else if (asyncTask_Spinner1.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTask_Spinner1.cancel(false);
				asyncTask_Spinner1 = new spin1_AsyncTaskRunner();
				asyncTask_Spinner1.execute();
			}
		} else {
			Toast.makeText(
					CycloneShelter.this,
					"Check Internet Connection", Toast.LENGTH_LONG)
					.show();
		}
	}

	class spin1_AsyncTaskRunner extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(CycloneShelter.this);

		protected void onPreExecute() {
			progressDialog.setMessage("Please wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				url = "http://codencrayon.com/crisis_resolve/getdata.php";
				//url = "http://192.168.43.191/myserver/getdata.php";
				jsonObj = JSONsimple.getJSONfromURL(url);

				spinner1_list.clear();
				spinner1_id.clear();
				int success=jsonObj.getInt("success");
				if(success==1)
				{
					JSONArray spins = jsonObj.getJSONArray("district");
					for (int i = 0; i < spins.length(); i++) {
						JSONObject spin = spins.getJSONObject(i);

						spinner1_list.add(spin.optString("dis_name"));
						spinner1_id.add(spin.optInt("dis_id"));
					}
					available=true;
				}

				
			} catch (Exception e) {
				Log.d("Error:", e + "");
			}
			return null;
		}

		protected void onPostExecute(String string) {
			spinner1.setAdapter(new ArrayAdapter<String>(CycloneShelter.this,
					R.layout.spinner, spinner1_list));
			spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parentView,
						View selectedItemView, int position, long id) {
					selectedspinner1 = spinner1_id.get(spinner1
							.getSelectedItemPosition());
					districtName=spinner1_list.get(spinner1.getSelectedItemPosition());
//					Toast.makeText(getApplicationContext(),
//							"DIS_ID: " + selectedspinner1, Toast.LENGTH_SHORT)
//							.show();
					checkIConnection();
					if(connected)
					{
						runspin2_AsyncTaskRunner();
					}
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
				}
			});
			progressDialog.dismiss();
		}
	}
	
	
	
	private void runspin2_AsyncTaskRunner() {
		checkIConnection();
		if (connected) {
			if (asyncTask_Spinner2 == null) {
				// --- create a new task --
				asyncTask_Spinner2 = new spin2_AsyncTaskRunner();
				asyncTask_Spinner2.execute();
			} else if (asyncTask_Spinner2.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTask_Spinner2 = new spin2_AsyncTaskRunner();
				asyncTask_Spinner2.execute();
			} else if (asyncTask_Spinner2.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTask_Spinner2.cancel(false);
				asyncTask_Spinner2 = new spin2_AsyncTaskRunner();
				asyncTask_Spinner2.execute();
			}
		} else {
			Toast.makeText(
					CycloneShelter.this,
					"Check Internet Connection", Toast.LENGTH_LONG)
					.show();
		}
	}

	class spin2_AsyncTaskRunner extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(CycloneShelter.this);

		protected void onPreExecute() {
			progressDialog.setMessage("Please wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				spinner2_list.clear();
				spinner2_id.clear();

				url = "http://codencrayon.com/crisis_resolve/getupazila.php?dist_id="
						+ selectedspinner1;
//				url = "http://192.168.43.191/myserver/getupazila.php?dist_id="
//						+ selectedspinner1;
				jsonObj = JSONsimple.getJSONfromURL(url);
				int success=jsonObj.getInt("success");
				if(success==1)
				{
					JSONArray spins = jsonObj.getJSONArray("upazila");
					for (int i = 0; i < spins.length(); i++) {
						JSONObject spin = spins.getJSONObject(i);

						spinner2_list.add(spin.optString("up_name"));
						spinner2_id.add(spin.optInt("up_id"));
					}
					available=true;
				}

				
			} catch (Exception e) {
				Log.d("Error:", e + "");
			}
			return null;
		}

		protected void onPostExecute(String string) {
			spinner2.setAdapter(new ArrayAdapter<String>(CycloneShelter.this,
					R.layout.spinner, spinner2_list));
			spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parentView,
						View selectedItemView, int position, long id) {
					selectedspinner2 = spinner2_id.get(spinner2
							.getSelectedItemPosition());
					upazilaName=spinner2_list.get(spinner2.getSelectedItemPosition());
//					Toast.makeText(getApplicationContext(),
//							"UP_ID: " + selectedspinner2, Toast.LENGTH_SHORT)
//							.show();
					checkIConnection();
					if(connected)
					{
						runspin3_AsyncTaskRunner();
					}
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
				}
			});

			progressDialog.dismiss();
		}
	}
	
	
	
	private void runspin3_AsyncTaskRunner() {
		checkIConnection();
		if (connected) {
			if (asyncTask_Spinner3 == null) {
				// --- create a new task --
				asyncTask_Spinner3 = new spin3_AsyncTaskRunner();
				asyncTask_Spinner3.execute();
			} else if (asyncTask_Spinner3.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTask_Spinner3 = new spin3_AsyncTaskRunner();
				asyncTask_Spinner3.execute();
			} else if (asyncTask_Spinner3.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTask_Spinner3.cancel(false);
				asyncTask_Spinner3 = new spin3_AsyncTaskRunner();
				asyncTask_Spinner3.execute();
			}
		} else {
			Toast.makeText(
					CycloneShelter.this,
					"Check Internet Connection", Toast.LENGTH_LONG)
					.show();
		}
	}

	class spin3_AsyncTaskRunner extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(CycloneShelter.this);

		protected void onPreExecute() {
			progressDialog.setMessage("Please wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				
				spinner3_list.clear();
				spinner3_id.clear();

				url = "http://codencrayon.com/crisis_resolve/getunion.php?upt_id="
						+ selectedspinner2;
				
//				url = "http://192.168.43.191/myserver/getunion.php?upt_id="
//						+ selectedspinner2;
				jsonObj = JSONsimple.getJSONfromURL(url);
				int success=jsonObj.getInt("success");
				if(success==1)
				{
					JSONArray spins = jsonObj.getJSONArray("union");
					for (int i = 0; i < spins.length(); i++) {
						JSONObject spin = spins.getJSONObject(i);

						spinner3_list.add(spin.optString("un_name"));
						spinner3_id.add(spin.optInt("un_id"));
					}
					available=true;
				}
			

				
			} catch (Exception e) {
				Log.d("Error:", e + "");
			}
			return null;
		}

		protected void onPostExecute(String string) {
			spinner3.setAdapter(new ArrayAdapter<String>(CycloneShelter.this,
					R.layout.spinner, spinner3_list));
			spinner3.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parentView,
						View selectedItemView, int position, long id) {
					selectedspinner3 = spinner3_id.get(spinner3
							.getSelectedItemPosition());
					unionName=spinner3_list.get(spinner3.getSelectedItemPosition());
//					Toast.makeText(getApplicationContext(),
//							"UN_ID: " + selectedspinner3, Toast.LENGTH_SHORT)
//							.show();
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
				}
			});

			progressDialog.dismiss();
			
		
		}
	}
	
	
	private void runshelter_AsyncTaskRunner() {
		checkIConnection();
		if (connected) {
			if (asyncTaskShelter == null) {
				// --- create a new task --
				asyncTaskShelter = new shelter_AsyncTaskRunner();
				asyncTaskShelter.execute();
			} else if (asyncTaskShelter.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTaskShelter = new shelter_AsyncTaskRunner();
				asyncTaskShelter.execute();
			} else if (asyncTaskShelter.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTaskShelter.cancel(false);
				asyncTaskShelter = new shelter_AsyncTaskRunner();
				asyncTaskShelter.execute();
			}
		} else {
			Toast.makeText(
					CycloneShelter.this,
					"Check Internet Connection", Toast.LENGTH_LONG)
					.show();
		}
	}
	

	
	class shelter_AsyncTaskRunner extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(CycloneShelter.this);

		protected void onPreExecute() {
			progressDialog.setMessage("Please wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				Shelter_Name.clear();
				Shelter_Lat.clear();
				Shleter_Lon.clear();
				Shelter_Condition.clear();
				Shelter_no_floor.clear();
				Shleter_floor_space.clear();
				Shelter_capacity.clear();
				Shelter_no_toilet.clear();
				Shleter_water.clear();
				structure_type.clear();
				
				

				url = "http://codencrayon.com/crisis_resolve/getshelter.php?unt_id="
						+ selectedspinner3;
				
//				url = "http://192.168.43.191/myserver/getshelter.php?unt_id="
//						+ selectedspinner3;
				
				jsonObj = JSONsimple.getJSONfromURL(url);
				int success=jsonObj.getInt("success");
				if(success==1)
				{
					
					JSONArray get_shelter = jsonObj.getJSONArray("shelter");
					
					for (int i = 0; i < get_shelter.length(); i++) {
						JSONObject shelter_value = get_shelter.getJSONObject(i);

						Shelter_Name.add(shelter_value.optString("shelter_name"));
						Shelter_Lat.add(shelter_value.optString("lat"));
						Shleter_Lon.add(shelter_value.optString("lon"));
						Shelter_Condition.add(shelter_value.optString("shelter_condition"));
						Shelter_no_floor.add(shelter_value.optString("no_floor"));
						Shleter_floor_space.add(shelter_value.optString("floor_space"));
						Shelter_capacity.add(shelter_value.optString("capacity"));
						Shelter_no_toilet.add(shelter_value.optString("no_toilet"));
						Shleter_water.add(shelter_value.optString("water"));
						structure_type.add(shelter_value.optString("structure_type"));
						type_use.add(shelter_value.optString("type_use"));
					
						
					}
					available=true;
				}
			

				
			} catch (Exception e) {
				Log.d("Error:", e + "");
			}
			return null;
		}

		protected void onPostExecute(String string) {
//			spinner3.setAdapter(new ArrayAdapter<String>(CycloneShelter.this,
//					R.layout.spinner, spinner3_list));
//			spinner3.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//				@Override
//				public void onItemSelected(AdapterView<?> parentView,
//						View selectedItemView, int position, long id) {
//					selectedspinner3 = spinner3_id.get(spinner3
//							.getSelectedItemPosition());
//					Toast.makeText(getApplicationContext(),
//							"UN_ID: " + selectedspinner3, Toast.LENGTH_SHORT)
//							.show();
//				}
//
//				@Override
//				public void onNothingSelected(AdapterView<?> arg0) {
//				}
//			});

			//Toast.makeText(getApplicationContext(), "Shelter Name: "+Shelter_Name.get(0), Toast.LENGTH_LONG).show();
			progressDialog.dismiss();
			if(available==true)
			{
				if(Shelter_Name.size()!=0)
				{
					
					 HashMap<String,ArrayList> shelter_hash=new HashMap<String,ArrayList>();
					 shelter_hash.put("Shelter_Name", Shelter_Name);
					 shelter_hash.put("Shelter_Lat", Shelter_Lat);
					 shelter_hash.put("Shelter_Lon", Shleter_Lon);
					 shelter_hash.put("Shelter_Condition", Shelter_Condition);
					 shelter_hash.put("Shelter_no_floor", Shelter_no_floor);
					 shelter_hash.put("Shleter_floor_space", Shleter_floor_space);
					 shelter_hash.put("Shelter_capacity", Shelter_capacity);
					 shelter_hash.put("Shelter_no_toilet", Shelter_no_toilet);
					 shelter_hash.put("Shleter_water", Shleter_water);
					 shelter_hash.put("structure_type", structure_type);
					 shelter_hash.put("type_use", type_use);
					 Intent in=new Intent(CycloneShelter.this,MapClass.class);
					 in.putExtra("key", shelter_hash);
					 in.putExtra("district", districtName);
					 in.putExtra("upazila", upazilaName);
					 in.putExtra("union", unionName);
					 //in.putExtra("shelter_hash", shelter_hash);
					 startActivity(in);
					
				}
				else
				{
					Toast.makeText(CycloneShelter.this, "Sorry..No Shelter data found for this region! We will update our database soon ", Toast.LENGTH_LONG).show();
				}
				 
				
			}
			else
			{
				Toast.makeText(CycloneShelter.this, "Sorry..No Shelter data found.", Toast.LENGTH_LONG).show();
			}
			
		
		}
	}

}
