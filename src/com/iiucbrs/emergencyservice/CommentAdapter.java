package com.iiucbrs.emergencyservice;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CommentAdapter extends BaseAdapter {
	LayoutInflater inflater;
	Context context;

	ArrayList<String> userName;
	ArrayList<String> commet;
	ArrayList<String> commet_date;

	public CommentAdapter(Context context, ArrayList<String> userName,
			ArrayList<String> commet,ArrayList<String> commet_date) {
		this.context = context;
		this.userName = userName;
		this.commet = commet;
		this.commet_date = commet_date;
		inflater = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return commet.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return commet.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.comment_design, null);
			holder.txtUser = (TextView) convertView.findViewById(R.id.txtUserName);
			holder.txtCommet = (TextView) convertView.findViewById(R.id.txtComments);
			holder.txtCommetDate = (TextView) convertView.findViewById(R.id.txtCommentDate);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.txtUser.setText(userName.get(position));
		holder.txtCommet.setText(commet.get(position));
		holder.txtCommetDate.setText(commet_date.get(position));
	
		return convertView;
	}

	public static class ViewHolder {
		TextView txtUser;
		TextView txtCommet;
		TextView txtCommetDate;
	}

}
