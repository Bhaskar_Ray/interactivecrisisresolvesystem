package com.iiucbrs.emergencyservice;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CustomCycloneListView extends BaseAdapter {
	
	LayoutInflater inflater;
Context context;
List<String>listStr;
//ArrayList<String>listArray;

public CustomCycloneListView(Context context,List<String>listStr){
	this.context=context;
	this.listStr=listStr;
	
	
	inflater=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
}

@Override
public int getCount() {
	// TODO Auto-generated method stub
	return listStr.size();
}

@Override
public Object getItem(int position) {
	// TODO Auto-generated method stub
	return listStr.get(position);
}

@Override
public long getItemId(int arg0) {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public View getView(int position, View convertView, ViewGroup parent) {
	// TODO Auto-generated method stub
	ViewHolder holder=null;
	if(convertView==null){
		holder=new ViewHolder();
		convertView=inflater.inflate(R.layout.customcyclonelist, null);
		
		holder.txt=(TextView)convertView.findViewById(R.id.txtText);
		convertView.setTag(holder);
	}else{
		holder=(ViewHolder)convertView.getTag();
	}
	holder.txt.setText(listStr.get(position));
	
		return convertView;
}
public static class ViewHolder{
	
	TextView txt;
}


}
