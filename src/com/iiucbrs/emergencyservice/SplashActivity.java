package com.iiucbrs.emergencyservice;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class SplashActivity extends Activity {
	private static int SPLASH_TIME_OUT = 3000;
	SharedPreferences pref_user_id;
	SharedPreferences.Editor editor_user_id;
	public static final String U_ID = "user_id";
	public static final String U_NAME = "Login_user";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		pref_user_id= getSharedPreferences("USER_ID",
				Context.MODE_PRIVATE);
		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity
				
				int id=getUserIdFromSharedPref();
				String check_usr=getUserNameFromSharedPref();
				Toast.makeText(SplashActivity.this, "ID: "+id, Toast.LENGTH_SHORT).show();
				try {
					if(id==-1&&check_usr.equals("no_login"))
					{
						Intent i = new Intent(SplashActivity.this, Login.class);
						startActivity(i);

						// close this activity
						finish();
					}
					else
					{
						Intent i = new Intent(SplashActivity.this, WeatherActivity.class);
						startActivity(i);

						// close this activity
						finish();
						
					}
				} catch (Exception e) {
					// TODO: handle exception
					Log.e("Splash_ERROR", e.getMessage());
				}
				
				
			}
		}, SPLASH_TIME_OUT);
	}
	
      private Integer getUserIdFromSharedPref() {
		
		int userID=pref_user_id.getInt(U_ID, -1);
		
		return userID;
		
	
	}
      
      private String getUserNameFromSharedPref() {
  		
  		String userName=pref_user_id.getString(U_NAME, "no_login");
  		
  		return userName;
  		
  	
  	}

}
