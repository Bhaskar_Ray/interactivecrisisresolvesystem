package com.iiucbrs.emergencyservice;

import java.util.List;

import com.iiucbrs.emergencyservice.CustomAdaptar.ViewHolder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SourceAdapter extends BaseAdapter {

	
LayoutInflater inflater;
Context context;
List<String>severityList;
List<String>severityIc;

public SourceAdapter(Context context,List<String>severityList,List<String>severityIc){
	this.context=context;
	this.severityList=severityList;
	this.severityIc=severityIc;
	inflater=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
}

@Override
public int getCount() {
	// TODO Auto-generated method stub
	return severityList.size();
}

@Override
public Object getItem(int position) {
	// TODO Auto-generated method stub
	return severityList.get(position);
}

@Override
public long getItemId(int arg0) {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public View getView(int position, View convertView, ViewGroup parent) {
	// TODO Auto-generated method stub
	ViewHolder holder=null;
	if(convertView==null){
		holder=new ViewHolder();
		convertView=inflater.inflate(R.layout.reportcustom, null);
		holder.img=(ImageView)convertView.findViewById(R.id.imgCustom);
		holder.txt=(TextView)convertView.findViewById(R.id.txtText);
		convertView.setTag(holder);
	}else{
		holder=(ViewHolder)convertView.getTag();
	}
	holder.txt.setText(severityList.get(position));
	int resourceID = context.getResources().getIdentifier(
			severityIc.get(position), "drawable",
			context.getPackageName());
	holder.img.setImageResource(resourceID);
		return convertView;
}
public static class ViewHolder{
	ImageView img;
	TextView txt;
}


}
