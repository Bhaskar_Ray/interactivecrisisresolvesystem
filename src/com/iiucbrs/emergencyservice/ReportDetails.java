package com.iiucbrs.emergencyservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class ReportDetails extends Activity {
	TextView reportTitle, category, severity, impact, locality,
			approximateDate, approximateTime, details, source, postedBy,
			submittedOn,txtCommentYet;
	ImageView reportImage;
	int reportId;
	LinearLayout layoutComment;
	
	Boolean connected;
	
	private ProgressDialog pDialog;
	public static final String Report_Details_URL = "http://codencrayon.com/crisis_resolve/get_report_details.php";
	public static final String LoadComment_URL = "http://codencrayon.com/crisis_resolve/loadComment.php";
	public static final String AddComment_URL = "http://codencrayon.com/crisis_resolve/addComment.php";
	public static final String Image_URL = "http://codencrayon.com/crisis_resolve/adminboot/images/";
//	public static final String Report_Details_URL = "http://192.168.43.191/myserver/get_report_details.php";
//	public static final String LoadComment_URL = "http://192.168.43.191/myserver/loadComment.php";
//	public static final String AddComment_URL = "http://192.168.43.191/myserver/addComment.php";
//	public static final String Image_URL = "http://192.168.43.191/myserver/images/";
	public static final String U_ID = "user_id";
	SharedPreferences pref_user_id;
	ArrayList<String> incident_reportTitle = new ArrayList<String>();
	ArrayList<String> incident_category = new ArrayList<String>();
	ArrayList<String> incident_severity = new ArrayList<String>();
	ArrayList<String> incident_impact = new ArrayList<String>();
	ArrayList<String> incident_locality = new ArrayList<String>();
	ArrayList<String> incident_approximateDate = new ArrayList<String>();
	ArrayList<String> incident_approximateTime = new ArrayList<String>();
	ArrayList<String> incident_details = new ArrayList<String>();
	ArrayList<String> incident_source = new ArrayList<String>();
	ArrayList<String> incident_postedBy = new ArrayList<String>();
	ArrayList<String> incident_submittedOn = new ArrayList<String>();
	ArrayList<String> incident_image = new ArrayList<String>();
	ArrayList<String> commentUser = new ArrayList<String>();
	ArrayList<String> comment_date = new ArrayList<String>();
	ArrayList<String> comment = new ArrayList<String>();
	private JSONParser jsonParser = new JSONParser();
	private JSONArray json_incident_details;
	private JSONArray json_load_comment;
	private AsyncTask<String, String, String> asyncTask;
	private AsyncTask<String, String, String> asyncTaskAddComment;
	private AsyncTask<String, String, String> asyncTaskLoadComment;
	private String addValuecomment=null;
	private String serverResponse = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reportdetails);
		reportId=getIntent().getExtras().getInt("incident_id");
		reportTitle=(TextView)findViewById(R.id.txtReportTitle);
		category=(TextView)findViewById(R.id.txtCategory);
		severity=(TextView)findViewById(R.id.txtSeverity);
		impact=(TextView)findViewById(R.id.txtImpact);
		locality=(TextView)findViewById(R.id.txtLocality);
		approximateDate=(TextView)findViewById(R.id.txtApproximateDate);
		approximateTime=(TextView)findViewById(R.id.txtApproximateTime);
		details=(TextView)findViewById(R.id.txtDetails);
		source=(TextView)findViewById(R.id.txtSource);
		postedBy=(TextView)findViewById(R.id.txtPost);
		submittedOn=(TextView)findViewById(R.id.txtSubmission);
		reportImage=(ImageView) findViewById(R.id.imgReport);
		txtCommentYet=(TextView) findViewById(R.id.txtComments);
		layoutComment=(LinearLayout) findViewById(R.id.layoutComment);
		pref_user_id= getSharedPreferences("USER_ID",
				Context.MODE_PRIVATE);
		
		layoutComment.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(getApplicationContext(), "Comment Clicked", Toast.LENGTH_LONG).show();
				//TO-DO comment asynctask--------------------

				try {
					
						runLoadCommet();	
					
					
				} catch (Exception e) {
					// TODO: handle exception
					Log.e("runLoadCommet", ""+e);
				}

				

			
			}
		});
		

		checkIConnection();
		try {
			if(connected)
			{
				runLoadIncidentDetails();
			}
			else
			{
				Toast.makeText(getApplicationContext(), "Check your Internet Connection", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(getApplicationContext(), "Sorry: "+e, Toast.LENGTH_LONG).show();
			
		}
		
		
		
		
		
	}
	

	class LoadIncidentDetails extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ReportDetails.this);
			pDialog.setMessage("Loading Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);

			pDialog.show();
		}

		protected String doInBackground(String... args) {

			incident_reportTitle.clear();
			incident_category.clear();
			incident_severity.clear();
			incident_impact.clear();
			incident_locality.clear();
			incident_approximateDate.clear();
			incident_approximateTime.clear();
			incident_details.clear();
			incident_source.clear();
			incident_postedBy.clear();
			incident_submittedOn.clear();
			incident_image.clear();
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("incident_id", ""+reportId));
			// getting JSON string from URL
			jsonParser = new JSONParser();
			JSONObject json = jsonParser.makeHttpRequest(Report_Details_URL, "GET",
					params);
			try {
				// Checking for SUCCESS TAG

				int success = json.getInt("success");
				if (success == 1) {
					json_incident_details = json.getJSONArray("incident_details");

					for (int i = 0; i < json_incident_details.length(); i++) {
						JSONObject c = json_incident_details.getJSONObject(i);

						
						
						incident_reportTitle.add(c.getString("incident_title"));
						incident_category.add(c.getString("type_name"));
						incident_severity.add(c.getString("severity_level"));
						incident_impact.add(c.getString("impact"));
						incident_locality.add(c.getString("locality"));
						incident_approximateDate.add(c.getString("incident_date"));
						incident_approximateTime.add(c.getString("incident_time"));
						incident_details.add(c.getString("description"));
						incident_source.add(c.getString("source"));
						incident_postedBy.add(c.getString("user_name"));
						incident_submittedOn.add(c.getString("submission_date"));
						incident_image.add(c.getString("image_path"));

					}
					

				} else {

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			Log.i("test", "AsynchTask Stopped");
			//Toast.makeText(getApplicationContext(), incident_reportTitle+"\n"+incident_image, Toast.LENGTH_LONG).show();
		   //To-Do call another
			try {
				
				 setValue();
				
				checkIConnection();
				if(connected)
				{
					//runLoadIncidentImage();
					functionCalledFromUIThread();
				}
				else
				{
					Toast.makeText(getApplicationContext(), "Check your Internet Connection", Toast.LENGTH_LONG).show();
				}
				
			} catch (Exception e) {
				// TODO: handle exception
				Toast.makeText(getApplicationContext(), "Sorry: "+e, Toast.LENGTH_LONG).show();
			}
			
			

		}

	}
	
	
	

	class LoadAllComments extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ReportDetails.this);
			pDialog.setMessage("Loading comment please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);

			pDialog.show();
		}

		protected String doInBackground(String... args) {

			commentUser.clear();
			comment.clear();
			comment_date.clear();
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("incident_id", ""+reportId));
			// getting JSON string from URL
			jsonParser = new JSONParser();
			JSONObject json = jsonParser.makeHttpRequest(LoadComment_URL, "GET",
					params);
			try {
				// Checking for SUCCESS TAG

				int success = json.getInt("success");
				if (success == 1) {
					json_load_comment = json.getJSONArray("incident_comment");

					for (int i = 0; i < json_load_comment.length(); i++) {
						JSONObject c = json_load_comment.getJSONObject(i);

						
						
						commentUser.add(c.getString("user_name"));
						comment_date.add(c.getString("comment_date"));
						comment.add(c.getString("comment"));
						

					}
					

				} else {

				}
			} catch (JSONException e) {
				Log.e("LoadComment_Success", ""+e);
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			try {
				
					loadCommentView();
				
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("loadCommentView", ""+e);
			}
			
			
			
			

		}

	}
	
	
	
	class AddComments extends AsyncTask<String, String, String> {
		
		Boolean flag=false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ReportDetails.this);
			pDialog.setMessage("Adding comment please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);

			pDialog.show();
		}

		protected String doInBackground(String... args) {

			try {
				
			// Building Parameters
			List<NameValuePair> paramsAddComment = new ArrayList<NameValuePair>();
			paramsAddComment.add(new BasicNameValuePair("incident_id", ""+reportId));
			//-------------------------------
			int userId=getUserIdFromSharedPref();
			paramsAddComment.add(new BasicNameValuePair("user_id", ""+userId));
			paramsAddComment.add(new BasicNameValuePair("add_comment", addValuecomment));
			// getting JSON string from URL
			jsonParser = new JSONParser();
			JSONObject json = jsonParser.makeHttpRequest(AddComment_URL, "GET",
					paramsAddComment);
			
				// Checking for SUCCESS TAG

				int success = json.getInt("success");
				serverResponse=json.getString("message");
				if (success == 1) {
					
					flag=true;
					//To-Do------------------
					
					
					
				} else {
					flag=false;
					
					

				}
			} catch (JSONException e) {
				Log.e("AddComment_Success", ""+e);
			}

			return null;
		}

		protected void onPostExecute(String file_url) {
			pDialog.dismiss();
			if(flag)
			{
				Toast.makeText(getApplicationContext(),serverResponse , Toast.LENGTH_LONG).show();
				
				try {

					runLoadCommet();

				} catch (Exception e) {
					// TODO: handle exception
					Log.e("loadCommentView", "" + e);
				}
			}
			else
			{
				Toast.makeText(getApplicationContext(),serverResponse , Toast.LENGTH_LONG).show();
			}
			
			
			
			
			

		}

	}
	
	
	

	public void checkIConnection() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
				.getState() == NetworkInfo.State.CONNECTED
				|| connectivityManager.getNetworkInfo(
						ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
			connected = true;
			// Toast.makeText(getApplicationContext(), "Connected",
			// Toast.LENGTH_LONG).show();
		} else {
			connected = false;
			Toast.makeText(getApplicationContext(),
					"No internet connection available", Toast.LENGTH_LONG)
					.show();
		}
		
		
		
	}
	
	private void runLoadIncidentDetails() {
		checkIConnection();
		if (connected) {
			if (asyncTask == null) {
				// --- create a new task --
				asyncTask = new LoadIncidentDetails();
				asyncTask.execute();
			} else if (asyncTask.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTask = new LoadIncidentDetails();
				asyncTask.execute();
			} else if (asyncTask.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTask.cancel(false);
				asyncTask = new LoadIncidentDetails();
				asyncTask.execute();
			}
		} else {
			Toast.makeText(this, "Internet connection not available",
					Toast.LENGTH_SHORT).show();
		}
	}
	

	
	
	
	private void runLoadCommet() {
		checkIConnection();
		if (connected) {
			if (asyncTaskLoadComment == null) {
				// --- create a new task --
				asyncTaskLoadComment = new LoadAllComments();
				asyncTaskLoadComment.execute();
			} else if (asyncTaskLoadComment.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTaskLoadComment = new LoadAllComments();
				asyncTaskLoadComment.execute();
			} else if (asyncTaskLoadComment.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTaskLoadComment.cancel(false);
				asyncTaskLoadComment = new LoadAllComments();
				asyncTaskLoadComment.execute();
			}
		} else {
			Toast.makeText(
					ReportDetails.this,
					"Comments can't be loaded." + "\n"
							+ "Check Internet Connection",
					Toast.LENGTH_LONG).show();
		}
	}
	
	
	private void runAddCommet() {
		checkIConnection();
		if (connected) {
			if (asyncTaskAddComment == null) {
				// --- create a new task --
				asyncTaskAddComment = new AddComments();
				asyncTaskAddComment.execute();
			} else if (asyncTaskAddComment.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTaskAddComment = new AddComments();
				asyncTaskAddComment.execute();
			} else if (asyncTaskAddComment.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTaskAddComment.cancel(false);
				asyncTaskAddComment = new AddComments();
				asyncTaskAddComment.execute();
			}
		} else {
			Toast.makeText(
					ReportDetails.this,
					"Comments can't be added." + "\n"
							+ "Check Internet Connection",
					Toast.LENGTH_LONG).show();
		}
	}
	

	
	
	public void functionCalledFromUIThread(){
		try {
			pDialog = new ProgressDialog(ReportDetails.this);
			pDialog.setMessage("Loading Image...");
			pDialog.setIndeterminate(false);
			pDialog.show();
			if(incident_image.get(0).equals("NoImage"))
			{
				int resourceID = ReportDetails.this.getResources().getIdentifier(
						"report_image_default", "drawable",
						ReportDetails.this.getPackageName());
				reportImage.setImageResource(resourceID);
				pDialog.dismiss();
			}
			else
			{
				Toast.makeText(getApplicationContext(), incident_image.get(0), Toast.LENGTH_LONG).show();
				Picasso.with(ReportDetails.this).load(Image_URL+incident_image.get(0)).into(reportImage,new com.squareup.picasso.Callback() {
		             @Override
		             public void onSuccess() {
		            	 pDialog.dismiss();
		            	
		             }

		             @Override
		             public void onError() {
		            	 pDialog.dismiss();
		             }
		         }); 
				
			}
		
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Piccasso Error", ""+e);
		}

		
		}
	private void setValue()
	{
		
		try {
			reportTitle.setText(incident_reportTitle.get(0));
			category.setText(incident_category.get(0));
			severity.setText(incident_severity.get(0));
			impact.setText(incident_impact.get(0));
			locality.setText(incident_locality.get(0));
			approximateDate.setText(incident_approximateDate.get(0));
			approximateTime.setText(incident_approximateTime.get(0));
			details.setText(incident_details.get(0));
			source.setText(incident_source.get(0));
			postedBy.setText(incident_postedBy.get(0));
			submittedOn.setText(incident_submittedOn.get(0));
		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(getApplicationContext(), "Set Text problem: "+e, Toast.LENGTH_LONG).show();
		}
		
		
		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//reportImage.setImageDrawable(null);
		finish();
	}
	
	public Void loadCommentView() {
		

		AlertDialog.Builder builder = new AlertDialog.Builder(
				ReportDetails.this);
		LayoutInflater inflater = LayoutInflater
				.from(ReportDetails.this);
		View view = inflater.inflate(R.layout.comment_layout, null);
		builder.setCancelable(true);
		builder.setView(view);
		LinearLayout addCommentLay=(LinearLayout)view.findViewById(R.id.addCmntLayout);
		addCommentLay.setClickable(true);
		ListView list = (ListView) view
				.findViewById(R.id.listComment);
		//ImageView imgAddCmnt=(ImageView) view.findViewById(R.id.imgAddComment);
		TextView noComment=(TextView) view.findViewById(R.id.txtNoComment);
		Button ok=(Button) view.findViewById(R.id.btnCommentList);
		
		if(comment.size()!=0)
		{
			noComment.setVisibility(View.GONE);
			noComment.setVisibility(View.INVISIBLE);
			CommentAdapter adapter=new CommentAdapter(ReportDetails.this, commentUser, comment,comment_date);
			list.setAdapter(adapter);
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent,
						View view, int position, long id) {
					// TODO Auto-generated method stub
					

				}
			});
		}
		else
		{
			list.setVisibility(View.GONE);
			list.setVisibility(View.INVISIBLE);
		}
		
		final AlertDialog dg = builder.create();
		
		
		
		
		
		//final AlertDialog dg = builder.create();
		dg.show();
        ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(ReportDetails.this, "ok clicked", Toast.LENGTH_SHORT).show();
				dg.cancel();
			}
		});
        
        //add comment
        
        
		addCommentLay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(ReportDetails.this, "addCommentLayout clicked", Toast.LENGTH_SHORT).show();
				dg.dismiss();
				LayoutInflater inflater = LayoutInflater
						.from(ReportDetails.this);
				View vw = inflater.inflate(R.layout.search_layout,
						null);

				final EditText searchET = (EditText)vw.findViewById(R.id.etSearch);
				searchET.setHint("Add Your Comment");
				AlertDialog.Builder builder=new AlertDialog.Builder(ReportDetails.this);
				builder.setTitle("Add Comment: ");
				builder.setView(vw);
				builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						 InputMethodManager imm = (InputMethodManager)getSystemService(ReportDetails.this.INPUT_METHOD_SERVICE);
				           imm.hideSoftInputFromWindow(searchET.getWindowToken(), 0);
						addValuecomment = searchET.getText()
								.toString();
						if(addValuecomment.equals(""))
						{
							
							Toast.makeText(ReportDetails.this, "No comment", Toast.LENGTH_SHORT).show();
						}
						else
						{
							try {
								runAddCommet();
							} catch (Exception e) {
								// TODO: handle exception
								Log.d("addComment dialog", ""+e);
							}
						}
						
						
						
						
						
						
						
						
						
						
					}
				});
				
				builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});
				
				
				AlertDialog simpleDialog = builder.create();
				simpleDialog.show();
				
				
				
				
				
			}
		});
		
		/*imgAddCmnt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				

				// TODO Auto-generated method stub
				Toast.makeText(ReportDetails.this, "imgAddComment ImageView clicked", Toast.LENGTH_SHORT).show();
				dg.cancel();
				LayoutInflater inflater = LayoutInflater
						.from(ReportDetails.this);
				View view = inflater.inflate(R.layout.search_layout,
						null);

				final EditText searchET = (EditText) view
						.findViewById(R.id.etSearch);
				searchET.setHint("Add Your Comment");
				AlertDialog.Builder builder=new AlertDialog.Builder(ReportDetails.this);
				builder.setTitle("Add Comment: ");
				builder.setView(view);
				builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						addValuecomment = searchET.getText()
								.toString();
						
						
						
						
						
						
						
						
						
						
					}
				});
				
				builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
				});
				
				
				AlertDialog simpleDialog = builder.create();
				simpleDialog.show();
				
				runAddCommet();
			
				
			}
		});*/
        
        
        
        
		return null;
		
	}
	
	 private Integer getUserIdFromSharedPref() {
			
			int userID=pref_user_id.getInt(U_ID, -1);
			
			return userID;
			
		
		}

}
