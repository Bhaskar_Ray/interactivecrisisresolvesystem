package com.iiucbrs.emergencyservice;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class HelpSmsSender extends Activity{
	
	SharedPreferences pref_sms;
	SharedPreferences.Editor editor_pref_sms;
	SharedPreferences pref_user_id;
	public static final String U_ID = "user_id";
	int usr_id;
	LinearLayout layoutContact1,layoutContact2,layoutContact3,layoutEmergencySMS;
	TextView txtContact1,txtContact2,txtContact3,txtSMS;
	String contact1,contact2,contact3,emergencySms,value;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_helpsmssender);
	
		pref_user_id= getSharedPreferences("USER_ID",
				Context.MODE_PRIVATE);
		usr_id=getUserIdFromSharedPref();
		pref_sms= getSharedPreferences(""+usr_id,
				Context.MODE_PRIVATE);
		layoutContact1=(LinearLayout) findViewById(R.id.layout1stContact);
		layoutContact2=(LinearLayout) findViewById(R.id.layout2ndContact);
		layoutContact3=(LinearLayout) findViewById(R.id.layout3rdContact);
		layoutEmergencySMS=(LinearLayout) findViewById(R.id.layoutEmergencySMS);
		txtContact1=(TextView) findViewById(R.id.txt1stContact);
		txtContact2=(TextView) findViewById(R.id.txt2ndContact);
		txtContact3=(TextView) findViewById(R.id.txt3rdContact);
		txtSMS=(TextView) findViewById(R.id.txtEmergencySMS);
		contact1=getsms_content("contact1", usr_id);
		contact2=getsms_content("contact2", usr_id);
		contact3=getsms_content("contact3", usr_id);
		emergencySms=getsms_content("sms", usr_id);
		
		if(!contact1.isEmpty())
		{
			txtContact1.setText(contact1);
		}
		
		if(!contact2.isEmpty())
		{
			txtContact2.setText(contact2);
		}
		
		if(!contact3.isEmpty())
		{
			txtContact3.setText(contact3);
		}
		
		if(!emergencySms.isEmpty())
		{
			txtSMS.setText(emergencySms);
		}
		
		
		
		layoutContact1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String preValue = txtContact1.getText()
						.toString();
				if(usr_id!=-1)
				{
					 inputSMSdialog(preValue,"1st Contact: ", "Add 1st contact number",1,usr_id);
				}
			   
					
					
					
						
					
			  
				
				
				
			}
		});
		layoutContact2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String preValue = txtContact2.getText()
						.toString();
				if(usr_id!=-1)
				{
					 inputSMSdialog(preValue,"2nd Contact: ", "Add 2nd contact number",2,usr_id);
				}
			   
				
			}
		});
		layoutContact3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String preValue = txtContact3.getText()
						.toString();
				if(usr_id!=-1)
				{
					 inputSMSdialog(preValue,"3rd Contact: ", "Add 3rd contact number",3,usr_id);
				}
			   
				
			}
		});
		layoutEmergencySMS.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String preValue = txtSMS.getText()
						.toString();
				if(usr_id!=-1)
				{
					 inputSMSdialog(preValue,"Emergency SMS: ", "Add Emergency SMS",4,usr_id);
				}
			   
			}
		});
		
		
		
		
	
	}
	
	private Integer getUserIdFromSharedPref() {
		
		int userID=pref_user_id.getInt(U_ID, -1);
		
		return userID;
		
	
	}
	
	
	private String getsms_content(String what,int uId) {
		
		String sms_content=pref_sms.getString(what+uId, "");
		
		return sms_content;
		
	
	}
	
	
private void storeSMS_content_Pref(Context context, int uId,String contact,String what) {
		
		
		try {
			
			
			if(uId!=-1&&!contact.isEmpty())
			{
				editor_pref_sms = pref_sms.edit();
				editor_pref_sms.putString(what+uId, contact);
				
				editor_pref_sms.commit();
				String saved=getsms_content(what, uId);
				Toast.makeText(context, what+" saved", Toast.LENGTH_SHORT).show();
				Toast.makeText(context,saved, Toast.LENGTH_SHORT).show();
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("storeSMS_content_Pref", e.getMessage());
		}
		
		

	}

public Void inputSMSdialog(String preValue,String titleDialog,String Hint,final int swtichValue,final int uid) {
	
	
	
	
	
	
	LayoutInflater inflater = LayoutInflater
			.from(HelpSmsSender.this);
	View view = inflater.inflate(R.layout.search_layout,
			null);

	final EditText searchET = (EditText) view
			.findViewById(R.id.etSearch);
	searchET.setHint(Hint);
	searchET.setText(preValue);
    if(swtichValue==4)
    {
    	searchET.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
    }
    else
    {
    	searchET.setInputType(InputType.TYPE_CLASS_PHONE);
    }
	
	searchET.setSelection(searchET.getText().length());
	
	AlertDialog.Builder builder=new AlertDialog.Builder(HelpSmsSender.this);
	builder.setTitle(titleDialog);
	builder.setView(view);
	builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			try {
				
				
				value = searchET.getText()
						.toString();
				switch (swtichValue) {
				case 1:
				
					txtContact1.setText(value);
					storeSMS_content_Pref(HelpSmsSender.this, uid, value, "contact1");
					break;
					
				case 2:
					txtContact2.setText(value);
					storeSMS_content_Pref(HelpSmsSender.this, uid, value, "contact2");
					break;

				case 3:
					txtContact3.setText(value);
					storeSMS_content_Pref(HelpSmsSender.this, uid, value, "contact3");
					break;
					
				case 4:
					
					if(value.length()<=100)
					{
						txtSMS.setText(value);
						storeSMS_content_Pref(HelpSmsSender.this, uid, value, "sms");
					}
					else
					{
						Toast.makeText(HelpSmsSender.this, "You can't add more than 100 character", Toast.LENGTH_LONG).show();
					}
					
					break;
				
				
				}
				
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("sms_saving", e.getMessage());
			}
	
			
			
			
			
			
			
			
			
			
		}
	});
	
	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			
		}
	});
	
	
	AlertDialog simpleDialog = builder.create();
	simpleDialog.show();
	return null;
	
	
	
	
	

	
}



}
