package com.iiucbrs.emergencyservice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapClass extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener,LocationListener, com.google.android.gms.location.LocationListener {
	GoogleMap map;
	Marker marker,myMarker,mMarker;
	LatLng loci,myLoc;//akta lat akta lon use kore location object toiri korbe.
	double lat,lon;
	private GoogleApiClient apiClient;
	Button nearest,refresh,myLocation,mapType;
	ImageView snapshot;
	ActionBar actionBar;
	//int j=0;
	float distance_Mini=100000000;
	Bitmap myBitmap;
	double currentLatitude_mapclass;
	double currentLongitude_mapclass;
    Location currentLocation_mapclass;
	private LocationRequest mLocationRequest_mapclass;
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	float[] results=new float[1];
	private String districtName;
	private String upazilaName;
	private String unionName;
			
	
	
	
	ArrayList<String> Shelter_Name = new ArrayList<String>();
	ArrayList<String> Shelter_Lat = new ArrayList<String>();
	ArrayList<String> Shelter_Lon = new ArrayList<String>();
	ArrayList<String> Shelter_Condition = new ArrayList<String>();
	ArrayList<String> Shelter_no_floor = new ArrayList<String>();
	ArrayList<String> Shleter_floor_space = new ArrayList<String>();
	ArrayList<String> Shelter_capacity = new ArrayList<String>();
	ArrayList<String> Shelter_no_toilet = new ArrayList<String>();
	ArrayList<String> Shleter_water = new ArrayList<String>();
	ArrayList<String> structure_type = new ArrayList<String>();
	ArrayList<String> type_use = new ArrayList<String>();
	ArrayList<Float> distance_list = new ArrayList<Float>();
	ArrayList<LatLng> loc = new ArrayList<LatLng>();
	
	

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.maps_layout);
		
		actionBar = getActionBar();
		actionBar.hide();
		nearest=(Button) findViewById(R.id.nearestShelter);
		myLocation=(Button) findViewById(R.id.myLocation_shelter);
		refresh=(Button) findViewById(R.id.refreshButton_shelter);
		mapType=(Button) findViewById(R.id.menu_shelter);
		snapshot=(ImageView) findViewById(R.id.actionImageView);
		distance_list.clear();
		
		districtName=getIntent().getExtras().getString("district");
		upazilaName=getIntent().getExtras().getString("upazila");
		unionName=getIntent().getExtras().getString("union");
		mLocationRequest_mapclass= LocationRequest.create()
		        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
		        .setInterval(10 * 1000)        // 10 seconds, in milliseconds
		        .setFastestInterval(1 * 1000); // 1 second, in milliseconds

		
		
		snapshot.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				 View v1 = getWindow().getDecorView().getRootView();
//				 v1.setDrawingCacheEnabled(true);
//				 myBitmap = v1.getDrawingCache();
//				 saveBitmap(myBitmap);
				
				 try {
		                CaptureMapScreen();
		            } catch (Exception e) {
		                // TODO: handle exception
		                e.printStackTrace();
		            }
				
			}
		});
		
		nearest.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				try {
//					
//					map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc.get(j), 11.0f));
//					
//					if(loc.size()>j+1)
//					{
//						j++;
//					}
//					else{
//						j=0;
//					}
//						
//				} catch (Exception e) {
//					// TODO: handle exception
//					Toast.makeText(getApplicationContext(), "Can't Find Shelter", Toast.LENGTH_LONG).show();
//				}
				
				

		
				
				try {
					distance_Mini=100000000;
					distance_list.clear();
					distance_shelter();
				} catch (Exception e2) {
					// TODO: handle exception
					Log.e("DISTANCE", e2.getMessage());
				}


				AlertDialog.Builder builder = new AlertDialog.Builder(
						MapClass.this);
				LayoutInflater inflater = LayoutInflater
						.from(MapClass.this);
				View view = inflater.inflate(R.layout.cyclonelist, null);
				builder.setCancelable(true);
				builder.setView(view);
				ListView list = (ListView) view
						.findViewById(R.id.listCycloneShelter);
				Button ok=(Button) view.findViewById(R.id.btnCycloneList);
			CycloneListAdapter adapter=new CycloneListAdapter(MapClass.this, Shelter_Name, districtName, upazilaName, unionName, distance_list,distance_Mini);
			
				list.setAdapter(adapter);
				
				final AlertDialog dg = builder.create();
				list.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc.get(position), 12.0f));

						dg.dismiss();

					}
				});
				
				//final AlertDialog dg = builder.create();
				dg.show();
		        ok.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dg.cancel();
					}
				});
				

			
				
				
				
				
				
			
				
				
			}
		});
		myLocation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				currentLocation_mapclass = LocationServices.FusedLocationApi
						.getLastLocation(apiClient);
				try {
					currentLatitude_mapclass=currentLocation_mapclass.getLatitude();
					currentLongitude_mapclass=currentLocation_mapclass.getLongitude();
					//myLoc=new LatLng(currentLatitude_mapclass, currentLongitude_mapclass);
					setMarkers("You are here", "your location",currentLatitude_mapclass , currentLongitude_mapclass, R.drawable.me);
					//myMarker=map.addMarker(new MarkerOptions().position(myLoc).title("You are here").snippet("Click here"));
					//myMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
					myMarker.showInfoWindow();
					gotoLocation_MapClass(currentLatitude_mapclass, currentLongitude_mapclass, 12.0f);
					//map.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 12.0f));
					
				} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(getApplicationContext(), "Location not found", Toast.LENGTH_LONG).show();
				}
				
			}
		});
		refresh.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		finish();
		Intent in =new Intent(MapClass.this,MapClass.class);
		HashMap<String,ArrayList> shelter_hash=new HashMap<String,ArrayList>();
		 shelter_hash.put("Shelter_Name", Shelter_Name);
		 shelter_hash.put("Shelter_Lat", Shelter_Lat);
		 shelter_hash.put("Shelter_Lon", Shelter_Lon);
		 shelter_hash.put("Shelter_Condition", Shelter_Condition);
		 shelter_hash.put("Shelter_no_floor", Shelter_no_floor);
		 shelter_hash.put("Shleter_floor_space", Shleter_floor_space);
		 shelter_hash.put("Shelter_capacity", Shelter_capacity);
		 shelter_hash.put("Shelter_no_toilet", Shelter_no_toilet);
		 shelter_hash.put("Shleter_water", Shleter_water);
		 shelter_hash.put("structure_type", structure_type);
		 shelter_hash.put("type_use", type_use);
		 Intent intent=new Intent(MapClass.this,MapClass.class);
		 intent.putExtra("key", shelter_hash);
		 intent.putExtra("district", districtName);
		 intent.putExtra("upazila", upazilaName);
		 intent.putExtra("union", unionName);
		 startActivity(intent);
		
		/*try {
			distance_Mini=100000000;
			distance_list.clear();
			distance_shelter();
		} catch (Exception e2) {
			// TODO: handle exception
			Log.e("DISTANCE", e2.getMessage());
		}


		AlertDialog.Builder builder = new AlertDialog.Builder(
				MapClass.this);
		LayoutInflater inflater = LayoutInflater
				.from(MapClass.this);
		View view = inflater.inflate(R.layout.cyclonelist, null);
		builder.setCancelable(true);
		builder.setView(view);
		ListView list = (ListView) view
				.findViewById(R.id.listCycloneShelter);
		Button ok=(Button) view.findViewById(R.id.btnCycloneList);
	CycloneListAdapter adapter=new CycloneListAdapter(MapClass.this, Shelter_Name, districtName, upazilaName, unionName, distance_list);
	
		list.setAdapter(adapter);
		final AlertDialog dg = builder.create();
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent,
					View view, int position, long id) {
				// TODO Auto-generated method stub
//				Intent in = new Intent(MapReport.this,
//						ReportDetails.class);
//				in.putExtra("incident_id",
//						incident_id.get(position));
//				startActivity(in);
				dg.dismiss();

			}
		});
		
		//final AlertDialog dg = builder.create();
		dg.show();
        ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dg.cancel();
			}
		});*/
		

	
		
		
		
		
		
	}
});
		mapType.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub


		final String mapTypes[] = { "None", "Normal", "Satellite",
				"Terrain", "Hybrid" };
		AlertDialog.Builder builder = new AlertDialog.Builder(MapClass.this);
		builder.setTitle("Set Map Type");
		builder.setCancelable(false);
		builder.setItems(mapTypes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int possition) {
				switch (possition) {
				case 0:
					map.setMapType(GoogleMap.MAP_TYPE_NONE);
					break;

				case 1:
					map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
					break;

				case 2:
					map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
					break;

				case 3:
					map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
					break;

				case 4:
					map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
					break;

				default:
					break;
				}
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int possition) {
				dialog.dismiss();
			}
		});
		AlertDialog mapTypeDialog = builder.create();
		mapTypeDialog.show();

	
		
	}
});
		
		
		
		if(isMapAvailable())
		{
			try {
				map=((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.mapCyclone)).getMap();
				if(map==null)
				{
					Toast.makeText(getApplicationContext(), "Map is not initialized", Toast.LENGTH_LONG).show();
				}
				else
				{
					map.setMyLocationEnabled(true);
//					lat=21.427581;
//					lon=91.978403;
					//loc=new LatLng(lat, lon);
					
					Shelter_Name.clear();
					Shelter_Lat.clear();
					Shelter_Lon.clear();
					Shelter_Condition.clear();
					Shelter_no_floor.clear();
					Shleter_floor_space.clear();
					Shelter_capacity.clear();
					Shelter_no_toilet.clear();
					Shleter_water.clear();
					structure_type.clear();
					type_use.clear();
					distance_list.clear();
					loc.clear();
					Intent intent = getIntent();    
				    HashMap<String,ArrayList> shelter_hash=(HashMap<String, ArrayList>) intent.getSerializableExtra("key");
				
					
					Shelter_Name=shelter_hash.get("Shelter_Name");
					Shelter_Lat=shelter_hash.get("Shelter_Lat");
					Shelter_Lon=shelter_hash.get("Shelter_Lon");
					Shelter_Condition=shelter_hash.get("Shelter_Condition");
					Shelter_no_floor=shelter_hash.get("Shelter_no_floor");
					Shleter_floor_space=shelter_hash.get("Shleter_floor_space");
					Shelter_capacity=shelter_hash.get("Shelter_capacity");
					Shelter_no_toilet=shelter_hash.get("Shelter_no_toilet");
					Shleter_water=shelter_hash.get("Shleter_water");
					structure_type=shelter_hash.get("structure_type");
					type_use=shelter_hash.get("type_use");
				//Toast.makeText(getApplicationContext(), "In map class: "+Shleter_facilities_disable.get(0),Toast.LENGTH_LONG).show();
					
//					currentLocation_mapclass = LocationServices.FusedLocationApi
//							.getLastLocation(apiClient);
					for(int i=0;i<Shelter_Lat.size();i++)
					{
					  lat=Double.parseDouble(Shelter_Lat.get(i));
					  lon=Double.parseDouble(Shelter_Lon.get(i));
//					  double lat2=22.967389;
//					 double lon2=89.781306;
//					 double mlat2=22.3604692;
//					 double mlon2=91.8362707;
					 
//					  myLat=currentLocation_mapclass.getLatitude();
//					  myLon=currentLocation_mapclass.getLongitude();
//					Toast.makeText(getApplicationContext(), "Lat: "+lat+" Lon: "+lon,Toast.LENGTH_LONG).show();
//					Toast.makeText(getApplicationContext(), "MLat: "+myLat+" MLon: "+myLon,Toast.LENGTH_LONG).show();
					   //Toast.makeText(getApplicationContext(), "Lat: "+Shelter_Lat.get(i)+" Lon: "+Shelter_Lon.get(0),Toast.LENGTH_LONG).show();
					   
						loci=new LatLng(lat, lon);
						loc.add(loci);
						
						
					
//						marker=map.addMarker(new MarkerOptions().position(loc.get(i)).title(Shelter_Name.get(i)).snippet("Cyclone Shelter"));
//						marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
						
						//results[0]=0.0f;
						
						
						//Location.distanceBetween(mlat2,mlon2 ,lat2 , lon2, results);
						
						
						//distance=results[0];
						
						
						setCycloneMarker(Shelter_Name.get(i), "(cyclone shelter)click to get direction", lat, lon, R.drawable.shelter2, i);
						
						
						map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc.get(0), 11.0f));
						apiClient = new GoogleApiClient.Builder(this)
						.addApi(LocationServices.API)
						.addConnectionCallbacks(this)
						.addOnConnectionFailedListener(this).build();
				apiClient.connect();
					}
//					for(int i=0;i<loc.size();i++)
//					{
//						final LatLngBounds bounds = new LatLngBounds.Builder().include(myLoc).include(loc.get(i)).build();
//				         map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 65));
//					}
					
					
//					marker=map.addMarker(new MarkerOptions().position(loc).title("Hotel").snippet("Here"));//option kon position a boshe ata must
//					//marker er img dui type ar nijeder img or bitmapdescriptor factory
//					marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
//					//map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 11.0f));
//					marker.showInfoWindow();
//					apiClient = new GoogleApiClient.Builder(this)
//					.addApi(LocationServices.API)
//					.addConnectionCallbacks(this)
//					.addOnConnectionFailedListener(this).build();
//			apiClient.connect();
					
				
				}
				
				
			} catch (Exception e) {
				// TODO: handle exception
				Toast.makeText(MapClass.this, ""+e, Toast.LENGTH_SHORT).show();
			}
			
			
			
			map.setOnMarkerClickListener(new OnMarkerClickListener() {
				
				@Override
				public boolean onMarkerClick(Marker marker) {
					// TODO Auto-generated method stub
					marker.showInfoWindow();
					Toast.makeText(MapClass.this, marker.getTitle(), Toast.LENGTH_LONG).show();

					
					
					return true;
				}
			});
			map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
				
				@Override
				public void onInfoWindowClick(final Marker marker) {
					try {
						distance_Mini=100000000;
						distance_list.clear();
						distance_shelter();
					} catch (Exception e2) {
						// TODO: handle exception
						Log.e("DISTANCE", e2.getMessage());
					}
					
					
					if(!marker.getSnippet().equals("your location"))
					{

						// TODO Auto-generated method stub
						AlertDialog.Builder ad = new AlertDialog.Builder(MapClass.this);
						ad.setTitle("Cyclone Shelter Finder");
						ad.setMessage("Details or Direction");
						ad.setPositiveButton("Direction", new DialogInterface.OnClickListener() {
							@Override
							public void onClick( DialogInterface dialog, int arg1) {
								try
								{
									//draw=false;
									Toast.makeText(getApplicationContext(), "Wait to get the direction", Toast.LENGTH_LONG).show();
									try{
										//Toast.makeText(getApplicationContext(), "SO FAR SO GOOD", Toast.LENGTH_LONG).show();
										for(int i=0;i<loc.size();i++)
										{
											final LatLngBounds bounds = new LatLngBounds.Builder().include(myLoc).include(loc.get(i)).build();
									         map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 65));
										}
//										 final LatLngBounds bounds = new LatLngBounds.Builder().include(myLoc).include(loc).build();
//									         map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 65));//65 akta pixel value screen theke 65 pixel distance a thakbe screen er pasher loctaion.
									}
									catch(Exception e)
									{
										Toast.makeText(getApplicationContext(), "Direction can not be shown, please try again.", Toast.LENGTH_LONG).show();
									}
								    String str_origin = "origin="+marker.getPosition().latitude+","+marker.getPosition().longitude;//Destination Lat lon
							        // Destination of route
							        String str_dest = "destination="+currentLatitude_mapclass+","+currentLongitude_mapclass; //Source Lat lon
							        // Sensor enabled
							        String sensor = "sensor=false"; 
							        // Building the parameters to the web service
							        String parameters = str_origin+"&"+str_dest+"&"+sensor;	 
							        // Output format
							        String output = "json";	 
							        // Building the url to the web service
							        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
							        DownloadTask downloadTask = new DownloadTask();	        
						         // Start downloading json data from Google Directions API
						            downloadTask.execute(url); 		            
								}
								catch(Exception e)
								{
									e.printStackTrace();
									Toast.makeText(getApplicationContext(), "Direction can not be shown, please try again.", Toast.LENGTH_LONG).show();
								}
								
								
							}
						});
						
						ad.setNegativeButton("Details", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								//ok, do nothing
								//dialog.cancel();
								AlertDialog.Builder builder = new AlertDialog.Builder(MapClass.this);
								LayoutInflater inflater = LayoutInflater
										.from(MapClass.this);
								View view = inflater.inflate(R.layout.shelter_details_alertbox, null);
								builder.setCancelable(true);
								builder.setView(view);
								TextView shelter_name=(TextView) view.findViewById(R.id.Detail_Shelter_Name);
								TextView shelter_capacity=(TextView) view.findViewById(R.id.Detail_Shelter_capacity);
								TextView shelter_condition=(TextView) view.findViewById(R.id.Detail_Shelter_condition);
								TextView shelter_no_floor=(TextView) view.findViewById(R.id.Detail_Shelter_no_floor);
								TextView shelter_floor_space=(TextView) view.findViewById(R.id.Detail_Shelter_floor_space);
								TextView shelter_structure_type=(TextView) view.findViewById(R.id.Detail_Shelter_structure_type);
								TextView shelter_no_toilet=(TextView) view.findViewById(R.id.Detail_Shelter_no_toilet);
								TextView shelter_water=(TextView) view.findViewById(R.id.Detail_Shelter_water);
								TextView shelter_type_use=(TextView) view.findViewById(R.id.Detail_Shelter_type_use);
								TextView shelter_nearest=(TextView) view.findViewById(R.id.shelter_nearest);
								int i=Integer.parseInt(marker.getSnippet());
								if(distance_list.get(i)==distance_Mini)
								{
									shelter_nearest.setText("Nearest");
								}
								
								
								shelter_name.setText(Shelter_Name.get(i));
								
							    
								shelter_capacity.setText("Capacity: "+Shelter_capacity.get(i)+" people");
								shelter_condition.setText("Condition: "+Shelter_Condition.get(i));
								shelter_no_floor.setText("No. of Floor: "+Shelter_no_floor.get(i)+" floor");
								shelter_floor_space.setText("Floor Space: "+Shleter_floor_space.get(i)+" square feet");
								shelter_structure_type.setText("Structure Type: "+structure_type.get(i));
								shelter_no_toilet.setText("Number of Toilet: "+Shelter_no_toilet.get(i));
								shelter_water.setText("Water: "+Shleter_water.get(i));
								shelter_type_use.setText("Type of Use: "+type_use.get(i));
								
//								double lat1=Double.parseDouble(Shelter_Lat.get(i));
//								double lon1=Double.parseDouble(Shelter_Lon.get(i));
//								float[] results=new float[1];
//								Location.distanceBetween(lat1, lon1, myLat, myLon, results);
//								double distance=(results[0]/1000);
								TextView shelter_address=(TextView) view.findViewById(R.id.Detail_Shelter_address);
								shelter_address.setText("Distance from your Location: "+distance_list.get(i)+" Km");
								/*try {
									

									// TODO Auto-generated method stub
									Geocoder gcd=new Geocoder(MapClass.this, Locale.getDefault());
									List<Address> addresses=null;
									Address addr=null;
									double lat1=Double.parseDouble(Shelter_Lat.get(Integer.parseInt(marker.getSnippet())));
									double lon1=Double.parseDouble(Shelter_Lon.get(Integer.parseInt(marker.getSnippet())));
									try {
										addresses=gcd.getFromLocation(lat1, lon1, 5);//internet connection er ovab a jodi
										if(addresses.size()>0)
										{
											addr=addresses.get(0);
											String area="Address is: ";
											if(addr.getMaxAddressLineIndex()>0)
											{
												area+=addr.getAddressLine(0);//address 0 te roadName thake
											}
											area+=", "+addr.getLocality()+", "+addr.getCountryName();
											//Toast.makeText(getApplicationContext(), area, Toast.LENGTH_LONG).show();
											TextView shelter_address=(TextView) view.findViewById(R.id.Detail_Shelter_address);
											shelter_address.setText("Area: "+area);
											
										
										}
										else
										{
											Toast.makeText(getApplicationContext(), "Address not Found", Toast.LENGTH_LONG).show();
										}
										
									} catch (Exception e) {
										// TODO: handle exception
										Toast.makeText(getApplicationContext(), "Address not found", Toast.LENGTH_LONG).show();
									}
									
								
								} catch (Exception e) {
									// TODO: handle exception
									Toast.makeText(getApplicationContext(), "Address not found", Toast.LENGTH_LONG).show();
								}*/
								
								
								AlertDialog dg = builder.create();
								dg.show();
								//dg.setCancelable(false);
							}
						});
						ad.show();
					
						
					
					
						
						
						//Toast.makeText(getApplicationContext(), marker.getTitle(), Toast.LENGTH_LONG).show();
					}
					

					
				}
					
					
					
				
			});
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			apiClient.connect();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("MCLS_onResume", e.getMessage());
		}
		
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		try {
			if (apiClient.isConnected()) {
				LocationServices.FusedLocationApi.removeLocationUpdates(apiClient, this);
				apiClient.disconnect();
		    }
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("MCLS_onPause", e.getMessage());
		}
	}


	public boolean isMapAvailable() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(MapClass.this);
		if (ConnectionResult.SUCCESS == resultCode)
			return true;
		// device a install kora nai or device a google play service support
		// kore na
		else if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
			// third parameter result code 1 akhetre
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					MapClass.this, 1);
			dialog.show();

		} else {

			Toast.makeText(getApplicationContext(),
					"GooglePlayServices is not avilable ", Toast.LENGTH_LONG)
					.show();
			finish();
		}

		return false;

	}
	
	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		// TODO Auto-generated method stub
		 if (connectionResult.hasResolution()) {
		        try {
		            // Start an Activity that tries to resolve the error
		            connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
		        } catch (IntentSender.SendIntentException e) {
		            e.printStackTrace();
		        }
		    } else {
		        Log.i("MCLS_onConnectionFailed", "Location services connection failed with code " + connectionResult.getErrorCode());
		    }
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		
		currentLocation_mapclass = LocationServices.FusedLocationApi
				.getLastLocation(apiClient);
		try {
			
			if (currentLocation_mapclass == null) {
		        LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, mLocationRequest_mapclass, this);
		    }
		    else {
		        //handleNewLocation(location);
	    	 LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, mLocationRequest_mapclass, this);
//		    	 handleNewLocation(currentLocation);
		    }
			currentLatitude_mapclass=currentLocation_mapclass.getLatitude();
			currentLongitude_mapclass=currentLocation_mapclass.getLongitude();
			myLoc=new LatLng(currentLatitude_mapclass, currentLongitude_mapclass);
			setMarkers("You are here", "your location", currentLatitude_mapclass, currentLongitude_mapclass, R.drawable.me);
			gotoLocation_MapClass(currentLatitude_mapclass, currentLongitude_mapclass, 12.0f);
			//myMarker=map.addMarker(new MarkerOptions().position(myLoc).title("You are here").snippet("Click here"));
			//myMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
			//myMarker.showInfoWindow();
			//map.moveCamera(CameraUpdateFactory.newLatLngZoom(myLoc, 12.0f));
			
		} catch (Exception e) {
			// TODO: handle exception
			//Toast.makeText(getApplicationContext(), "Location not found", Toast.LENGTH_LONG).show();
		}
		
	}
	

	
	
	 private String downloadUrl(String strUrl) throws IOException{
	        String data = "";
	        InputStream iStream = null;
	        HttpURLConnection urlConnection = null;
	        try{
	            URL url = new URL(strUrl);
	            // Creating an http connection to communicate with url
	            urlConnection = (HttpURLConnection) url.openConnection();
	            // Connecting to url
	            urlConnection.connect();
	            // Reading data from url
	            iStream = urlConnection.getInputStream();
	            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
	            StringBuffer sb = new StringBuffer();
	            String line = "";
	            while( ( line = br.readLine()) != null){
	                sb.append(line);
	            }
	            data = sb.toString();
	            br.close();
	 
	        }catch(Exception e){
	            Log.d("Exception while downloading url", e.toString());
	        }finally{
	            iStream.close();
	            urlConnection.disconnect();
	        }
	        return data;
	    }
	 
	 
	  private class DownloadTask extends AsyncTask<String, Void, String>{

	        // Downloading data in non-ui thread
	        @Override
	        protected String doInBackground(String... url) {

	            // For storing data from web service
	            String data = "";

	            try{
	                // Fetching the data from web service
	                data = downloadUrl(url[0]);
	            }catch(Exception e){
	                Log.d("Background Task",e.toString());
	            }
	            return data;
	        }

	        // Executes in UI thread, after the execution of
	        // doInBackground()
	        @Override
	        protected void onPostExecute(String result) {
	        	try {
	        		super.onPostExecute(result);

		            ParserTask parserTask = new ParserTask();

		            // Invokes the thread for parsing the JSON data
		            parserTask.execute(result);
					
				} catch (Exception e) {
					// TODO: handle exception
					//Toast.makeText(getApplicationContext(), ""+e, Toast.LENGTH_LONG).show();
				}
	            
	        }
	    }
	  
	  private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

	        // Parsing the data in non-ui thread
	        @Override
	        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

	            JSONObject jObject;
	            List<List<HashMap<String, String>>> routes = null;

	            try{
	                jObject = new JSONObject(jsonData[0]);
	                DirectionsJSONParser parser = new DirectionsJSONParser();

	                // Starts parsing data
	                routes = parser.parse(jObject);
	            }catch(Exception e){
	                e.printStackTrace();
	            }
	            return routes;
	        }
	  
	  
	  // Executes in UI thread, after the parsing process
      @Override
      protected void onPostExecute(List<List<HashMap<String, String>>> result) {
    	  
    	  try {
    		  
    		  ArrayList<LatLng> points = null;
              PolylineOptions lineOptions = null;
              MarkerOptions markerOptions = new MarkerOptions();

              // Traversing through all the routes
              for(int i=0;i<result.size();i++){
                  points = new ArrayList<LatLng>();
                  lineOptions = new PolylineOptions();

                  // Fetching i-th route
                  List<HashMap<String, String>> path = result.get(i);

                  // Fetching all the points in i-th route
                  for(int j=0;j<path.size();j++){
                      HashMap<String,String> point = path.get(j);

                      double lat = Double.parseDouble(point.get("lat"));
                      double lng = Double.parseDouble(point.get("lng"));
                      LatLng position = new LatLng(lat, lng);

                      points.add(position);
                  }

                  // Adding all the points in the route to LineOptions
                  lineOptions.addAll(points);
                  lineOptions.width(5);
                  lineOptions.color(Color.BLUE);
              }

              // Drawing polyline in the Google Map for the i-th route
              map.addPolyline(lineOptions);
			
		} catch (Exception e) {
			// TODO: handle exception
			//Toast.makeText(getApplicationContext(), ""+e, Toast.LENGTH_LONG).show();
		}
          
      }

  }

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		 Log.i("MCLS_onConnectionSuspended", "Location services suspended. Please reconnect.");
		
	}
//	private LatLng getCurrentLocation() {
//		currentLocation_mapclass = LocationServices.FusedLocationApi
//				.getLastLocation(apiClient);
//		return new LatLng(currentLocation_mapclass.getLatitude(),
//				currentLocation_mapclass.getLongitude());
//	}
	private void setMarkers(String locality, String countryName, double lat,
			double lng, int markerIconRes) {

		if (myMarker != null) {
			myMarker.remove();
		}
		MarkerOptions options = new MarkerOptions().title(locality)
				.position(new LatLng(lat, lng))
				.icon(BitmapDescriptorFactory.fromResource(markerIconRes));

		if (markerIconRes == 0) {
			options.icon(BitmapDescriptorFactory.defaultMarker());
		}

		if (countryName.length() > 0) {
			options.snippet(countryName);
		}

		myMarker = map.addMarker(options);
	}
	private void setCycloneMarker(String locality, String countryName, double lat,
			double lng, int markerIconRes,int pos) {


		MarkerOptions options = new MarkerOptions().title(locality)
				.position(new LatLng(lat, lng))
				.icon(BitmapDescriptorFactory.fromResource(markerIconRes));

		if (markerIconRes == 0) {
			options.icon(BitmapDescriptorFactory.defaultMarker());
		}

		if (countryName.length() > 0) {
			options.snippet(""+pos);
			
		}

		marker = map.addMarker(options);
	}
//	public void saveBitmap(Bitmap bitmap) {
//		 String filePath = Environment.getExternalStorageDirectory()
//		 + File.separator + "Pictures/screenshot.png";
//		 File imagePath = new File(filePath);
//		 FileOutputStream fos;
//		 try {
//		 fos = new FileOutputStream(imagePath);
//		 bitmap.compress(CompressFormat.PNG, 100, fos);
//		 fos.flush();
//		 fos.close();
//		 sendMail(filePath);
//		 } catch (FileNotFoundException e) {
//		 Log.e("GREC", e.getMessage(), e);
//		 } catch (IOException e) {
//		 Log.e("GREC", e.getMessage(), e);
//		 }
//		 }
//		public void sendMail(String path) {
//		 Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
//		 emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
//		 new String[] { "receiver@website.com" });
//		 emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
//		 "Truiton Test Mail");
//		 emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
//		 "This is an autogenerated mail from Truiton's InAppMail app");
//		 emailIntent.setType("image/png");
//		 Uri myUri = Uri.parse("file://" + path);
//		 emailIntent.putExtra(Intent.EXTRA_STREAM, myUri);
//		 startActivity(Intent.createChooser(emailIntent, "Send mail..."));
//		 }
		
	
	private void distance_shelter() {
		
		
		float distance;
		
		for(int i=0;i<Shelter_Lat.size();i++)
		{

		lat=Double.parseDouble(Shelter_Lat.get(i));
		lon=Double.parseDouble(Shelter_Lon.get(i));
		Location.distanceBetween(currentLatitude_mapclass,currentLongitude_mapclass ,lat , lon, results);
		//Toast.makeText(MapClass.this, i+": "+results[0], Toast.LENGTH_LONG).show();
		distance=(float)(results[0]/1000);
		//Toast.makeText(MapClass.this, "km: "+distance, Toast.LENGTH_LONG).show();
		distance_list.add(distance);
		if(distance<distance_Mini)
		distance_Mini=distance;

		}
		
		
	}
	
	

	
	private void gotoLocation_MapClass(double lat_new, double lng_new, float zoom_new) {
		try {
//			Toast.makeText(this, "zomming",
//					Toast.LENGTH_SHORT).show();
			
			LatLng lat_lng = new LatLng(lat_new, lng_new);
			CameraUpdate update_new = CameraUpdateFactory.newLatLngZoom(lat_lng, zoom_new);
			map.animateCamera(update_new);
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("MPAC_GOTO", e.getMessage());
		}
		
	}
		public void CaptureMapScreen() 
		{
		SnapshotReadyCallback callback = new SnapshotReadyCallback() {
		            Bitmap bitmap;

		            @Override
		            public void onSnapshotReady(Bitmap snapshot) {
		                // TODO Auto-generated method stub
		                bitmap = snapshot;
		                
		                String filePath = Environment.getExternalStorageDirectory()
		               		 + File.separator + "Pictures/MapScreen.png";
		               		 File imagePath = new File(filePath);
		               		 FileOutputStream fos;
		               		 try {
		               		 fos = new FileOutputStream(imagePath);
		               		 bitmap.compress(CompressFormat.PNG, 100, fos);
		               		 fos.flush();
		               		 fos.close();
		               		
		               		 } catch (FileNotFoundException e) {
		               		 Log.e("GREC", e.getMessage(), e);
		               		 } catch (IOException e) {
		               		 Log.e("GREC", e.getMessage(), e);
		               		 }
		                
		                
//		                try {
//		                    FileOutputStream out = new FileOutputStream("/mnt/sdcard/"
//		                        + "MyMapScreen" + System.currentTimeMillis()
//		                        + ".png");
//
//		                    // above "/mnt ..... png" => is a storage path (where image will be stored) + name of image you can customize as per your Requirement
//
//		                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
//		                } catch (Exception e) {
//		                    e.printStackTrace();
//		                }
		                openShareImageDialog(filePath);
		            }
		        };

		        map.snapshot(callback);

		        // myMap is object of GoogleMap +> GoogleMap myMap;
		        // which is initialized in onCreate() => 
		        // myMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_pass_home_call)).getMap();
		}
		
		public void openShareImageDialog(String filePath) 
		{
		//File file = this.getFileStreamPath(filePath);

		if(!filePath.equals(""))
		{
//		    final ContentValues values = new ContentValues(2);
//		    values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
//		    values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
//		    final Uri contentUriFile = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		    
		    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
		    intent.setType("image/png");
		    Uri myUri = Uri.parse("file://" + filePath);
		    intent.putExtra(android.content.Intent.EXTRA_STREAM, myUri);
		    startActivity(Intent.createChooser(intent, "Share Image"));
		}
		else
		{
		            //This is a custom class I use to show dialogs...simply replace this with whatever you want to show an error message, Toast, etc.
		    Toast.makeText(getApplicationContext(), "Share Image Failed", Toast.LENGTH_LONG).show();
		}
		}


}
	        
	
	

	  
