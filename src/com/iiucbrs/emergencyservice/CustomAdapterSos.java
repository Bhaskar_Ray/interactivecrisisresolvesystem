package com.iiucbrs.emergencyservice;

import java.util.ArrayList;



import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapterSos extends BaseAdapter {

	
LayoutInflater inflater;
Context context;
//List<String>listStr;
ArrayList<String>listArray;
ArrayList<String>listNumber;
int[] array;
public CustomAdapterSos(Context context,ArrayList<String>listArray,ArrayList<String>listNumber,int[]array){
	this.context=context;
	this.listArray=listArray;
	this.listNumber=listNumber;
	this.array=array;
	inflater=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
}

@Override
public int getCount() {
	// TODO Auto-generated method stub
	return listArray.size();
}

@Override
public Object getItem(int position) {
	// TODO Auto-generated method stub
	return listArray.get(position);
}

@Override
public long getItemId(int arg0) {
	// TODO Auto-generated method stub
	return 0;
}

@Override
public View getView(int position, View convertView, ViewGroup parent) {
	// TODO Auto-generated method stub
	ViewHolder holder=null;
	if(convertView==null){
		holder=new ViewHolder();
		convertView=inflater.inflate(R.layout.customsos, null);
		holder.img=(ImageView)convertView.findViewById(R.id.imgCustom);
		holder.txt=(TextView)convertView.findViewById(R.id.txtText);
		holder.txt1=(TextView)convertView.findViewById(R.id.txtText2);
		convertView.setTag(holder);
	}else{
		holder=(ViewHolder)convertView.getTag();
	}
	holder.txt.setText(listArray.get(position));
	holder.txt1.setText(listNumber.get(position));
	holder.img.setImageResource(array[position]);
		return convertView;
}
public static class ViewHolder{
	ImageView img;
	TextView txt;
	TextView txt1;
}


}
