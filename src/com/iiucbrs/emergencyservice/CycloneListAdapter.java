package com.iiucbrs.emergencyservice;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CycloneListAdapter extends BaseAdapter {
	LayoutInflater inflater;
	Context context;

	ArrayList<String> cycloneShelterName;
	ArrayList<Float> distanceList;
	String districtName,upazilaName,unionName;
	float distance_mini;

	public CycloneListAdapter(Context context, ArrayList<String> cycloneShelterName,
			String districtName, String upazilaName, String unionName,ArrayList<Float> distanceList,float distance_mini ) {
		this.context = context;
		this.cycloneShelterName = cycloneShelterName;
		this.districtName = districtName;
		this.upazilaName = upazilaName;
		this.unionName = unionName;
		this.distanceList = distanceList;
		this.distance_mini=distance_mini;
		inflater = (LayoutInflater) context
				.getSystemService(context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return cycloneShelterName.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return cycloneShelterName.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = inflater.inflate(R.layout.cyclonelistdesign, null);
			holder.shelterName = (TextView) convertView.findViewById(R.id.txtCycloneShelterName);
			holder.locality = (TextView) convertView.findViewById(R.id.txtCycloneShelterLocality);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if(distance_mini==distanceList.get(position))
		{
			holder.shelterName.setTextColor(Color.MAGENTA);
			holder.locality.setTextColor(Color.MAGENTA);
			holder.shelterName.setText(cycloneShelterName.get(position));
			
			holder.locality.setText(districtName+" // "+upazilaName+" // "+unionName+" // "+distanceList.get(position)+" Km"+"(Nearest)");
		}
		else
		{
			holder.shelterName.setTextColor(Color.BLACK);
			holder.locality.setTextColor(Color.BLACK);
			holder.shelterName.setText(cycloneShelterName.get(position));
			
			holder.locality.setText(districtName+" // "+upazilaName+" // "+unionName+" // "+distanceList.get(position)+" Km");
		}
		
		return convertView;
	}

	public static class ViewHolder {
		TextView shelterName;
		TextView locality;
	}

}
