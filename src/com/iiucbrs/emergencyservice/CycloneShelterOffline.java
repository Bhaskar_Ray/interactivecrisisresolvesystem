package com.iiucbrs.emergencyservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CycloneShelterOffline extends Activity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener, LocationListener {


	private AsyncTask<String, String, String> asyncTaskShelter;
	private AsyncTask<String, String, String> asyncTask_Spinner1;
	private AsyncTask<String, String, String> asyncTask_Spinner2;
	private AsyncTask<String, String, String> asyncTask_Spinner3;
	double lat,lon;

	Spinner spinner1, spinner2, spinner3;
	Button submit;
	int selectedspinner1, selectedspinner2, selectedspinner3;
	ArrayList<String> spinner1_list = new ArrayList<String>();
	ArrayList<Integer> spinner1_id = new ArrayList<Integer>();
	ArrayList<String> spinner2_list = new ArrayList<String>();
	ArrayList<Integer> spinner2_id = new ArrayList<Integer>();
	ArrayList<String> spinner3_list = new ArrayList<String>();
	ArrayList<Integer> spinner3_id = new ArrayList<Integer>();
	ArrayList<String> Shelter_Name = new ArrayList<String>();
	ArrayList<String> Shelter_Lat = new ArrayList<String>();
	ArrayList<String> Shleter_Lon = new ArrayList<String>();
	ArrayList<String> Shelter_Condition = new ArrayList<String>();
	ArrayList<String> Shelter_no_floor = new ArrayList<String>();
	ArrayList<String> Shleter_floor_space = new ArrayList<String>();
	ArrayList<String> Shelter_capacity = new ArrayList<String>();
	ArrayList<String> Shelter_no_toilet = new ArrayList<String>();
	ArrayList<String> Shleter_water = new ArrayList<String>();
	//ArrayList<String> Shleter_facilities_disable = new ArrayList<String>();
	ArrayList<String> structure_type = new ArrayList<String>();
	ArrayList<String> type_use = new ArrayList<String>();
	ArrayList<Float> distance_list = new ArrayList<Float>();
	
	private String districtName="";
	private String upazilaName="";
	private String unionName="";
	private GoogleApiClient apiClient_cyclone_offline;
	float distance_Mini=100000000;
	double currentLatitude_cyclone_offline;
	double currentLongitude_cyclone_offline;
    Location currentLocation_cyclone_offline;
	private LocationRequest mLocationRequest_cyclone_offline;
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	float[] results=new float[1];
	 int i;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cyclone_shelter);
		
		setDataHandler();
		mLocationRequest_cyclone_offline= LocationRequest.create()
		        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
		        .setInterval(10 * 1000)        // 10 seconds, in milliseconds
		        .setFastestInterval(1 * 1000); // 1 second, in milliseconds
		
		
		apiClient_cyclone_offline = new GoogleApiClient.Builder(this)
		.addApi(LocationServices.API)
		.addConnectionCallbacks(this)
		.addOnConnectionFailedListener(this).build();
         apiClient_cyclone_offline.connect();

		spinner1 = (Spinner) findViewById(R.id.spDistrict);
		spinner2 = (Spinner) findViewById(R.id.spUpazila);
		spinner3 = (Spinner) findViewById(R.id.spUnion);
		submit=(Button) findViewById(R.id.cyclone_button);
		submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				
					runshelter_AsyncTaskRunner();
				
				
			}
		});
		

	
	
			runspin1_AsyncTaskRunner();
			
		
		
	}

	
	
	
	private void runspin1_AsyncTaskRunner() {
	
			if (asyncTask_Spinner1 == null) {
				// --- create a new task --
				asyncTask_Spinner1 = new spin1_AsyncTaskRunner();
				asyncTask_Spinner1.execute();
			} else if (asyncTask_Spinner1.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTask_Spinner1 = new spin1_AsyncTaskRunner();
				asyncTask_Spinner1.execute();
			} else if (asyncTask_Spinner1.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTask_Spinner1.cancel(false);
				asyncTask_Spinner1 = new spin1_AsyncTaskRunner();
				asyncTask_Spinner1.execute();
			}
		
	}

	class spin1_AsyncTaskRunner extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(CycloneShelterOffline.this);

		protected void onPreExecute() {
			progressDialog.setMessage("Please wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				
				//offline-do
				
				spinner1_list.clear();
				spinner1_id.clear();
				setContentfirst();
				
				
				
				
				
			} catch (Exception e) {
				Log.d("Error:", e + "");
			}
			return null;
		}

		protected void onPostExecute(String string) {
			spinner1.setAdapter(new ArrayAdapter<String>(CycloneShelterOffline.this,
					R.layout.spinner, spinner1_list));
			spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parentView,
						View selectedItemView, int position, long id) {
					selectedspinner1 = spinner1_id.get(spinner1
							.getSelectedItemPosition());
					districtName=spinner1_list.get(spinner1.getSelectedItemPosition());
					Toast.makeText(getApplicationContext(),
							"DIS_ID: " + selectedspinner1, Toast.LENGTH_SHORT)
							.show();
				
						runspin2_AsyncTaskRunner();
					
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
				}
			});
			progressDialog.dismiss();
		}
	}
	
	
	
	private void runspin2_AsyncTaskRunner() {
	
			if (asyncTask_Spinner2 == null) {
				// --- create a new task --
				asyncTask_Spinner2 = new spin2_AsyncTaskRunner();
				asyncTask_Spinner2.execute();
			} else if (asyncTask_Spinner2.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTask_Spinner2 = new spin2_AsyncTaskRunner();
				asyncTask_Spinner2.execute();
			} else if (asyncTask_Spinner2.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTask_Spinner2.cancel(false);
				asyncTask_Spinner2 = new spin2_AsyncTaskRunner();
				asyncTask_Spinner2.execute();
			}
	
	}

	class spin2_AsyncTaskRunner extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(CycloneShelterOffline.this);

		protected void onPreExecute() {
			progressDialog.setMessage("Please wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				
				//offline do
				
				spinner2_list.clear();
				spinner2_id.clear();
				setContent_upazila(selectedspinner1);
				
				
				
				
				
			} catch (Exception e) {
				Log.d("Error:", e + "");
			}
			return null;
		}

		protected void onPostExecute(String string) {
			spinner2.setAdapter(new ArrayAdapter<String>(CycloneShelterOffline.this,
					R.layout.spinner, spinner2_list));
			spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parentView,
						View selectedItemView, int position, long id) {
					selectedspinner2 = spinner2_id.get(spinner2
							.getSelectedItemPosition());
					upazilaName=spinner2_list.get(spinner2.getSelectedItemPosition());
					Toast.makeText(getApplicationContext(),
							"UP_ID: " + selectedspinner2, Toast.LENGTH_SHORT)
							.show();
				
					
						runspin3_AsyncTaskRunner();
					
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
				}
			});

			progressDialog.dismiss();
		}
	}
	
	
	
	private void runspin3_AsyncTaskRunner() {
	
			if (asyncTask_Spinner3 == null) {
				// --- create a new task --
				asyncTask_Spinner3 = new spin3_AsyncTaskRunner();
				asyncTask_Spinner3.execute();
			} else if (asyncTask_Spinner3.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTask_Spinner3 = new spin3_AsyncTaskRunner();
				asyncTask_Spinner3.execute();
			} else if (asyncTask_Spinner3.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTask_Spinner3.cancel(false);
				asyncTask_Spinner3 = new spin3_AsyncTaskRunner();
				asyncTask_Spinner3.execute();
			}
	
	}

	class spin3_AsyncTaskRunner extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(CycloneShelterOffline.this);

		protected void onPreExecute() {
			progressDialog.setMessage("Please wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				
				//offline do----
				spinner3_list.clear();
				spinner3_id.clear();
				setContent_union(selectedspinner2);
				
				
				
			} catch (Exception e) {
				Log.d("Error:", e + "");
			}
			return null;
		}

		protected void onPostExecute(String string) {
			spinner3.setAdapter(new ArrayAdapter<String>(CycloneShelterOffline.this,
					R.layout.spinner, spinner3_list));
			spinner3.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parentView,
						View selectedItemView, int position, long id) {
					selectedspinner3 = spinner3_id.get(spinner3
							.getSelectedItemPosition());
					unionName=spinner3_list.get(spinner3.getSelectedItemPosition());
					Toast.makeText(getApplicationContext(),
							"UN_ID: " + selectedspinner3, Toast.LENGTH_SHORT)
							.show();
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
				}
			});

			progressDialog.dismiss();
			
		
		}
	}
	
	
	private void runshelter_AsyncTaskRunner() {
	
			if (asyncTaskShelter == null) {
				// --- create a new task --
				asyncTaskShelter = new shelter_AsyncTaskRunner();
				asyncTaskShelter.execute();
			} else if (asyncTaskShelter.getStatus() == AsyncTask.Status.FINISHED) {
				asyncTaskShelter = new shelter_AsyncTaskRunner();
				asyncTaskShelter.execute();
			} else if (asyncTaskShelter.getStatus() == AsyncTask.Status.RUNNING) {
				asyncTaskShelter.cancel(false);
				asyncTaskShelter = new shelter_AsyncTaskRunner();
				asyncTaskShelter.execute();
			}
	
	}
	

	
	class shelter_AsyncTaskRunner extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(CycloneShelterOffline.this);

		protected void onPreExecute() {
			progressDialog.setMessage("Please wait...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				Shelter_Name.clear();
				Shelter_Lat.clear();
				Shleter_Lon.clear();
				Shelter_Condition.clear();
				Shelter_no_floor.clear();
				Shleter_floor_space.clear();
				Shelter_capacity.clear();
				Shelter_no_toilet.clear();
				Shleter_water.clear();
				structure_type.clear();
				type_use.clear();
				setContent_shelter(selectedspinner3);
				
				//offline do----

			
			

				
			} catch (Exception e) {
				Log.d("Error:", e + "");
			}
			return null;
		}

		protected void onPostExecute(String string) {
//			spinner3.setAdapter(new ArrayAdapter<String>(CycloneShelter.this,
//					R.layout.spinner, spinner3_list));
//			spinner3.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//				@Override
//				public void onItemSelected(AdapterView<?> parentView,
//						View selectedItemView, int position, long id) {
//					selectedspinner3 = spinner3_id.get(spinner3
//							.getSelectedItemPosition());
//					Toast.makeText(getApplicationContext(),
//							"UN_ID: " + selectedspinner3, Toast.LENGTH_SHORT)
//							.show();
//				}
//
//				@Override
//				public void onNothingSelected(AdapterView<?> arg0) {
//				}
//			});

			//Toast.makeText(getApplicationContext(), "Shelter Name: "+Shelter_Name.get(0), Toast.LENGTH_LONG).show();
			progressDialog.dismiss();
			
			//offline to do-----
			alertshow();
		
			
		
		}
	}
	
	
	
	
	private void setContentfirst() {
		String dbPath = "/data/data/com.iiucbrs.emergencyservice/databases/survival.db";
		SQLiteDatabase myDatabase = null;

		try {
			myDatabase = SQLiteDatabase.openDatabase(dbPath, null,
					SQLiteDatabase.OPEN_READWRITE);
		} catch (Exception e) {
			// Log.d("myLog", "Database Open Problem menu");
		}
		// Log.d("myLog", "getCategory before query=" + getCategory);

		String sql = "SELECT * FROM district";
		// Log.d("myLog", "SQL: " + sql);

		Cursor c = null;
		try {
			c = myDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			// Log.d("myLog", "raw query error");
			// e.printStackTrace();
		}

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			try {
			
				spinner1_list.add(c.getString(c.getColumnIndex("dis_name")));
				spinner1_id.add(c.getInt(c.getColumnIndex("dis_id")));
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				
				Log.e("setContentfirst", e.getMessage());
				
			}

			
		}

		myDatabase.close();

	}
	
	
	
	private void setContent_upazila(int x) {
		String dbPath = "/data/data/com.iiucbrs.emergencyservice/databases/survival.db";
		SQLiteDatabase myDatabase = null;

		try {
			myDatabase = SQLiteDatabase.openDatabase(dbPath, null,
					SQLiteDatabase.OPEN_READWRITE);
		} catch (Exception e) {
			// Log.d("myLog", "Database Open Problem menu");
		}
		// Log.d("myLog", "getCategory before query=" + getCategory);

		String sql = "SELECT * FROM upazila WHERE dis_id=" + x;
		// Log.d("myLog", "SQL: " + sql);

		Cursor c = null;
		try {
			c = myDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			// Log.d("myLog", "raw query error");
			// e.printStackTrace();
		}

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			try {
				// rest_description.add(c.getString(c
				// .getColumnIndex("rest_description")));
				//
				// rest_id.add(c.getInt(c.getColumnIndex("rest_id")));
				// rest_contact.add(c.getString(c.getColumnIndex("rest_contact")));
				// rest_name.add(c.getString(c.getColumnIndex("rest_name")));
				// rest_lat.add(c.getDouble(c.getColumnIndex("rest_lat")));
				// rest_lon.add(c.getDouble(c.getColumnIndex("rest_lon")));
				spinner2_list.add(c.getString(c.getColumnIndex("up_name")));
				spinner2_id.add(c.getInt(c.getColumnIndex("up_id")));
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// Log.d("myLog", "setting var error");
				// e.printStackTrace();
			}

			//
		}

		myDatabase.close();

	}
	
	
	
	private void setContent_union(int x) {
		String dbPath = "/data/data/com.iiucbrs.emergencyservice/databases/survival.db";
		SQLiteDatabase myDatabase = null;

		try {
			myDatabase = SQLiteDatabase.openDatabase(dbPath, null,
					SQLiteDatabase.OPEN_READWRITE);
		} catch (Exception e) {
			// Log.d("myLog", "Database Open Problem menu");
		}
		// Log.d("myLog", "getCategory before query=" + getCategory);

		String sql = "SELECT * FROM union_tbl WHERE up_id=" + x;
		// Log.d("myLog", "SQL: " + sql);

		Cursor c = null;
		try {
			c = myDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			// Log.d("myLog", "raw query error");
			// e.printStackTrace();
		}

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			try {
				// rest_description.add(c.getString(c
				// .getColumnIndex("rest_description")));
				//
				// rest_id.add(c.getInt(c.getColumnIndex("rest_id")));
				// rest_contact.add(c.getString(c.getColumnIndex("rest_contact")));
				// rest_name.add(c.getString(c.getColumnIndex("rest_name")));
				// rest_lat.add(c.getDouble(c.getColumnIndex("rest_lat")));
				// rest_lon.add(c.getDouble(c.getColumnIndex("rest_lon")));
				spinner3_list.add(c.getString(c.getColumnIndex("un_name")));
				spinner3_id.add(c.getInt(c.getColumnIndex("un_id")));
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// Log.d("myLog", "setting var error");
				// e.printStackTrace();
			}

			//
		}

		myDatabase.close();

	}
	
	
	
	private void setContent_shelter(int x) {
		String dbPath = "/data/data/com.iiucbrs.emergencyservice/databases/survival.db";
		SQLiteDatabase myDatabase = null;

		try {
			myDatabase = SQLiteDatabase.openDatabase(dbPath, null,
					SQLiteDatabase.OPEN_READWRITE);
		} catch (Exception e) {
			// Log.d("myLog", "Database Open Problem menu");
		}
		// Log.d("myLog", "getCategory before query=" + getCategory);

		String sql = "SELECT * FROM shelter WHERE un_id=" + x;
		// Log.d("myLog", "SQL: " + sql);

		Cursor c = null;
		try {
			c = myDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			// Log.d("myLog", "raw query error");
			// e.printStackTrace();
		}

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			try {
			
				Shelter_Name.add(c.getString(c.getColumnIndex("shelter_name")));
				Shelter_Lat.add(c.getString(c.getColumnIndex("lat")));
				Shleter_Lon.add(c.getString(c.getColumnIndex("lon")));
				Shelter_Condition.add(c.getString(c.getColumnIndex("shelter_condition")));
				Shelter_no_floor.add(c.getString(c.getColumnIndex("no_floor")));
				Shleter_floor_space.add(c.getString(c.getColumnIndex("floor_space")));
				Shelter_capacity.add(c.getString(c.getColumnIndex("capacity")));
				Shelter_no_toilet.add(c.getString(c.getColumnIndex("no_toilet")));
				Shleter_water.add(c.getString(c.getColumnIndex("water")));
				structure_type.add(c.getString(c.getColumnIndex("structure_type")));
				type_use.add(c.getString(c.getColumnIndex("type_use")));
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// Log.d("myLog", "setting var error");
				// e.printStackTrace();
			}

			//
		}

		myDatabase.close();

	}
	
	
	
	
	private void setDataHandler() {
		DataHandler db = new DataHandler(getApplicationContext());
		try {
			db.createDataBase();
		} catch (IOException e) {
			// Log.d("myLog", "DataBase Exists");
			// e.printStackTrace();
		}

	}




	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		// TODO Auto-generated method stub
		
		if (connectionResult.hasResolution()) {
	        try {
	            // Start an Activity that tries to resolve the error
	            connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
	        } catch (IntentSender.SendIntentException e) {
	            e.printStackTrace();
	        }
	    } else {
	        Log.i("cyoff_onConnectionFailed", "Location services connection failed with code " + connectionResult.getErrorCode());
	    }
		
	}




	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		
		currentLocation_cyclone_offline = LocationServices.FusedLocationApi
				.getLastLocation(apiClient_cyclone_offline);
		try {
			
			if (currentLocation_cyclone_offline == null) {
		        LocationServices.FusedLocationApi.requestLocationUpdates(apiClient_cyclone_offline, mLocationRequest_cyclone_offline, this);
		    }
		    else {
		  
	    	 LocationServices.FusedLocationApi.requestLocationUpdates(apiClient_cyclone_offline, mLocationRequest_cyclone_offline, this);

		    }
			currentLatitude_cyclone_offline=currentLocation_cyclone_offline.getLatitude();
			currentLongitude_cyclone_offline=currentLocation_cyclone_offline.getLongitude();
			
	
		} catch (Exception e) {
			
			Log.e("cyoff_onConnected", e.getMessage());
		
		}
		
		
	}




	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		 Log.i("cyoff_onConnectionSuspended", "Location services suspended. Please reconnect.");
		
	}
	
	
	
	private void distance_shelter() {
		
		
		float distance;
		
		for(int i=0;i<Shelter_Lat.size();i++)
		{

		lat=Double.parseDouble(Shelter_Lat.get(i));
		lon=Double.parseDouble(Shleter_Lon.get(i));
		Location.distanceBetween(currentLatitude_cyclone_offline,currentLongitude_cyclone_offline ,lat , lon, results);
		//Toast.makeText(MapClass.this, i+": "+results[0], Toast.LENGTH_LONG).show();
		distance=(float)(results[0]/1000);
		//Toast.makeText(MapClass.this, "km: "+distance, Toast.LENGTH_LONG).show();
		distance_list.add(distance);
		if(distance<distance_Mini)
		distance_Mini=distance;

		}
		
		
	}
	
	
	private void alertshow()
	{

	
		
		

		
		try {
			distance_Mini=100000000;
			distance_list.clear();
			distance_shelter();
		} catch (Exception e2) {
			// TODO: handle exception
			Log.e("DISTANCE", e2.getMessage());
		}


		AlertDialog.Builder builder = new AlertDialog.Builder(
				CycloneShelterOffline.this);
		LayoutInflater inflater = LayoutInflater
				.from(CycloneShelterOffline.this);
		View view = inflater.inflate(R.layout.cyclonelist, null);
		builder.setCancelable(true);
		builder.setView(view);
		ListView list = (ListView) view
				.findViewById(R.id.listCycloneShelter);
		Button ok=(Button) view.findViewById(R.id.btnCycloneList);
	CycloneListAdapter adapter=new CycloneListAdapter(CycloneShelterOffline.this, Shelter_Name, districtName, upazilaName, unionName, distance_list,distance_Mini);
	
		list.setAdapter(adapter);
		final AlertDialog dg = builder.create();
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent,
					View view, int position, long id) {
				// TODO Auto-generated method stub
				
                //offline  to do-----

				// TODO Auto-generated method stub
				//ok, do nothing
				//dialog.cancel();
			   i=position;
				dg.cancel();
				AlertDialog.Builder builder_new = new AlertDialog.Builder(CycloneShelterOffline.this);
				LayoutInflater inflater = LayoutInflater
						.from(CycloneShelterOffline.this);
				View view_new = inflater.inflate(R.layout.cyclone_details_alrt_offline, null);
				builder_new.setCancelable(true);
				builder_new.setView(view_new);
				TextView shelter_name=(TextView) view_new.findViewById(R.id.Detail_Shelter_Name);
				TextView shelter_capacity=(TextView) view_new.findViewById(R.id.Detail_Shelter_capacity);
				TextView shelter_condition=(TextView) view_new.findViewById(R.id.Detail_Shelter_condition);
				TextView shelter_no_floor=(TextView) view_new.findViewById(R.id.Detail_Shelter_no_floor);
				TextView shelter_floor_space=(TextView) view_new.findViewById(R.id.Detail_Shelter_floor_space);
				TextView shelter_structure_type=(TextView) view_new.findViewById(R.id.Detail_Shelter_structure_type);
				TextView shelter_no_toilet=(TextView) view_new.findViewById(R.id.Detail_Shelter_no_toilet);
				TextView shelter_water=(TextView) view_new.findViewById(R.id.Detail_Shelter_water);
				TextView shelter_type_use=(TextView) view_new.findViewById(R.id.Detail_Shelter_type_use);
				TextView shelter_nearest=(TextView) view_new.findViewById(R.id.shelter_nearest);
				TextView shelter_address=(TextView) view_new.findViewById(R.id.Detail_Shelter_address);
				Button offline_map=(Button) view_new.findViewById(R.id.btnLocationOffline);
				offline_map.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						//to  do---map offline
						Intent in=new Intent(CycloneShelterOffline.this,OfflineMap.class);
						in.putExtra("districtName", districtName);
						in.putExtra("upazilaName", upazilaName);
						in.putExtra("unionName", unionName);
						in.putExtra("shelter_name",Shelter_Name.get(i) );
						in.putExtra("lat",Shelter_Lat.get(i) );
						in.putExtra("lon",Shleter_Lon.get(i) );
						String distance_value=String.valueOf(distance_list.get(i));
						in.putExtra("distance", distance_value);
						startActivity(in);
					}
				});
				
				if(distance_list.get(i)==distance_Mini)
				{
					shelter_nearest.setText("Nearest");
				}
				
				
				shelter_name.setText(Shelter_Name.get(i));
				
			    
				shelter_capacity.setText("Capacity: "+Shelter_capacity.get(i)+" people");
				shelter_condition.setText("Condition: "+Shelter_Condition.get(i));
				shelter_no_floor.setText("No. of Floor: "+Shelter_no_floor.get(i)+" floor");
				shelter_floor_space.setText("Floor Space: "+Shleter_floor_space.get(i)+" square feet");
				shelter_structure_type.setText("Structure Type: "+structure_type.get(i));
				shelter_no_toilet.setText("Number of Toilet: "+Shelter_no_toilet.get(i));
				shelter_water.setText("Water: "+Shleter_water.get(i));
				shelter_type_use.setText("Type of Use: "+type_use.get(i));

				
				shelter_address.setText("Distance from your Location: "+distance_list.get(i)+" Km");
		
				
				
				AlertDialog dg_new = builder_new.create();
				dg_new.show();
				

			}
		});
		
		//final AlertDialog dg = builder.create();
		dg.show();
        ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dg.cancel();
			}
		});
		

	
		
		
		
		
		
	
		
		
	
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		try {
			apiClient_cyclone_offline.connect();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Cyoff_onResume", e.getMessage());
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		try {
			if (apiClient_cyclone_offline.isConnected()) {
				LocationServices.FusedLocationApi.removeLocationUpdates(apiClient_cyclone_offline, this);
				apiClient_cyclone_offline.disconnect();
		    }
			
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Cyoff_onPause", e.getMessage());
		}
	}




	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		
	}

}
