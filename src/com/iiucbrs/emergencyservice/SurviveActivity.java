package com.iiucbrs.emergencyservice;

import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class SurviveActivity extends Activity {
	List<String>disasterList;
	String[]disaster;
	ListView list;
	

	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_survive);
		
		list = (ListView) findViewById(R.id.listRest);
		disaster=getResources().getStringArray(R.array.disaster);
		disasterList=Arrays.asList(disaster);
		
		
		
	int[] array = { R.drawable.bontab, R.drawable.ghurnitab,
			R.drawable.vumitab, R.drawable.drymudtab, R.drawable.noditab};

	CustomAdaptar adapter = new CustomAdaptar(getApplicationContext(),
			disasterList, array);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int pos,
					long id) {
				// TODO Auto-generated method stub
				
				Intent in=new Intent(SurviveActivity.this,SurviveDescription.class);
				in.putExtra("key", pos);
				startActivity(in);

			}
		});
	}

	
}
