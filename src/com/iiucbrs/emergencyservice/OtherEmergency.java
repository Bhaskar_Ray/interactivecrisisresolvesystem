package com.iiucbrs.emergencyservice;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class OtherEmergency extends Activity {
	Button police, hospital, firestation,strategy,about;
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_otheremergency);
		actionBar = getActionBar();
		actionBar.hide();
		police = (Button) findViewById(R.id.btnPolice);
		hospital = (Button) findViewById(R.id.btnDoctor);
		firestation = (Button) findViewById(R.id.btnFire);
		strategy = (Button) findViewById(R.id.btnStrategy);
		about=(Button) findViewById(R.id.menu_shelter);
		about.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in =new Intent(OtherEmergency.this,AboutDev.class);
				startActivity(in);
				
			}
		});
		police.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in =new Intent(OtherEmergency.this,MapActivity.class);
				in.putExtra("types", "police");
				startActivity(in);
				

			}
		});
		hospital.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in =new Intent(OtherEmergency.this,MapActivity.class);
				in.putExtra("types", "hospital");
				startActivity(in);
				

			}
		});
		firestation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in =new Intent(OtherEmergency.this,MapActivity.class);
				in.putExtra("types", "fire_station");
				startActivity(in);

			}
		});
		strategy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent in =new Intent(OtherEmergency.this,SurviveActivity.class);
				startActivity(in);
				
			}
		});

	}

}
