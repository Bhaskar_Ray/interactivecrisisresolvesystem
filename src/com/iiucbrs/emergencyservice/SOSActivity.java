package com.iiucbrs.emergencyservice;

import java.io.IOException;
import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLngBounds;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class SOSActivity extends Activity {
	ListView list;

	ArrayList<String> name = new ArrayList<String>();
	ArrayList<String> num = new ArrayList<String>();
	ArrayList<Integer> id = new ArrayList<Integer>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sos);
		setDataHandler();
		setContent();
		list = (ListView) findViewById(R.id.listsos);
		// str = getResources().getStringArray(R.array.hotel);
		int[] array = { R.drawable.phone1, R.drawable.phone1,
				R.drawable.phone1, R.drawable.phone1, R.drawable.phone1,
				R.drawable.phone1, R.drawable.phone1, R.drawable.phone1,
				R.drawable.phone1, R.drawable.phone1, R.drawable.phone1 };
		// listStr = Arrays.asList(str);
		CustomAdapterSos adapter = new CustomAdapterSos(
				getApplicationContext(), name, num, array);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, final int pos,
					long id) {
				// TODO Auto-generated method stub
				AlertDialog.Builder ad = new AlertDialog.Builder(SOSActivity.this);
				ad.setTitle("SOS Service");
				ad.setMessage("Would you like to Call?");
				ad.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick( DialogInterface dialog, int arg1) {
						
						try {
							Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
									+ num.get(pos)));
							startActivity(i);
							
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
				});
				ad.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						//ok, do nothing
						dialog.cancel();
					}
				});
				ad.show();
			
				

			}
		});
	}

	private void setContent() {
		String dbPath = "/data/data/com.iiucbrs.emergencyservice/databases/survival.db";
		SQLiteDatabase myDatabase = null;

		try {
			myDatabase = SQLiteDatabase.openDatabase(dbPath, null,
					SQLiteDatabase.OPEN_READWRITE);
		} catch (Exception e) {
			// Log.d("myLog", "Database Open Problem menu");
		}
		// Log.d("myLog", "getCategory before query=" + getCategory);

		String sql = "SELECT * FROM number";
		// Log.d("myLog", "SQL: " + sql);

		Cursor c = null;
		try {
			c = myDatabase.rawQuery(sql, null);
		} catch (Exception e) {
			// Log.d("myLog", "raw query error");
			// e.printStackTrace();
		}

		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			try {

				id.add(c.getInt(c.getColumnIndex("id")));

				name.add(c.getString(c.getColumnIndex("name")));
				num.add(c.getString(c.getColumnIndex("num")));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				// Log.d("myLog", "setting var error");
				// e.printStackTrace();
			}

		}

		myDatabase.close();

	}

	private void setDataHandler() {
		DataHandler db = new DataHandler(getApplicationContext());
		try {
			db.createDataBase();
		} catch (IOException e) {
			// Log.d("myLog", "DataBase Exists");
			// e.printStackTrace();
		}

	}

}
